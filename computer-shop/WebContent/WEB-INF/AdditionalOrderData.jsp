<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<t:genericpage pageTitle="Zusätzliche Auftragsdaten">
	<jsp:body>
		<form action="AdditionalOrderData" method="post">
		
			<div class="row">
					<br><br>
					<h2 class="text-center">
						Abweichende Lieferadresse (optional):
					</h2>
					<div class="form-group">
					
						<div class="col-md-2">
						</div>
						
						<div class="col-md-4">
							<br>
							<label for="deliveryName">Name:</label>
		    				<input class="form-control" type="text" name="deliveryName" id="deliveryName" >
		    				<br>
		    				
							<label for="deliveryFirstname">Vorname:</label>
		    				<input class="form-control" type="text" name="deliveryFirstname" id="deliveryFirstname" >
		    				<br>
		    				
		    				<label for="deliveryPostalCode">PLZ:</label>
		    				<input class="form-control" type="text" name="deliveryPostalCode" id="deliveryPostalCode" >
		    				
						</div>
						
						<div class="col-md-4">
							<br>
							<label for="deliveryTown">Ort:</label>
		    				<input class="form-control" type="text" name="deliveryTown" id="deliveryTown" >
		    				<br>
		    				
							<label for="deliveryStreet">Straße:</label>
		    				<input class="form-control" type="text" name="deliveryStreet" id="deliveryStreet" >
		    				<br>
		    				
		    				<label for="deliveryHouseNumber">Hausnummer:</label>
		    				<input class="form-control" type="text" name="deliveryHouseNumber" id="deliveryHouseNumber" >
						</div>
						
						<div class="col-md-2">
						</div>
					
					</div>
				</div>
				
				<div class="row">
					<div class="col-md-2">
					</div>
				
					<div class="col-md-4 text-center">
						<br><br>
						<h2 class="text-center">
							Versandart:
						</h2>
						<label class="radio">
	      					<input type="radio" name="dispatchType" value="Standardversand (3-5 Tage)" checked>Standardversand (3-5 Tage)
	    				</label>
	    				<label class="radio">
	      					<input type="radio" name="dispatchType" value="Premium-Versand (1-3 Tage) +3 EUR">Premium-Versand (1-3 Tage) +3 EUR
	    				</label>
					</div>
						
					<div class="col-md-4 text-center">
						<br><br>
						<h2 class="text-center">
							Zahlungsmethode:
						</h2>
						
						<div class="form-group">
	      						<select class="form-control" name="paymentMethod">
						        <option>Rechnung</option>
						        <option>Vorkasse</option>
						        <option>PayPal</option>
	     						</select>
	     					</div>
					</div>
					
					<div class="col-md-2">
					</div>
				</div>
				
				<div class="row">
					<div class="col-md-8">
					</div>
					<div class="col-md-2">
						<br>
						<button name="submit" value="toOrderOverview" class="btn-float-right btn btn-success btn-lg" type="submit">
							Zur Übersicht
						</button>
					</div>
					<div class="col-md-2">
					</div>
				</div>
			
			</form>
		
	</jsp:body>
</t:genericpage>