<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<t:genericpage pageTitle="${title }">

		<jsp:body>
		
		<div class="container">
		
		<div class="dropdown">
 			 <button class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown">Sortieren
 			 <span class="caret"></span></button>
 			 <ul class="dropdown-menu">
    			 <li><a href="${pageContext.request.contextPath}/AdminArticleOverview?Category=${category}&sortCrit=Price_asc&search=${search}">Preis aufsteigend</a></li>
   				 <li><a href="${pageContext.request.contextPath}/AdminArticleOverview?Category=${category}&sortCrit=Price_desc&search=${search}">Preis absteigend</a></li>
   				 <li><a href="${pageContext.request.contextPath}/AdminArticleOverview?Category=${category}&sortCrit=Name_asc&search=${search}">Name (A-Z)</a></li>
   				 <li><a href="${pageContext.request.contextPath}/AdminArticleOverview?Category=${category}&sortCrit=Name_desc&search=${search}">Name (Z-A)</a></li>
  			</ul>
		</div>
		
			<div class="row">
			<div class="col-md-6">
			<br>
				<!-- Looping through article list and display every second article starting at the first one -->
				<c:forEach begin="0" step="2" items="${articleList }" var="item">
					<div class="row col-bottom-border col-fixed-height">			
						<div class="col-md-5 col-xs-4">
							<a href="AdminArticleView?ArticleNo=${item.articleNo }">
								<img class="img-responsive img-fixed-height" src="${item.pictureLink }" alt="${item.name }">
							</a>
						</div>
						<div class="col-md-6 col-xs-8">
							<h3 class="text-overflow"><a class="article-link" href="AdminArticleView?ArticleNo=${item.articleNo }">${item.name }</a></h3>
							
							<h4><a class="article-link" href="AdminArticleView?ArticleNo=${item.articleNo }">${item.price } EUR</a></h4>
							<h4><a class="article-link" href="AdminArticleView?ArticleNo=${item.articleNo }">${item.averageStars }</a></h4>
						</div>
						<div class="col-md-1 hidden-sm">
						</div>
					</div>
				</c:forEach>
			</div>
			<div class="col-md-6">
			<br>
				<!-- Looping through article list and display every second article starting at the second one -->
				<c:forEach begin="1" step="2" items="${articleList }" var="item">
					<div class="row col-bottom-border col-fixed-height">			
						<div class="col-md-1 hidden-sm">
						</div>
						<div class="col-md-5 col-xs-4">
							<a href="AdminArticleView?ArticleNo=${item.articleNo }">
								<img class="img-responsive img-fixed-height" src="${item.pictureLink }" alt="${item.name }">
							</a>
						</div>
						<div class="col-md-6 col-xs-8">
							<h3 class="text-overflow"><a class="article-link" href="AdminArticleView?ArticleNo=${item.articleNo }">${item.name }</a></h3>
							
							<h4><a class="article-link" href="AdminArticleView?ArticleNo=${item.articleNo }">${item.price } EUR</a></h4>
							<h4><a class="article-link" href="AdminArticleView?ArticleNo=${item.articleNo }">${item.averageStars }</a></h4>
						</div>
					</div>
				</c:forEach>
			</div>
		</div>
		</div>
			
		</jsp:body>

</t:genericpage>