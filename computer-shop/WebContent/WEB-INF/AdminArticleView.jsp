<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<t:genericpage pageTitle="${article.name }">
	<jsp:body>
		<!-- Save article number in session -->
		<c:set var="articleNo" value="${param.ArticleNo }" scope="session" />
		
		<!-- If article was changed, display message -->
		<c:if test="${articleChanged == 'true' }">
			<div class="alert alert-success alert-dismissible fade in text-center">
   		 		<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
				Artikel wurde erfolgreich geändert!
  			</div>
		</c:if>
		
		<h1 class="text-center"> Artikelbearbeitung von ID: ${article.articleNo } </h1>
		<br><br>
			
		<div class="col-md-4">
		<p><img class="img-responsive" src="${article.pictureLink }" alt="${name }"> </p>
						</div>
		<div class="row">
		<div class="col-md-4">
		</div>
		<div class="col-md-4">
			<form action="AdminArticleView" method="post">
				
				<div class="form-group"> 
					<label class="label-big" for="adminName">Artikelbezeichnung:</label>
					<input maxLength="45" class="form-control " type="text" id="adminName" name="adminName" value ="${article.name}">
					<br>
					<label class="label-big" for="adminDescription">Artikelbeschreibung:</label>
  					<textarea class="form-control" rows="5" id="adminDescription" name="adminDescription">${article.description}</textarea>
					<br>
					<label class="label-big" for="adminPrice">Preis:</label>
					<input maxLength="45" class="form-control " type="text" id="adminPrice" name="adminPrice" value ="${article.price}">
					<br>
					<label class="label-big" for="adminMinStock">Mindestbestand:</label>
					<input maxLength="45" class="form-control " type="text" id="adminMinStock" name="adminMinStock" value ="${article.minStock}">
					<br>
					<label class="label-big" for="adminCurrentStock">Aktueller Lagerbestand:</label>
					<input maxLength="45" class="form-control " type="text" id="adminCurrentStock" name="adminCurrentStock" value ="${article.currentStock}">
					
					<label class="label-big" for="adminCurrentStock">Lagermeldungen: </label>
					
					<!-- If current stock is smaller than the minimum stock display red light, otherwise display green light -->
					<c:choose>
						<c:when test="${article.currentStock+0 < article.minStock+0 }">
							<img src="Pictures/red.png" alt="Nahezu ausverkauft" width="25em"height="25em" />
						</c:when>
						<c:otherwise>
							<img src="Pictures/green.png" alt="Noch auf Lager" width="25em" height="25em" />
						</c:otherwise>
					</c:choose>
					
					<br><br>
					
						<label class="label-big" for="adminCategory">Kategorie:</label>
						<select id="adminCategory" name="adminCategory" class="form-control" >
		        			<option value="1">PC</option>
 							<option value="2">Maus</option>
	        				<option value="3">Monitor</option>
	        				<option value="4">Notebook</option>
	    				</select>
					
					
	    			<br>
	    			<label class="label-big" for="adminStatus">Artikelstatus:</label>
						<select id="adminStatus" name="adminStatus" class="form-control" >
		        			<option value="active">Aktiv</option>
 							<option value="inactive">Inaktiv</option>
	    				</select>
	    			
					
					<br><br>
					
					<input class="btn btn-success label-big" type="submit" value="Artikel aktualisieren" name="update">
					
				</div>
				
				
					

			</form>
			</div>
		</div>
		
	</jsp:body>

</t:genericpage>