<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<t:genericpage pageTitle="Fragen beantworten">
	<jsp:body>
	
		<!-- If question was answered, display message -->
		<c:if test="${questionAnswered == 'true' }">
			<div class="alert alert-success alert-dismissible fade in text-center">
   		 		<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
				Frage beantwortet!
  			</div>
		</c:if>

		<div class="container">
			<div class="row">
				<h1 class="text-center">
					Fragen beantworten:
				</h1>
				<br>
				
				<!-- Looping through question list and displaying them -->
				<c:forEach items="${questionList }" var="items">
					<div class="panel panel-default">
						<div class="panel-heading">
							<h4>Frage-Nr: ${items.ID } | Status: ${items.active } | Vom: ${items.date }</h4>
						</div>
						<div class="panel-body">
							<h4>Thema: ${items.title }</h4>
							<p>${items.question }</p>
							
							<h4>Antwort:</h4>
							<p>${items.answer }</p>
						</div>
						<div class="panel-footer">
							<a class="btn btn-success" href="AnswerQuestionsDetail?ID=${items.ID }">Antwort schreiben</a>
						</div>
					</div>
				</c:forEach>
				
				
			</div>
		</div>

	</jsp:body>
</t:genericpage>