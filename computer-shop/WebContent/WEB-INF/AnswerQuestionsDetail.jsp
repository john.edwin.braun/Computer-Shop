<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<t:genericpage pageTitle="Fragen beantworten">
	<jsp:body>

		<div class="container">
			<div class="row">
				<h1 class="text-center">
					Frage beantworten:
				</h1>
				
				<h3 class="text-center">
					Frage-Nr: ${question.ID } | Status: ${question.active } | Vom: ${question.date }
				</h3>
				
				<form action="AnswerQuestionsDetail" method="post">
						<div class="form-group">
							<label for="title">Thema:</label>
							<input class="form-control" type="text" name="title" id="title" maxlength="45" value="${question.title }" required>
							<br>
   							<label for="question">Frage:</label>
   							<textarea rows="5" class="form-control" id="question" name="question" placeholder="Maximal 400 Zeichen..." maxlength="400" required>${question.question }</textarea>
		    				<br>
		    				<label for="questioner">Name:</label>
							<input class="form-control" type="text" name="questioner" id="questioner" maxlength="45" value="${question.questioner }">
							<br>
							<label for="answer">Antwort:</label>
   							<textarea rows="5" class="form-control" id="answer" name="answer" placeholder="Maximal 400 Zeichen..." maxlength="400" required>
   							</textarea>
		    				<br>
		    				
		    				<label for="active">Artikelstatus:</label>
							<select id="active" name="active" class="form-control" >
			        			<option value="true">Aktiv</option>
	 							<option value="false">Inaktiv</option>
		    				</select>
		    				<br>
		    			</div>
		    			
		    			<input type="hidden" name="ID" value="${question.ID}">
		    			<input type="hidden" name="date" value="${question.date}">
		    			<div class="text-center">
		    				<input type="hidden" name="questionAsked" value="true">
		    				<input class="btn label-big btn-success" type="submit" value="Beantworten" name="submit">
		    				<br>
		    				<br>
		    			</div>
		    			
					</form>
				
			</div>
		</div>

	</jsp:body>
</t:genericpage>