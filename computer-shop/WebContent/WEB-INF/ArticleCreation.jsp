<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>

<t:genericpage pageTitle="Neuen Artikel anlegen">
	<jsp:body>
		<div class="container">
			<h1 class="text-center">Anlegen eines neuen Artikels in der Datenbank <br> Schritt 1/2</h1>
			<div class="row">
				<div class="col-md-2">
				</div>
				<div class="col-md-8">
				
					<form action="ArticleCreation" method="post">
						<div class="form-group"> 
							<label class="label-big" for="name">Artikelbezeichnung:</label>
							<input maxLength="45" class="form-control " type="text" id="name" name="name">
							<br>
							<label class="label-big" for="description">Artikelbeschreibung:</label>
		  					<textarea class="form-control" rows="5" id="description" name="description" maxlength="998"></textarea>
							<br>
							<label class="label-big" for="price">Preis:</label>
							<input maxLength="45" class="form-control " type="number" step="0.01" id="price" name="price">
							<br>
							<label class="label-big" for="minStock">Mindestbestand:</label>
							<input maxLength="45" class="form-control " type="number" id="minStock" name="minStock">
							<br>
							<label class="label-big" for="currentStock">Aktueller Lagerbestand:</label>
							<input maxLength="45" class="form-control " type="number" id="currentStock" name="currentStock">
							
							<br>
							
								<label class="label-big" for="category">Kategorie:</label>
								<select id="category" name="category" class="form-control" >
				        			<option value="1">PC</option>
		 							<option value="2">Maus</option>
			        				<option value="3">Monitor</option>
			        				<option value="4">Notebook</option>
			    				</select>
							
							
			    			<br>
			    			<label class="label-big" for="status">Artikelstatus:</label>
								<select id="status" name="status" class="form-control" >
				        			<option value="active">Aktiv</option>
		 							<option value="inactive">Inaktiv</option>
			    				</select>
			    			
							
							<br><br>
							
							<input class="btn btn-success label-big" type="submit" value="Artikel anlegen (1/2)" name="create">
							
						</div>
					</form>
				
				</div>
				<div class="col-md-2">
				</div>
			</div>
		</div>
	
		
		
		
	</jsp:body>
</t:genericpage>