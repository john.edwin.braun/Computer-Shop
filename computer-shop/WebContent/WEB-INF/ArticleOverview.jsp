<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<t:genericpage pageTitle="${title }" category="${category }" sortCrit="${sortCrit }">

	<jsp:body>
	
	<div class="container">
		<div class="row">
			<div class="col-md-1">
				<br>
				<div class="dropdown">
		 			<button class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown">Sortieren
		 			<span class="caret"></span></button>
		 			<ul class="dropdown-menu">
		    			 <li><a href="ArticleOverview?Category=${category}&sortCrit=Price_asc&search=${search}">Preis aufsteigend</a></li>
		   				 <li><a href="ArticleOverview?Category=${category}&sortCrit=Price_desc&search=${search}">Preis absteigend</a></li>
		   				 <li><a href="ArticleOverview?Category=${category}&sortCrit=Name_asc&search=${search}">Name (A-Z)</a></li>
		   				 <li><a href="ArticleOverview?Category=${category}&sortCrit=Name_desc&search=${search}">Name (Z-A)</a></li>
		  			</ul>
				</div>
			</div>
			
			<div class="col-md-10">
				<h2 class="text-center">Kategorie: 
					<!-- Displaying the current category -->
					<c:choose>
						<c:when test="${category == 'All' }">
							Alle Artikel
						</c:when>
						<c:when test="${category == 'Accessory' }">
							Zubehör
						</c:when>
						<c:otherwise>
							${category}
						</c:otherwise>
					</c:choose>
				</h2>
				<p class="text-center"><a href="ArticleOverview?Category=All&sortCrit=Name_asc">Alle Artikel anzeigen</a></p>
				<br>
			</div>
			
			<div class="col-md-1">
			</div>
			
		</div>
			
		<div class="row">
			<div class="col-md-6">
			<br>
				<!-- Looping through article list and displaying every second item starting with the first one -->
				<c:forEach begin="0" step="2" items="${articleList }" var="item">
					<div class="row col-bottom-border col-fixed-height">			
						<div class="col-md-5 col-xs-4">
							<a href="ArticleView?ArticleNo=${item.articleNo }">
								<img class="img-responsive img-fixed-height" src="${item.pictureLink }" alt="${item.name }">
							</a>
						</div>
						<div class="col-md-6 col-xs-8">
							<h3 class="text-overflow"><a class="article-link" href="ArticleView?ArticleNo=${item.articleNo }">${item.name }</a></h3>
							
							<h4><a class="article-link" href="ArticleView?ArticleNo=${item.articleNo }">${item.price } EUR</a></h4>
							<h4><a class="article-link" href="ArticleView?ArticleNo=${item.articleNo }">${item.averageStars }</a></h4>
						</div>
						<div class="col-md-1 hidden-sm">
						</div>
					</div>
				</c:forEach>
			</div>
			<div class="col-md-6">
			<br>
				<!-- Looping through article list and displaying every second item starting with the second one -->
				<c:forEach begin="1" step="2" items="${articleList }" var="item">
					<div class="row col-bottom-border col-fixed-height">			
						<div class="col-md-1 hidden-sm">
						</div>
						<div class="col-md-5 col-xs-4">
							<a href="ArticleView?ArticleNo=${item.articleNo }">
								<img class="img-responsive img-fixed-height" src="${item.pictureLink }" alt="${item.name }">
							</a>
						</div>
						<div class="col-md-6 col-xs-8">
							<h3 class="text-overflow"><a class="article-link" href="ArticleView?ArticleNo=${item.articleNo }">${item.name }</a></h3>
							
							<h4><a class="article-link" href="ArticleView?ArticleNo=${item.articleNo }">${item.price } EUR</a></h4>
							<h4><a class="article-link" href="ArticleView?ArticleNo=${item.articleNo }">${item.averageStars }</a></h4>
						</div>
					</div>
				</c:forEach>
			</div>
		</div>
	</div>
		
	</jsp:body>

</t:genericpage>