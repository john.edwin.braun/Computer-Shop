<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<t:genericpage pageTitle="${article.name }">
	<jsp:body>
		<div class="container-fluid">
			<!-- If article was added to the shopping cart, display message -->
			<c:if test="${addedToShoppingCart == 'true' }">
				<div class="alert alert-success alert-dismissible fade in text-center">
    				<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
    				Artikel wurde erfolgreich zum Warenkorb hinzugefügt!
  				</div>
			</c:if>
			
			<!-- If article was reviewed, display message -->
			<c:if test="${savedReview == 'true' }">
				<div class="alert alert-success alert-dismissible fade in text-center">
    				<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
    				Ihre Bewertung wurde erfolgreich gespeichert!
  				</div>
			</c:if>
			
			<div class="row">
				<div class="col-xs-12">
					<h1 class="text-center" style="font-size:4em;word-break: break-word;">
						${article.name }
					</h1><br>
				</div>
			</div>
			
			
			<div class="row">
				<div class="col-md-4">
					<p><img class="img-responsive" src="${article.pictureLink }" alt="${name }"> </p>
				</div>
				<div class="col-md-5">
					<p>
						${article.description } <br><br>
					</p>
					
					<p style="font-size:2em; font-weight:bold;">
						${article.price } EUR 
						<!-- If current stock is smaller than minimum stock, display yellow light. Otherwise display green light -->
						<c:choose>
							<c:when test="${article.currentStock+0 < 0+article.minStock}">
								<img src="Pictures/yellow.png" alt="Nahezu ausverkauft" width="25em"height="25em" />
								<p>Dieser Artikel hat leider eine verlängerte Lieferzeit</p>
							</c:when>
							<c:otherwise>
								<img src="Pictures/green.png" alt="Noch auf Lager" width="25em" height="25em" />
								<p>Dieser Artikel ist frei verfügbar!</p>
							</c:otherwise>
						</c:choose>
					</p>
							
					<form action="ArticleView" method="post">
						<input type="hidden" name="articleNo" value="${article.articleNo }"/>
						<div class="input-group">
							<span class="input-group-addon label-big">Anzahl</span>
							<input class="form-control label-big" type="number" id="amount" name="amount" min="1" value="1">
						</div>
						<br>
						<button class="btn btn-success label-big" type="submit" value="addToShoppingCart" name="submit">
							In den Warenkorb!
						</button>
						
					</form>
					<br><br><br>
				</div>
				<div class="col-md-3">
				
				</div>
			</div>
		</div>
		
		<div class="container">
			<br><br><br>
			<h2 class="text-center">
				Reviews: ${article.averageStars }<br>
			</h2>
			
			<!-- Looping through review list and displaying as much as number of reviews -->
			<c:forEach items="${reviews }" varStatus="item" var="review" end="${numberOfReviews - 1 }">
				<!-- Count the reviews displayed -->
				<c:set var="lastReviewIndex" value="${item.count }"/>
				<div class="row">
					<div class="col-md-1">
					</div>
					<div class="col-md-10">
						<div class="panel-group">
			    			<div class="panel panel-default">
			      				<div class="panel-heading">
			        				<h2 class="panel-title">
			          					<a data-toggle="collapse" href="#collapse_${item.index }">${review.title }
			          					<span class="glyphicon glyphicon-collapse-down"></span>
			          					</a>
			        				</h2>
			      				</div>
			      				<div id="collapse_${item.index }" class="panel-collapse collapse">
			        				<ul class="list-group">
			          					<li class="list-group-item">Geschrieben von: ${review.customerName }</li>
			          					<li class="list-group-item">${review.text }</li>
			        				</ul>
			        				<div class="panel-footer">
			        					${review.numberOfStars } von 5 Sternen.
			        				</div>
			      				</div>
			    			</div>
			  			</div>
					</div>
					<div class="col-md-1">
					</div>
				</div>
			</c:forEach>
			
			<!-- If less reviews are shown than there are reviews, display button to load more reviews.
			Otherwise show that there are no more reviews -->
			<c:choose>
				<c:when test="${lastReviewIndex < maxNoOfReviews}">
					<div class="text-center">
						<form action="ArticleView" method="post">
							<input type="hidden" name="articleNo" value="${article.articleNo }"/>
							<input type="hidden" name="numberOfReviews" value="${numberOfReviews }"/>
							<button class="btn btn-info" type="submit" value="loadMoreReviews" name="submit">
								Weitere Reviews laden...
							</button>
						</form>
						<br>
					</div>
				</c:when>
				<c:otherwise>
					<p class="text-center">Keine weiteren Reviews</p>
					<br>
				</c:otherwise>
			</c:choose>
		</div>
		
		<div class="container">
			<!--
			If user is not logged in, let him/her log in.
			If user already reviewed the article, show message that he/she already reviewed the article.
			If user did not review the article, let him/her review the article. 
			-->
			<c:choose>
				<c:when test="${reviewStatus == 'notLoggedIn' }">
					<p class="text-center">
						Um eine Review zu schreiben, melden Sie sich <a href="Login?destination=ArticleView?ArticleNo=${article.articleNo }">hier</a> an.
					</p>
				</c:when>
				<c:when test="${reviewStatus == 'alreadyReviewed' }">
					<p class="text-center">
						Sie haben dieses Produkt bereits bewertet.<br>
					</p>
				</c:when>
				<c:when test="${reviewStatus == 'notReviewed' }">
					<h2 class="text-center">
						Schreiben Sie ihre eigene Bewertung:
					</h2>
					<form action="ArticleView" method="post">
						<div class="row">
							<div class="col-md-1">
							</div>
							<div class="col-md-7">
								<div class="form-group">
	      							<label for="title">Titel:</label>
	      							<input type="text" class="form-control" id="title" name="reviewTitle" maxlength="44" required>
	   							</div>
							</div>
							<div class="col-md-3">
								<div class="form-group">
									<label for="stars">Sterne:</label>
									<select class="form-control" id="stars" name="stars">
								    	<option>1</option>
								   		<option>2</option>
								   		<option>3</option>
								    	<option>4</option>
								    	<option>5</option>
									</select>
								</div>
							</div>
							<div class="col-md-1">
							</div>
						</div>
						<div class="row">
							<div class="col-md-1">
							</div>
							<div class="col-md-10">
	   							<div class="form-group">
	      							<label for="text">Text:</label>
	      							<textarea rows="5" class="form-control" id="text" name="reviewText" placeholder="Maximal 1000 Zeichen..." maxlength="999" ></textarea>
	   							</div>
	   							<input type="hidden" name="articleNo" value="${article.articleNo }"/>
	   							<button class="btn btn-success label-big" type="submit" value="sendReview" name="submit">
									Bewertung speichern!
								</button>
							</div>
							<div class="col-md-1">
							</div>
						</div>
					</form>
					
				</c:when>
			</c:choose>
		</div>
		
	</jsp:body>

</t:genericpage>