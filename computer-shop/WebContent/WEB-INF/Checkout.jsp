<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>

<t:genericpage pageTitle="Kasse">
	<jsp:body>
		<div class="container">
			<h1 class="text-center">Zur Kasse</h1>
			<br><br>
			
			<div class="row">
				<div class="col-md-4">
				</div>
				<div class="col-md-4">
					<a href="Login?destination=AdditionalOrderData" class="btn btn-block btn-success btn-lg" role="button">Login</a>
					
					<h4 class="text-center">oder</h4>
					
					<a href="OneTimeCustomer" class="btn btn-block btn-info" role="button">Als Gast bestellen</a>
				</div>
				<div class="col-md-4">
				</div>
			</div>		
		</div>
	</jsp:body>
</t:genericpage>