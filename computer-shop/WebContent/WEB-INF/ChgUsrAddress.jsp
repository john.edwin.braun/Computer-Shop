<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<t:genericpage pageTitle="Adresse ändern">
	<jsp:body>
	
		<!-- If address was changed, display message -->
		<c:if test="${addressChanged == 'true' }">
			<div class="alert alert-success alert-dismissible fade in text-center">
   		 		<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
				Die Adresse wurde erfolgreich geändert!
  			</div>
		</c:if>
		
		<div class="container">
			<h1 class="center-text"> Nutzerdetails ändern </h1>
			<br><br>
		
		
		<form action="ChgUsrAdress" method="post">

	
		
 		 <label for="newInvoiceStreet">Straße:</label>
	     <input class="form-control" type="text" name="newInvoiceStreet" id="newInvoiceStreet" value ="${customer.invoiceStreet}" required>
	     <br>
	     
	    <label for="newInvoiceHouseNumber">Hausnummer:</label>
	    <input class="form-control" type="text" name="newInvoiceHouseNumber" id="newInvoiceHouseNumber" value ="${customer.invoiceHouseNumber}" required>
	    <br>
		
		 <label for="newInvoicePostalCode">PLZ:</label>
	    <input class="form-control" type="text" name="newInvoicePostalCode" id="newInvoicePostalCode" value ="${customer.invoicePostalCode}" required>
	    <br>
	    		
	    <label for="newInvoiceTown">Ort:</label>
	    <input class="form-control" type="text" name="newInvoiceTown" id="newInvoiceTown" value ="${customer.invoiceTown}" required>
	    <br>	    
	     
	     <div class="text-center">
		 <button class="btn btn-success" type="submit" value="submit"> Adresse ändern </button>
		</div>
</form>
		
		
		<p class="error-message center-text">
			${message }
		</p>
		
		
	
</div>
	</jsp:body>
</t:genericpage>
