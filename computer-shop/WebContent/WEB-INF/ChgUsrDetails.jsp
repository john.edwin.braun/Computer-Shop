<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<t:genericpage pageTitle="Nutzerdetails ändern">
	<jsp:body>
	
	<!-- If details were changed, display message -->
	<c:if test="${detailsChanged == 'true' }">
		<div class="alert alert-success alert-dismissible fade in text-center">
 			<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
			Die Angaben wurde erfolgreich geändert!
		</div>
	</c:if>
		
		<div class="container">
			<h1 class="center-text"> Nutzerdetails ändern </h1>
			<br><br>
		
		
	<form action="ChgUsrDetails" method="post">
	
 
		<label for="newtitle">Neuer Titel:</label>
	    <input class="form-control" type="text" name="newtitle" id="newTitle" value ="${customer.title}">
	    <br>
				
	    <label for="newformOfAddress">Neue Anrede:</label>
						<select id="newformOfAddress" name="newformOfAddress" class="form-control">
		        			<option value="Herr">Herr</option>
		        			<option value="Frau">Frau</option>
		        			<option value="Anderes">Anderes</option>
	    				</select>
	    				<br>
	    
		<label for="newfirstname">Neuer Vorname:</label>
	    <input class="form-control" type="text" name="newfirstname" id="newfirstname" value ="${customer.firstname}" required>
	    <br>
	
				
	    <label for="newname">Neuer Nachname:</label>
	    <input class="form-control" type="text" name="newname" id="newname" value ="${customer.name}"required>
	    <br>
	    
		<div class="text-center">
		 <button class="btn btn-success" type="submit" value="submit"> Benutzerdetails ändern </button>
		</div>
	
</form>

	<p class="error-message center-text">
		${message }
	</p>
	
		
	
</div>
	</jsp:body>
</t:genericpage>

