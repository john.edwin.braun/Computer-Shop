<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<t:genericpage pageTitle="E-Mail ändern">
	<jsp:body>
	
	<!-- If email was changed, display message -->
	<c:if test="${mailChanged == 'true' }">
			<div class="alert alert-success alert-dismissible fade in text-center">
   		 		<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
				Die Email - Adresse wurde erfolgreich geändert!
  			</div>
		</c:if>
		
		<div class="container">
			<h1 class="center-text"> E-Mail ändern </h1>
			<br><br>
		
	<form action="ChgUsrMail" method="post">
	
		<label for="oldmail">Alte E-Mail Adresse:</label>
	    <input maxLength="45" class="form-control" type="email" name="oldmail" id="oldmail" required>
	    <br>
				
	    <label for="newmail1">Neue E-Mail Adresse:</label>
	    <input maxLength="45" class="form-control" type="email" name="newmail1" id="newmail1" required>
	    <br>
	    
	    <label for="newmail2">Neue E-Mail Adresse bestätigen:</label>
	    <input maxLength="45" class="form-control" type="email" name="newmail2" id="newmail2" required>
	    <br>
	    
	    <div class="text-center">
		 <button class="btn btn-success" type="submit" value="submit"> E-Mail ändern </button>
		
		</div>
	</form> 
	
	<p class="error-message center-text">
		${message }
	</p>
	
	
		
	
</div>


</jsp:body>
</t:genericpage>