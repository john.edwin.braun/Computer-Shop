<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<t:genericpage pageTitle="Passwort ändern">
	<jsp:body>
	
	<!-- If password was changed, display message -->
	<c:if test="${passwordChanged == 'true' }">
			<div class="alert alert-success alert-dismissible fade in text-center">
   		 		<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
				Das Passwort wurde erfolgreich geändert!
  			</div>
		</c:if>
		
		<div class="container">
			<h1 class="center-text"> Passwort ändern </h1>
			<br><br>
		
	<form action="ChgUsrPass" method="post">
	
		<label for="oldpass">Altes Passwort:</label>
	    <input maxLength="45" class="form-control" type="password" name="oldpass" id="oldpass" required>
	    <br>
				
	    <label for="newpass1">Neues Passwort:</label>
	    <input maxLength="45" class="form-control" type="password" name="newpass1" id="newpass1" required>
	    <br>
	    				
	    <label for="newpass2">Neues Passwort wiederholen:</label>
	    <input maxLength="45" class="form-control" type="password" name="newpass2" id="newpass2" required>
	    <br>
						
		<div class="text-center">
		 <button class="btn btn-success" type="submit" value="submit"> Passwort ändern </button>
		
		</div>
	</form> 
	
	<p class="error-message center-text">
		${message }
	</p>
	
		
	
</div>


</jsp:body>
</t:genericpage>