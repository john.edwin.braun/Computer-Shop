<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<t:genericpage pageTitle="Kontodaten löschen">
	<jsp:body>
	
	<h1 class="text-center">Benutzerdaten löschen </h1>
	
	<form action="DelUsr" method="post">
	
	<div class="text-center">
	<br>
	 <button class="btn btn-danger" onclick="myFunction()"> Benutzerkonto löschen </button>
	</div>
	
		
		<p id="demo"></p>
		
		
		<script>
		/* Display alert */
		function myFunction() 
		{
			alert("Ihr Konto wurde gelöscht. Sie werden weitergeleitet");
		}
		
		</script>

	</form>
	
	</jsp:body>
</t:genericpage>
