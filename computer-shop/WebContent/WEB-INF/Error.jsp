<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>

<t:genericpage pageTitle="Sorry!">
	<jsp:body>
		<h1 class="text-center error-message" style="font-size:2em;">Ups! Es ist etwas schief gelaufen.</h1>
		<p class="text-center">${error }</p>
	</jsp:body>
</t:genericpage>