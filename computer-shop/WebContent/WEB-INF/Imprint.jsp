<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>

<t:genericpage pageTitle="Impressum">
	<jsp:body>
		<div class="container">
			<h1>Impressum</h1>

			<p>FHDW Computer GmbH</p>
			<p>Meisenstraße 92</p>
			<p>Deutschland 33607 Bielefeld</p>
			
			<p>Herr Max Mustermann</p>
			
			<p>Telefon: <a href="tel:+491520876881">+49 1520876881</a></p>
			
			
			<p>E-Mail: fhdw.computershop@info.de</p>
			
			<p>Registergericht: Musterstadt Deutschland</p>
			<p>Registernummer: 112767</p>
			<p>Stammkapital: EUR 192.061</p>
			
			<p>Umsatzsteuer-Id: DE999999999</p>
			
			<p>Verantwortlicher: FHDW Computer GmbH</p>
		</div>
		
	</jsp:body>
</t:genericpage>