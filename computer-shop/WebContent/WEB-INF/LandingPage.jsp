<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<t:genericpage pageTitle="Computer-Shop" >
	
	<jsp:body>
		<div class="container">
		
		<!-- If article was created, display message -->
		<c:if test="${articleCreated == 'true' }">
			<div class="alert alert-success alert-dismissible fade in text-center">
   		 		<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
				Artikel wurde erfolgreich erstellt und mit dem Bild in der Datenbank synchronisiert!
  			</div>
		</c:if>
		
		<!-- If order is placed, display message -->
		<c:if test="${placedOrder == 'true' }">
			<div class="alert alert-success alert-dismissible fade in text-center">
   		 		<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
				Ihre Bestellung wurde erfolgreich angelegt!
  			</div>
		</c:if>
			
			<h1 class="text-center" style="font-size:3em;"> 
			Herzlich Willkommen in unserem Computer-Shop.
			</h1>
			
			<h4 class="text-center"> Sie können bei uns die verschiedensten Artikel für Ihren Heim-Computer erwerben!</h4>
			
			<!-- If there are offers, display them -->
			<c:if test="${noOffers != true }">
				<div class="row">
					<br><br>
					<h1 class="text-center">
						Unsere Empfehlungen:
					</h1>
					<br>
					<div class="col-md-4">
						<div class="panel panel-default">
  							<div class="panel-heading">
  								<h3 class="text-center">
  								<b>
  									<a class="article-link" href="ArticleView?ArticleNo=${article1.articleNo }">${article1.name }</a>
  								</b>
  								</h3>
  							</div>
  							<div class="panel-body">
  								<a href="ArticleView?ArticleNo=${article1.articleNo }">
  									<img class="img-responsive img-fixed-height-big" src="${article1.pictureLink }" alt="${article1.name }">
  								</a>
  							</div>
  							<div class="panel-footer">
  								<h3 class="text-center">
  								<b>	
  									<a class="article-link" href="ArticleView?ArticleNo=${article1.articleNo }">${article1.price } EUR</a>
  								</b>
  								</h3>
  							</div>
						</div>
					</div>
					
					<div class="col-md-4">
						<div class="panel panel-default">
  							<div class="panel-heading">
  								<h3 class="text-center">
  								<b>
  									<a class="article-link" href="ArticleView?ArticleNo=${article2.articleNo }">${article2.name }</a>
  								</b>
  								</h3>
  							</div>
  							<div class="panel-body">
  								<a href="ArticleView?ArticleNo=${article2.articleNo }">
  									<img class="img-responsive img-fixed-height-big" src="${article2.pictureLink }" alt="${article2.name }">
  								</a>
  							</div>
  							<div class="panel-footer">
  								<h3 class="text-center">
  								<b>
  									<a class="article-link" href="ArticleView?ArticleNo=${article2.articleNo }">${article2.price } EUR</a>
  								</b>
  								</h3>
  							</div>
						</div>
					</div>
					
					<div class="col-md-4">
						<div class="panel panel-default">
  							<div class="panel-heading">
  								<h3 class="text-center">
  								<b>
  									<a class="article-link" href="ArticleView?ArticleNo=${article3.articleNo }">${article3.name }</a>
  								</b>
  								</h3>
  							</div>
  							<div class="panel-body">
  								<a href="ArticleView?ArticleNo=${article3.articleNo }">
  									<img class="img-responsive img-fixed-height-big" src="${article3.pictureLink }" alt="${article3.name }">
  								</a>
  							</div>
  							<div class="panel-footer">
  								<h3 class="text-center">
  								<b>
  									<a class="article-link" href="ArticleView?ArticleNo=${article3.articleNo }">${article3.price } EUR</a>
  								</b>
  								</h3>
  							</div>
						</div>
					</div>
				</div>				
			</c:if>
			
			
		</div>
		
		
	</jsp:body>

</t:genericpage>