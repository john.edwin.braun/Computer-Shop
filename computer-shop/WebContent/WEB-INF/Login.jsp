<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<t:genericpage pageTitle="Anmelden">
	
	<jsp:body>
		<div class="container">
			<h1 class="text-center"> Anmelden </h1>
			<br><br>
			
			<div class="row">
				<div class="col-md-4">
				</div>
				<div class="col-md-4">
					<form action="Login" method="post">
						<div class="form-group"> 
							<label class="label-big" for="email">E-Mail:</label>
							<input class="form-control " type="email" id="email" name="email">
							<br>
							<label class="label-big" for="password">Passwort:</label>
							<input class="form-control label-big" type="password" id="password" name="password">
						</div>
						<div class="text-center">
							<input type="hidden" name="destination" value="${destination }">
							<input class="btn label-big btn-success" type="submit" value="Anmelden" name="login">
						</div>
					</form>
					<br>
					
					<p class="text-center">
						Noch kein Konto? <a href="Registration">Hier registrieren!</a>
					</p>
			
					<p class="error-message text-center">
						${message }
					</p>
				</div>
				<div class="col-md-4">
				</div>
				
			</div>
			
		</div>
		
	</jsp:body>

</t:genericpage>