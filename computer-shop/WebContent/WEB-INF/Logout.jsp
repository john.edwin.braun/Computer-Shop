<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>

<t:genericpage pageTitle="Abgemeldet">
	
	<jsp:body>
		<h2 class="text-center">Sie haben sich erfolgreich abgemeldet.</h2>
	</jsp:body>

</t:genericpage>