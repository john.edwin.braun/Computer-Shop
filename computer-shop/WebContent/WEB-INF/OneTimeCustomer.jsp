<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>

<t:genericpage pageTitle="Einmal-Kunde">
	<jsp:body>
		<h1 class="text-center">Kunden-Infos</h1>
		
		<form action="OneTimeCustomer" method="post">
			<div class="row">
				<div class="col-md-3">
				</div>
				<div class="col-md-3">
					<h2>
						Persönliche Daten
					</h2>
					<div class="form-group">
						<br>
						<label for="title">Titel:</label>
						<input class="form-control" type="text" name="title" id="title">
						<br>
						
						<label for="formOfAdress">Anrede:</label>
						<select id="formOfAdress" name="formOfAddress" class="form-control">
		        			<option value="Herr">Herr</option>
		        			<option value="Frau">Frau</option>
		        			<option value="Anderes">Anderes</option>
	    				</select>
	    				<br>
	    				
	    				<label for="name">Nachname:</label>
	    				<input maxLength="45" class="form-control" type="text" name="name" id="name" required>
	    				<br>
	    				
	    				<label for="firstName">Vorname:</label>
	    				<input maxLength="45" class="form-control" type="text" name="firstName" id="firstName" required>
	    				<br>
	    				
	    				<label for="email">E-Mail:</label>
	    				<input maxLength="45" class="form-control" type="email" name="email" id="email" required>
	    				<br>
	    				
					</div>
				</div>
				<div class="col-md-3">
					<h2>
						Adresse
					</h2>
					<div class="form-group">
						<br>
						<label for="invoiceStreet">Straße:</label>
	    				<input maxLength="45" class="form-control" type="text" name="invoiceStreet" id="invoiceStreet" required>
	    				<br>
	    				
	    				<label for="invoiceHouseNumber">Hausnummer:</label>
	    				<input maxLength="45" class="form-control" type="text" name="invoiceHouseNumber" id="invoiceHouseNumber" required>
	    				<br>
	    				
	    				<label for="invoiceTown">Ort:</label>
	    				<input maxLength="45" class="form-control" type="text" name="invoiceTown" id="invoiceTown" required>
	    				<br>
	    				
	    				<label for="invoicePostalCode">PLZ:</label>
	    				<input maxLength="45" class="form-control" type="text" name="invoicePostalCode" id="invoicePostalCode" required>
	    				<br>
					</div>
				</div>
				<div class="col-md-3">
				</div>
			</div>
			
			<div class="text-center">
				<input class="btn label-big btn-success" type="submit" value="Weiter zur Bestellung" name="registration">
			</div>
			
		</form>
			<p class="error-message center-text">
				${message }
			</p>

	</jsp:body>
</t:genericpage>