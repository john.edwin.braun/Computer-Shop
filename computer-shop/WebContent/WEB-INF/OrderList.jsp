<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<t:genericpage pageTitle="Meine Bestellungen">
	
	<jsp:body>
		<div class="container">
			<h1 class="text-center">
				Meine Bestellungen
			</h1>
			
			<div class="col-md-2">
			</div>
			
			<div class="col-md-8">
				<c:set var="numberOfOrders" value="0"/>
				
				<!-- Looping through order list -->
				<c:forEach items="${orderList }" var="orders">
				
					<!-- Counting orders -->
					<c:set var="numberOfOrders" value="${loop.count }"/>
					<div class="row col-bottom-border">
						<h2>Vom: ${orders.creationDate } (Status: ${orders.status })</h2>
						<c:set var="priceOfOrder" value="0"/>
						
						<!-- Looping through order position list to find corresponding position to the current order -->
						<c:forEach items="${orderPositionList }" var="orderPositions">
							<c:if test="${orders.orderNo == orderPositions.orderNo }">
							
								<!-- Looping through artice list to find corresponding article to the current position -->
								<c:forEach items="${articleList }" var="articles">
									<c:if test="${orderPositions.articleNo == articles.articleNo }">
										<c:set var="priceOfOrder" value="${priceOfOrder + orderPositions.pricePerPiece * orderPositions.amount }"/>
										
										<div class="row">
											<div class="col-xs-2">
												<h4>${orderPositions.amount }x</h4>
											</div>
											<div class="col-xs-6">
												<h4>${articles.name }</h4>
											</div>
											<div class="col-xs-4">
												<h4>${orderPositions.pricePerPiece * orderPositions.amount } EUR</h4>
											</div>
										</div>
										
									</c:if>
								</c:forEach>
							</c:if>
						</c:forEach>
						
						<div class="row">
							<div class="col-xs-2">
							</div>
							<div class="col-xs-6">
								<h4><b>Summe:</b></h4>
							</div>
							<div class="col-xs-4">
								<h4>${priceOfOrder } EUR</h4>
							</div>
						</div>
						
						<div class="row">
							<div class="col-md-6">
								<br><h4>Lieferdatum: ${orders.deliveryDate }</h4>
							</div>
							<div class="col-md-6">
								<br><h4>Zahlungsart: ${orders.paymentMethod }</h4>
								<br>
								<br>
							</div>
						</div>
						
					</div>
				</c:forEach>
				
				<!-- If there are no orders, display message -->
				<c:if test="${numberOfOrders < 1 }">
				<h4 class="text-center">Sie haben noch keine Bestellungen bei uns.</h4>
				</c:if>
			</div>
			
			<div class="col-md-2">
			</div>
			
		</div>
		
	</jsp:body>

</t:genericpage>