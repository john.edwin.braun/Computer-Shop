<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<t:genericpage pageTitle="Bestellübersicht">
	<jsp:body>
	
		<div class="container">
			<h1 class="text-center">Ihre Bestellung</h1><br>
			<div class="row">
				<div class="col-md-2">
				</div>
				
				<div class="col-md-8">
				
					<c:set var="priceOfShoppingCart" value="0"/>
					<!-- Looping through shopping cart items -->
					<c:forEach items="${shoppingCartItems }" var="item" varStatus="loop">
						<!-- Counting positions and calculating price of shopping cart -->
						<c:set var="numberOfPositions" value="${loop.count }"/>
						<c:set var="priceOfShoppingCart" value="${priceOfShoppingCart + item.price * item.amount }"/>
						
						<div class="row col-bottom-border col-fixed-height">	
							<div class="col-md-5 col-xs-4">
								<a href="ArticleView?ArticleNo=${item.articleNo }">
									<img class="img-responsive img-fixed-height" src="${item.pictureLink }" alt="${item.articleName }">
								</a>
							</div>
							<div class="col-md-7 col-xs-8">
								<h3 class="text-overflow">${item.articleName }</h3>
								
								<h4>${item.price } EUR</h4>
								<h4>Anzahl: ${item.amount }</h4>
							</div>
						</div>
						
					</c:forEach>
					<h4 class="text-right">Versand: 
						<!-- If dispatch type is standard, display 0 extra costs. Otherwise display 3 extra costs -->
						<c:choose>
							<c:when test="${dispatchType == 'Standardversand (3-5 Tage)' }">
								0.00 EUR
							</c:when>
							<c:otherwise>
								3.00 EUR
							</c:otherwise>
						</c:choose>
					</h4>
					<h3 class="text-right"><b>Summe:
						<!-- If dispatch type is premium, add 3 EUR to price -->
						<c:choose>
							<c:when test="${dispatchType == 'Standardversand (3-5 Tage)' }">
								${priceOfShoppingCart } EUR
							</c:when>
							<c:otherwise>
								${priceOfShoppingCart + 3 } EUR
							</c:otherwise>
						</c:choose>
					</b></h3>	
				
				</div>
				
				<div class="col-md-2">
				</div>
			</div>
			
			<div class="row">
				<div class="col-md-2">
				</div>
				
				<div class="col-md-8">
					<br>
					<h1 class="text-center">
						Ihre Kontaktdaten:
					</h1>
					
					<div class="row">
						<div class="col-md-6">
							<h3 class="text-center">Lieferadresse:</h3>
							<p class="text-center">
								${deliveryFirstname } ${deliveryName }<br>
								${deliveryStreet } ${deliveryHouseNumber }<br>
								${deliveryPostalCode } ${deliveryTown }<br>
								Versandart: ${dispatchType }
							</p>
							
						</div>
						<div class="col-md-6">
							<h3 class="text-center">Rechnungsadresse:</h3>
							<p class="text-center">
								${sessionScope.customer.firstname } ${sessionScope.customer.name }<br>
								${sessionScope.customer.invoiceStreet } ${sessionScope.customer.invoiceHouseNumber }<br>
								${sessionScope.customer.invoicePostalCode } ${sessionScope.customer.invoiceTown }<br>
								Zahlungsmethode: ${paymentMethod }<br>
							</p>
						</div>
					</div>
				</div>
				
				<div class="col-md-2">
				</div>
			</div>
			
			<div class="text-center">
				<br><br>
				<form action="OrderOverview" method="post">
					<input type="hidden" name="customerNo" value="${sessionScope.customer.customerNo }">
					<input type="hidden" name="deliveryFirstname" value="${deliveryFirstname }">
					<input type="hidden" name="deliveryHouseNumber" value="${deliveryHouseNumber }">
					<input type="hidden" name="deliveryName" value="${deliveryName }">
					<input type="hidden" name="deliveryPostalCode" value="${deliveryPostalCode }">
					<input type="hidden" name="deliveryStreet" value="${deliveryStreet }">
					<input type="hidden" name="deliveryTown" value="${deliveryTown }">
					<input type="hidden" name="dispatchType" value="${dispatchType }">
					<input type="hidden" name="invoiceHouseNumber" value="${sessionScope.customer.invoiceHouseNumber }">
					<input type="hidden" name="invoicePostalCode" value="${sessionScope.customer.invoicePostalCode }">
					<input type="hidden" name="invoiceStreet" value="${sessionScope.customer.invoiceStreet }">
					<input type="hidden" name="invoiceTown" value="${sessionScope.customer.invoiceTown }">
					<input type="hidden" name="paymentMethod" value="${paymentMethod }">
					<input type="hidden" name="invoiceName" value="${sessionScope.customer.name }">
					<input type="hidden" name="invoiceFirstname" value="${sessionScope.customer.firstname }">
					
					<button class="btn btn-success btn-lg" type="submit">
						Jetzt zahlungspflichtig bestellen.
					</button>
				</form>
			</div>
			
				
		</div>	
		
	</jsp:body>
</t:genericpage>