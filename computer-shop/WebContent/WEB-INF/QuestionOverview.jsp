<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<t:genericpage pageTitle="Q and A">
	<jsp:body>
	
		<!-- If question was asked, display message -->
		<c:if test="${questionAsked == 'true' }">
			<div class="alert alert-success alert-dismissible fade in text-center">
   		 		<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
				Wir beantworten deine Frage so schnell wie möglich!
  			</div>
		</c:if>
	
		<h1 class="text-center">Fragen und Antworten</h1>
		<h2 class="text-center"> Sie haben die Fragen und wir haben die Antworten!</h2>
			
		<div class="container">
			<br>
			<div class="row">
				<div class="col-md-2">
				</div>
				<div class="col-md-8">
				
					<div class="panel-group" id="accordion">
					
						<!-- Looping through question list and display them -->
						<c:forEach items="${questionList }" var="item" varStatus="loop">
		    				<div class="panel panel-default">
		      					<div class="panel-heading">
		        					<h4 class="panel-title">
		          						<a data-toggle="collapse" data-parent="#accordion" href="#collapse${loop.count}">
		          							${item.title} <span class="glyphicon glyphicon-collapse-down"></span>
		          						</a>
		       						</h4>
		      					</div>
		      					<div id="collapse${loop.count}" class="panel-collapse collapse">
		        					<div class="panel-body">
		        						<p>Gefragt von: ${item.questioner } am: ${item.date }</p>
		        						<h4>Frage:</h4>
		        						<h4>${item.question }</h4>
		        						<br>
		        						<h4>Antwort:</h4>
		        						<h4>${item.answer }</h4>
		        					</div>
		      					</div>
		    				</div>
	    				</c:forEach>
	    				
	    			</div> 				
				</div>
				<div class="col-md-2">
				</div>
			</div>
			
			<div class="row">
				<br>
				<div class="col-md-2">
				</div>
				<div class="col-md-8">
				
					<h2>Sie haben selbst eine Frage?</h2>
					
					<form action="QuestionOverview" method="post">
						<div class="form-group">
							<label for="title">Thema:</label>
							<input class="form-control" type="text" name="title" id="title" maxlength="45" required>
							<br>
   							<label for="question">Frage:</label>
   							<textarea rows="5" class="form-control" id="question" name="question" placeholder="Maximal 400 Zeichen..." maxlength="400" required></textarea>
		    				<br>
		    				<label for="name">Name:</label>
							<input class="form-control" type="text" name="name" id="name" maxlength="45">
		    			</div>
		    			
		    			<div class="text-center">
		    				<input type="hidden" name="questionAsked" value="true">
		    				<input class="btn label-big btn-success" type="submit" value="Jetzt Fragen!" name="submit">
		    				<br>
		    				<br>
		    			</div>
		    			
					</form>
				
				</div>
				<div class="col-md-2">
				</div>
			</div>
			
		</div>
	</jsp:body>
</t:genericpage>