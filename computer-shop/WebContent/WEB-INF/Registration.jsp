<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>

<t:genericpage pageTitle="Registrierung">
	<jsp:body>
		<h1 class="text-center">Kunden-Infos</h1>
		
		
		<form action="Registration" method="post" >
			<div class="row">
				<div class="col-md-3">
				</div>
				<div class="col-md-3">
					<h2>
						Persönliche Daten
					</h2>
					<div class="form-group">
						<br>
						<label for="title">Titel:</label>
						<input class="form-control" type="text" name="title" id="title" value="${title}">
						<br>
						
						<label for="formOfAdress">Anrede:</label>
						<select id="formOfAdress" name="formOfAddress" class="form-control">
		        			<option value="Herr">Herr</option>
		        			<option value="Frau">Frau</option>
		        			<option value="Anderes">Anderes</option>
	    				</select>
	    				<br>
	    				
	    				<label for="name">Nachname:</label>
	    				<input maxLength="45" class="form-control" type="text" name="name" id="name" value="${name}" >
	    				<br>
	    				
	    				<label for="firstName">Vorname:</label>
	    				<input maxLength="45" class="form-control" type="text" name="firstName" id="firstName" value="${firstName}" >
	    				<br>
	    				
	    				<label for="email">E-Mail:</label>
	    				<input maxLength="45" class="form-control" type="email" name="email" id="email" value="${email}" >
	    				<br>
	    				
	    				<label for="password">
	    				<b>Passwort: 
	    				<span class ="glyphicon glyphicon-question-sign" data-toggle="tooltip" title="Das Passwort muss genau 8 Zeichen lang sein.
	    				Davon mind. 1 Großbuchstabe (A-Z), 1 Kleinbuchstabe (a-z), 1 Ziffern (0-9), 1 Sonderzeichen (%/!,.)"></span>
	    				</b>
	    				</label>
	    				<input maxLength="45" class="form-control" type="password" name="password" id="password" >
	    				
	    				<br>
					</div>
				</div>
				<div class="col-md-3">
					<h2>
						Adresse
					</h2>
					<div class="form-group">
						<br>
						<label for="invoiceStreet">Straße:</label>
	    				<input maxLength="45" class="form-control" type="text" name="invoiceStreet" id="invoiceStreet" value="${invoiceStreet}" >
	    				<br>
	    				
	    				<label for="invoiceHouseNumber">Hausnummer:</label>
	    				<input maxLength="45" class="form-control" type="text" name="invoiceHouseNumber" id="invoiceHouseNumber" value="${invoiceHouseNumber}" >
	    				<br>
	    				
	    				<label for="invoiceTown">Ort:</label>
	    				<input maxLength="45" class="form-control" type="text" name="invoiceTown" id="invoiceTown" value="${invoiceTown}" >
	    				<br>
	    				
	    				<label for="invoicePostalCode">PLZ:</label>
	    				<input maxLength="45" class="form-control" type="text" name="invoicePostalCode" id="invoicePostalCode" value="${invoicePostalCode}" >
	    				<br>
					</div>
				</div>
				<div class="col-md-3">
				</div>
			</div>
			
			<div class="text-center">
				<input class="btn label-big btn-success" type="submit" value="Registrieren" name="registration">
			</div>
		</form>
		
		<p class="text-center error-message">
			<br><br>
			${message }
		</p>
		
		<script>
			/* Tooltip display for Password */
			$(document).ready(function(){
			    $('[data-toggle="tooltip"]').tooltip();   
			});
		</script>
		
	</jsp:body>
</t:genericpage>