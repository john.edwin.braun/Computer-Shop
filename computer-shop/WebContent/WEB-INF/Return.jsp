<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<t:genericpage pageTitle="Reklamation">
	<jsp:body>
	
	<div class="container">
		<div class="row">
			<div class="col-md-2">
			</div>
			<div class="col-md-8">
				<h1> Reklamation </h1>
				<br>
				<p> Sie können falsche, beschädigte oder defekte Artikel innerhalb von zwei Jahren ab Erhalt der Ware reklamieren. Abhängig vom Reklamationszeitpunkt und von der Art des Artikels gibt es verschiedene Optionen für die Reklamation. </p>
				<br>
				<p> Bitte drucken Sie dazu das folgende Dokument aus, befüllen es mit den notwendigen Informationen und legen es Ihrer Bestellung bei.</p>		
				<br>
				<h2 class="text-center"> <a class="article-link" href="Documents/Retoure.pdf" download="Retourenschein">
					Retourenvorlage<br><span class="glyphicon glyphicon-download-alt"></span>
					</a>
				</h2>
			</div>
			<div class="col-md-2">
			</div>
		</div>
	</div>
	
		
		
		
		
	</jsp:body>
</t:genericpage>