<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<t:genericpage pageTitle="Warenkorb">
	<jsp:body>
		<div class="container">
		<h1 class="text-center">Warenkorb</h1><br>
			
			<div class="row">
				<div class="col-md-2">
				</div>
				
				<div class="col-md-8">
				
					<c:set var="priceOfShoppingCart" value="0"/>
					<!-- Looping through shopping cart item list and display them -->
					<c:forEach items="${shoppingCartItems }" var="item" varStatus="loop">
						<c:set var="numberOfPositions" value="${loop.count }"/>
						<c:set var="priceOfShoppingCart" value="${priceOfShoppingCart + item.price * item.amount }"/>
						
						<div class="row col-bottom-border col-fixed-height">	
							<div class="col-md-5 col-xs-4">
								<a href="ArticleView?ArticleNo=${item.articleNo }">
									<img class="img-responsive img-fixed-height" src="${item.pictureLink }" alt="${item.articleName }">
								</a>
							</div>
							<div class="col-md-7 col-xs-8">
								<h3 class="text-overflow">${item.articleName }</h3>
								
								<h4>${item.price } EUR Anzahl: ${item.amount }</h4>
								
								<form action="ShoppingCart" method="post">
									<button name="submit" value="deletePosition_${item.positionNo }" class="delete-button btn-float-left label-big" type="submit">
										<span class="glyphicon glyphicon-trash"></span>
									</button>
								</form>
							</div>
						</div>
						
					</c:forEach>
					
					<!-- If there are articles in the shopping cart, display the price -->
					<c:if test="${numberOfPositions > 0 }">
						<h3 class="text-right"><b>Summe: ${priceOfShoppingCart } EUR</b></h3>	
					</c:if>
					
					<!-- If there are articles in the shopping cart, display buttons 'To checkout' and 'delete all products' -->
					<c:if test="${numberOfPositions > 0 }">
						<div class="row">
						<br><br>
							<div class="col-md-6">
									<form action="ShoppingCart" method="post">
										<button name="submit" value="deleteAll" class="btn-float-left btn btn-danger" type="submit">
											Alle Produkte entfernen
										</button>
									</form>
							</div>
							<div class="col-md-6">
									<a href="Checkout" class="btn-float-right btn btn-success" role="button">Zur Kasse</a> 
							</div>
						</div>
					</c:if>
				
				</div>
				
				<div class="col-md-2">
				</div>
			</div>
			
			
			<p class="text-center error-message">${message }</p>
			
			
		</div>
		
	</jsp:body>
</t:genericpage>