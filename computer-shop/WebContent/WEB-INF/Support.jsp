<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>

<t:genericpage pageTitle="Beratung">
	<jsp:body>
		<h1 class="text-center">Unsere Berater</h1>
		
		<div class="container">
			<br>
			<div class="row">
				<div class="col-md-2">
				</div>
				<div class="col-md-8">
				
					<div class="panel-group">
					    <div class="panel panel-default">
							<div class="panel-heading">
								<h4 class="panel-title">
									<a data-toggle="collapse" href="#collapse1">
										Desktop-PCs <span class="glyphicon glyphicon-collapse-down"></span>
									</a>
						        </h4>
						    </div>
						    <div id="collapse1" class="panel-collapse collapse in">
						    	<div class="panel-body">
						    		<div class="row">
						    			<div class="col-md-5">
						    				<img class="img-responsive" src="Pictures/Peter_Beispiel.jpg" alt="Dietmar" />
						    			</div>
						    			<div class="col-md-7">
						    				<h4>Unser Berater: Max Mustermann</h4>
						    				<p>Wir beraten Sie gerne dazu, was Sie für einen Computer benötigen. Von der Hardware bis zur Software!</p>
						    				<br>
						    				<p>E-Mail: <a href="mailto:max.mustermann@computershop.de">max.mustermann@computershop.de</a></p>
						    				<br>
						    				<p>Telefon: <a href="tel:+4957112345">0571 / 12345</a></p>
						    			</div>
						    		</div>
						    	</div>
						    </div>
						</div>
					</div>
					
					<div class="panel-group">
				    	<div class="panel panel-default">
							<div class="panel-heading">
								<h4 class="panel-title">
									<a data-toggle="collapse" href="#collapse2">
									Zubehör <span class="glyphicon glyphicon-collapse-down"></span>
									</a>
				       	 		</h4>
				    		</div>
				    		<div id="collapse2" class="panel-collapse collapse">
				    			<div class="panel-body">
				    				<div class="row">
				    					<div class="col-md-5">
				    						<img class="img-responsive" src="Pictures/Max_Mustermann.jpg" alt="Dietmar" />
				    					</div>
				    					<div class="col-md-7">
						    				<h4>Unser Berater: Peter Beispiel</h4>
						    				<p>Wir empfehlen Ihnen gerne das richtige Zubehör für einen Arbeitsplatz,
						    					der Ihren Ansprüchen gerecht werden kann!</p>
						    				<br>
						    				<p>E-Mail: <a href="mailto:peter.beispiel@computershop.de">peter.beispiel@computershop.de</a></p>
						    				<br>
						    				<p>Telefon: <a href="tel:+4957112346">0571 / 12346</a></p>
				    					</div>
				    				</div>
				    			</div>
				    		</div>
				    	</div>
				    </div>
						    
					<div class="panel-group">
						<div class="panel panel-default">
							<div class="panel-heading">
								<h4 class="panel-title">
									<a data-toggle="collapse" href="#collapse3">
										Notebooks <span class="glyphicon glyphicon-collapse-down"></span>
									</a>
						        </h4>
						    </div>
						    <div id="collapse3" class="panel-collapse collapse">
						    	<div class="panel-body">
						    		<div class="row">
						    			<div class="col-md-5">
						    				<img class="img-responsive" src="Pictures/Jannik_Vorlage.jpg" alt="Dietmar" />
						    			</div>
						    			<div class="col-md-7">
						    				<h4>Unser Berater: Jannik Vorlage</h4>
						    				<p>Wenn Sie den perfekten Laptop für ihre Bedürfnisse suchen, bin ich der richtige!</p>
						    				<br>
						    				<p>E-Mail: <a href="mailto:jannik.vorlage@computershop.de">jannik.vorlage@computershop.de</a></p>
						    				<br>
						    				<p>Telefon: <a href="tel:+4957112347">0571 / 12347</a></p>
						    			</div>
						    		</div>
						    	</div>
						    </div> 
					    </div>
					</div>
				</div>
				
				<div class="col-md-2">
				</div>
			</div>
		</div>
		
		
	</jsp:body>
</t:genericpage>