<%@tag description="Overall Page template" pageEncoding="UTF-8"%>
<%@attribute name="pageTitle" required="true" %>
<%@attribute name="search" required="false" %>
<%@attribute name="category" required="false" %>
<%@attribute name="sortCrit" required="false" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<!-- This is the standard page for all pages of the web application -->

<!DOCTYPE html>
<html>
	<head>
	    <meta charset="UTF-8">
	    <meta name="viewport" content="width=device-width, initial-scale=1.0">
	    <link rel="stylesheet" href="${pageContext.request.contextPath}/bootstrap/css/bootstrap.css" >
	    <script src="${pageContext.request.contextPath}/jquery/jquery.js"></script>
	    <script src="${pageContext.request.contextPath}/bootstrap/js/bootstrap.js"></script>
	    
	    <link href="${pageContext.request.contextPath}/RespDesign.css" rel="stylesheet">
	    
	    <title>${pageTitle }</title>
	</head>
	
 	<body>
	 	<!-- Standard category for the search -->
	 	<c:if test="${category=='' }">
	 		<c:set var="category" value="All"/>
	 	</c:if>
	 	 
	 	<!-- Standard sort criteria for the search -->
	 	<c:if test="${sortCrit=='' }">
	 		<c:set var="category" value="Name asc"/>
	 	</c:if>
	 	
 		<!-- Navigation bar -->
		<nav class="navbar navbar-inverse">
			<div class="container">
				<div class="navbar-header">
					<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
						<span class="glyphicon glyphicon-home glyphicon-white"></span>
				    </button>
				    <a class="navbar-brand" href="${pageContext.request.contextPath}/LandingPage">Computer-Shop</a>
				</div>

				<div class="collapse navbar-collapse" id="myNavbar">

				<ul class="nav navbar-nav">
					<li><a href="${pageContext.request.contextPath}/Support">Beratung</a></li>
					<li class="dropdown">
						<a class="dropdown-toggle" data-toggle="dropdown" href="#">Produkte<span class="caret"></span></a>
						<ul class="dropdown-menu">
							<li><a href="${pageContext.request.contextPath}/ArticleOverview?Category=Accessory&sortCrit=Name asc">Zubehör</a></li>
							<li><a href="${pageContext.request.contextPath}/ArticleOverview?Category=PC&sortCrit=Name asc">PC</a></li>
							<li><a href="${pageContext.request.contextPath}/ArticleOverview?Category=Monitor&sortCrit=Name asc">Monitore</a></li>
							<li><a href="${pageContext.request.contextPath}/ArticleOverview?Category=Laptop&sortCrit=Name asc">Laptops</a></li>
						</ul>
					</li> 					
				</ul>
				
				<form class="navbar-form navbar-left" action="ArticleOverview" method="get">
     				<div class="input-group">
     				<input type="hidden" name="Category" value="${category }">
     				<input type="hidden" name="sortCrit" value="${sortCrit }">
     					<input type="text" class="form-control" placeholder="Suche..." name="search">
        				<div class="input-group-btn">
      						<button class="btn btn-default" type="submit">
        						<i class="glyphicon glyphicon-search"></i>
     						</button>
    					</div>	
      				</div>
    			</form>
				
				<ul class="nav navbar-nav navbar-right">
					<li><a href="ShoppingCart"><span class="glyphicon glyphicon-shopping-cart"></span> Warenkorb</a></li>
					<c:choose>
						<c:when test="${sessionScope.user == null || sessionScope.user == ''}">
						<!-- When user is not logged in -->
							<li><a href="Login"><span class="glyphicon glyphicon-log-in"></span> Anmelden</a></li>
						</c:when>
						
						
						<c:when test="${sessionScope.admin == 'true'}">
						<!-- When admin is logged in -->
							<li class="dropdown">
								<a class="dropdown-toggle" data-toggle="dropdown" href="#">
								${sessionScope.customer.firstname} ${sessionScope.customer.name } 
								<span class="glyphicon glyphicon-user"></span><span class="caret"></span></a>
								<ul class="dropdown-menu">
										<li class="dropdown-header">Mein Bereich</li>
										<li><a href="Logout"><span class="glyphicon glyphicon-log-out"></span> Abmelden</a></li>
										<li><a href="${pageContext.request.contextPath}/OrderList">Meine Bestellungen</a></li>
										<li><a href="${pageContext.request.contextPath}/ChgUsrDetails">Nutzerdetails ändern</a></li>
										<li><a href="${pageContext.request.contextPath}/ChgUsrMail">E-Mail ändern</a></li>
										<li><a href="${pageContext.request.contextPath}/ChgUsrPass">Passwort ändern</a></li>
										<li><a href="${pageContext.request.contextPath}/ChgUsrAdress">Adresse ändern</a></li>
										<li><a href="${pageContext.request.contextPath}/DelUsr">Kontodaten löschen</a></li>
										<li class="dropdown-header">Administration</li>
										<li><a href="${pageContext.request.contextPath}/ArticleCreation">Neuen Artikel erstellen</a></li>
										<li><a href="${pageContext.request.contextPath}/AdminArticleOverview">Artikelverwaltung</a></li>
										<li><a href="${pageContext.request.contextPath}/AnswerQuestions">Fragen beantworten</a></li>
								</ul>
							</li>
						</c:when>
						
						
						<c:otherwise>
						<!-- When normal user is logged in -->
							<li class="dropdown">
								<a class="dropdown-toggle" data-toggle="dropdown" href="#">
								${sessionScope.customer.firstname} ${sessionScope.customer.name } 
								<span class="glyphicon glyphicon-user"></span><span class="caret"></span></a>
								<ul class="dropdown-menu">
										<li class="dropdown-header">Mein Bereich</li>
										<li><a href="Logout"><span class="glyphicon glyphicon-log-out"></span> Abmelden</a></li>
										<li><a href="${pageContext.request.contextPath}/OrderList">Meine Bestellungen</a></li>
										<li><a href="${pageContext.request.contextPath}/ChgUsrDetails">Nutzerdetails ändern</a></li>
										<li><a href="${pageContext.request.contextPath}/ChgUsrMail">E-Mail ändern</a></li>
										<li><a href="${pageContext.request.contextPath}/ChgUsrPass">Passwort ändern</a></li>
										<li><a href="${pageContext.request.contextPath}/ChgUsrAdress">Adresse ändern</a></li>
										<li><a href="${pageContext.request.contextPath}/DelUsr">Kontodaten löschen</a></li>
								</ul>
							</li>
						</c:otherwise>
					</c:choose>
				</ul>
				</div>
			</div>
		</nav>


	    <div class="container-fluid" id="body">
	    	<jsp:doBody/>
	    </div>

	  
	    <footer class="footer">
      		<div class="container">
      			<div class="row">
      			<div class="col-xs-1">
      			</div>
      			<div class="col-xs-3">
      			<span class="text-muted"><a href="${pageContext.request.contextPath}/QuestionOverview">FAQ</a></span>
      			</div>
      			<div class="col-xs-4">
      			<span class="text-muted"><a href="${pageContext.request.contextPath}/Imprint">Impressum</a></span>
      			</div>
      			<div class="col-xs-3">
      			<span class="text-muted"><a href="${pageContext.request.contextPath}/Return">Retoure</a></span>
      			</div>
      			<div class="col-xs-1">
      			</div>
      			</div>
      		</div>
   		 </footer>
	    
    
 	</body>
</html>