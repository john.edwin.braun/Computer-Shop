<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>

<t:genericpage pageTitle="FileUpload!">
	<jsp:body>
	
	<!-- Source: https://www.codejava.net/java-ee/servlet/java-file-upload-example-with-servlet-30-api -->
	
<div class="container">
			<h1 class="text-center">Fügen Sie dem Artikel noch ein passendes Bild hinzu! <br> Schritt 2/2</h1>
			<div class="row">
				<div class="col-md-2">
				</div>
				<div class="col-md-8">
				<br>
				<form  method="post" action="UploadServlet" enctype="multipart/form-data">
				
				<div class="input-group">
	                <label class="input-group-btn">
	                    <span class="btn btn-primary">
	                        Browse&hellip; <input style="display:none;" type="file" name="file" size="60" required accept="image/*" />
	                    </span>
	                </label>
	                <input type="text" class="form-control" readonly>
	            </div>
				
   					
					<br />
					<br>
					<input class="btn btn-success label-big"type="submit" value="Artikel anlegen (2/2)" />
				</form>
				</div>
				<div class="col-md-2">
				</div>
			</div>
		</div>
	
	<script>
		$(function() {
	
		  // We can attach the `fileselect` event to all file inputs on the page
		  $(document).on('change', ':file', function() {
		    var input = $(this),
		        numFiles = input.get(0).files ? input.get(0).files.length : 1,
		        label = input.val().replace(/\\/g, '/').replace(/.*\//, '');
		    input.trigger('fileselect', [numFiles, label]);
		  });
		
		  // We can watch for our custom `fileselect` event like this
		  $(document).ready( function() {
		      $(':file').on('fileselect', function(event, numFiles, label) {
		
		          var input = $(this).parents('.input-group').find(':text'),
		              log = numFiles > 1 ? numFiles + ' files selected' : label;
		
		          if( input.length ) {
		              input.val(log);
		          } else {
		              if( log ) alert(log);
		          }
		
		      });
		  });
		});
	  </script>
  

	
	
	</jsp:body>
</t:genericpage>