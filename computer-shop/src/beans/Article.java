package beans;

/**
 * The Class Article. It represents one row of the table 'articles' in the database.
 * It also contains the average stars of the reviews for this article.
 * 
 * @author John Edwin Braun
 * 
 */
public class Article {
	
	/** The article no. */
	private String articleNo;
	
	/** The name. */
	private String name;
	
	/** The description. */
	private String description;
	
	/** The price. */
	private String price;
	
	/** The category. */
	private String category;
	
	/** The picture link. */
	private String pictureLink;
	
	/** The minimum stock. */
	private String minStock;
	
	/** The current stock. */
	private String currentStock;
	
	/** The status. */
	private String status;
	
	/** The average stars. */
	private String averageStars;
	
	/**
	 * Gets the picture link.
	 *
	 * @return the picture link
	 */
	public String getPictureLink() {
		return pictureLink;
	}
	
	/**
	 * Sets the picture link.
	 *
	 * @param pictureLink the new picture link
	 */
	public void setPictureLink(String pictureLink) {
		this.pictureLink = pictureLink;
	}
	
	/**
	 * Gets the article no.
	 *
	 * @return the article no
	 */
	public String getArticleNo() {
		return articleNo;
	}
	
	/**
	 * Sets the article no.
	 *
	 * @param articleNo the new article no
	 */
	public void setArticleNo(String articleNo) {
		this.articleNo = articleNo;
	}
	
	/**
	 * Gets the name.
	 *
	 * @return the name
	 */
	public String getName() {
		return name;
	}
	
	/**
	 * Sets the name.
	 *
	 * @param name the new name
	 */
	public void setName(String name) {
		this.name = name;
	}
	
	/**
	 * Gets the description.
	 *
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}
	
	/**
	 * Sets the description.
	 *
	 * @param description the new description
	 */
	public void setDescription(String description) {
		this.description = description;
	}
	
	/**
	 * Gets the price.
	 *
	 * @return the price
	 */
	public String getPrice() {
		return price;
	}
	
	/**
	 * Sets the price.
	 *
	 * @param price the new price
	 */
	public void setPrice(String price) {
		this.price = price;
	}
	
	/**
	 * Gets the category.
	 *
	 * @return the category
	 */
	public String getCategory() {
		return category;
	}
	
	/**
	 * Sets the category.
	 *
	 * @param category the new category
	 */
	public void setCategory(String category) {
		this.category = category;
	}
	
	/**
	 * Gets the minimum stock.
	 *
	 * @return the minimum stock
	 */
	public String getMinStock() {
		return minStock;
	}
	
	/**
	 * Sets the minimum stock.
	 *
	 * @param minStock the new minimum stock
	 */
	public void setMinStock(String minStock) {
		this.minStock = minStock;
	}	
	
	/**
	 * Gets the current stock.
	 *
	 * @return the current stock
	 */
	public String getCurrentStock() {
		return currentStock;
	}
	
	/**
	 * Sets the current stock.
	 *
	 * @param currentStock the new current stock
	 */
	public void setCurrentStock(String currentStock) {
		this.currentStock = currentStock;
	}
	
	/**
	 * Gets the status.
	 *
	 * @return the status
	 */
	public String getStatus() {
		return status;
	}
	
	/**
	 * Sets the status.
	 *
	 * @param status the new status
	 */
	public void setStatus(String status) {
		this.status = status;
	}
	
	/**
	 * Gets the average stars.
	 *
	 * @return the average stars
	 */
	public String getAverageStars() {
		return averageStars;
	}
	
	/**
	 * Sets the average stars.
	 *
	 * @param averageStars the new average stars
	 */
	public void setAverageStars(String averageStars) {
		this.averageStars = averageStars;
	}
	
}