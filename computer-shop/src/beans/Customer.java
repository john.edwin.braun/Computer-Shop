package beans;

/**
 * The Class Customer. It represents one row of the table 'users' in the database.
 * 
 * @author John Edwin Braun
 * 
 */
public class Customer {
	
	/** The customer Number. */
	private String customerNo;
	
	/** The email. */
	private String email;
	
	/** The password. */
	private String password;
	
	/** The name. */
	private String name;
	
	/** The firstname. */
	private String firstname;
	
	/** The title. */
	private String title;
	
	/** The form of address. */
	private String formOfAddress;
	
	/** The invoice street. */
	private String invoiceStreet;
	
	/** The invoice house number. */
	private String invoiceHouseNumber;
	
	/** The invoice postal code. */
	private String invoicePostalCode;
	
	/** The invoice town. */
	private String invoiceTown;
	
	/** The one time customer. */
	private String oneTimeCustomer;
	
	/** The admin. */
	private String admin;
	
	/**
	 * Gets the email.
	 *
	 * @return the email
	 */
	public String getEmail() {
		return email;
	}
	
	/**
	 * Sets the email.
	 *
	 * @param email the new email
	 */
	public void setEmail(String email) {
		this.email = email;
	}
	
	/**
	 * Gets the one time customer.
	 *
	 * @return the one time customer
	 */
	public String getOneTimeCustomer() {
		return oneTimeCustomer;
	}
	
	/**
	 * Sets the one time customer.
	 *
	 * @param oneTimeCustomer the new one time customer
	 */
	public void setOneTimeCustomer(String oneTimeCustomer) {
		this.oneTimeCustomer = oneTimeCustomer;
	}
	
	/**
	 * Gets the customer Number.
	 *
	 * @return the customer Number
	 */
	public String getCustomerNo() {
		return customerNo;
	}
	
	/**
	 * Sets the customer Number.
	 *
	 * @param customerNo the new customer Number
	 */
	public void setCustomerNo(String customerNo) {
		this.customerNo = customerNo;
	}
	
	/**
	 * Gets the password.
	 *
	 * @return the password
	 */
	public String getPassword() {
		return password;
	}
	
	/**
	 * Sets the password.
	 *
	 * @param password the new password
	 */
	public void setPassword(String password) {
		this.password = password;
	}
	
	/**
	 * Gets the name.
	 *
	 * @return the name
	 */
	public String getName() {
		return name;
	}
	
	/**
	 * Sets the name.
	 *
	 * @param name the new name
	 */
	public void setName(String name) {
		this.name = name;
	}
	
	/**
	 * Gets the firstname.
	 *
	 * @return the firstname
	 */
	public String getFirstname() {
		return firstname;
	}
	
	/**
	 * Sets the firstname.
	 *
	 * @param firstname the new firstname
	 */
	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}
	
	/**
	 * Gets the title.
	 *
	 * @return the title
	 */
	public String getTitle() {
		return title;
	}
	
	/**
	 * Sets the title.
	 *
	 * @param title the new title
	 */
	public void setTitle(String title) {
		this.title = title;
	}
	
	/**
	 * Gets the form of address.
	 *
	 * @return the form of address
	 */
	public String getFormOfAddress() {
		return formOfAddress;
	}
	
	/**
	 * Sets the form of address.
	 *
	 * @param formOfAddress the new form of address
	 */
	public void setFormOfAddress(String formOfAddress) {
		this.formOfAddress = formOfAddress;
	}
	
	
	/**
	 * Gets the invoice street.
	 *
	 * @return the invoice street
	 */
	public String getInvoiceStreet() {
		return invoiceStreet;
	}
	
	/**
	 * Sets the invoice street.
	 *
	 * @param invoiceStreet the new invoice street
	 */
	public void setInvoiceStreet(String invoiceStreet) {
		this.invoiceStreet = invoiceStreet;
	}
	
	/**
	 * Gets the invoice house number.
	 *
	 * @return the invoice house number
	 */
	public String getInvoiceHouseNumber() {
		return invoiceHouseNumber;
	}
	
	/**
	 * Sets the invoice house number.
	 *
	 * @param invoiceHouseNumber the new invoice house number
	 */
	public void setInvoiceHouseNumber(String invoiceHouseNumber) {
		this.invoiceHouseNumber = invoiceHouseNumber;
	}
	
	/**
	 * Gets the invoice postal code.
	 *
	 * @return the invoice postal code
	 */
	public String getInvoicePostalCode() {
		return invoicePostalCode;
	}
	
	/**
	 * Sets the invoice postal code.
	 *
	 * @param invoicePostalCode the new invoice postal code
	 */
	public void setInvoicePostalCode(String invoicePostalCode) {
		this.invoicePostalCode = invoicePostalCode;
	}
	
	/**
	 * Gets the invoice town.
	 *
	 * @return the invoice town
	 */
	public String getInvoiceTown() {
		return invoiceTown;
	}
	
	/**
	 * Sets the invoice town.
	 *
	 * @param invoiceTown the new invoice town
	 */
	public void setInvoiceTown(String invoiceTown) {
		this.invoiceTown = invoiceTown;
	}
	
	/**
	 * Gets the admin.
	 *
	 * @return the admin
	 */
	public String getAdmin() {
		return admin;
	}
	
	/**
	 * Sets the admin.
	 *
	 * @param admin the new admin
	 */
	public void setAdmin(String admin) {
		this.admin = admin;
	}
}
