package beans;

/**
 * The Class Order. It represents one row of the table 'orders' in the database.
 * 
 * @author John Edwin Braun
 * 
 */
public class Order {
	
	/** The order number. */
	private String orderNo;
	
	/** The customer number. */
	private String customerNo;
	
	/** The status. */
	private String status;
	
	/** The delivery street. */
	private String deliveryStreet;
	
	/** The delivery house number. */
	private String deliveryHouseNumber;
	
	/** The delivery postal code. */
	private String deliveryPostalCode;
	
	/** The delivery town. */
	private String deliveryTown;
	
	/** The invoice street. */
	private String invoiceStreet;
	
	/** The invoice house number. */
	private String invoiceHouseNumber;
	
	/** The invoice postal code. */
	private String invoicePostalCode;
	
	/** The invoice town. */
	private String invoiceTown;
	
	/** The creation date. */
	private String creationDate;
	
	/** The payment method. */
	private String paymentMethod;
	
	/** The dispatch type. */
	private String dispatchType;
	
	/** The delivery name. */
	private String deliveryName;
	
	/** The delivery firstname. */
	private String deliveryFirstname;
	
	/** The invoice name. */
	private String invoiceName;
	
	/** The invoice firstname. */
	private String invoiceFirstname;
	
	/** The delivery date. */
	private String deliveryDate;
	
	/**
	 * Gets the delivery date.
	 *
	 * @return the delivery date
	 */
	public String getDeliveryDate() {
		return deliveryDate;
	}
	
	/**
	 * Sets the delivery date.
	 *
	 * @param deliveryDate the new delivery date
	 */
	public void setDeliveryDate(String deliveryDate) {
		this.deliveryDate = deliveryDate;
	}
	
	/**
	 * Gets the invoice name.
	 *
	 * @return the invoice name
	 */
	public String getInvoiceName() {
		return invoiceName;
	}
	
	/**
	 * Sets the invoice name.
	 *
	 * @param invoiceName the new invoice name
	 */
	public void setInvoiceName(String invoiceName) {
		this.invoiceName = invoiceName;
	}
	
	/**
	 * Gets the invoice firstname.
	 *
	 * @return the invoice firstname
	 */
	public String getInvoiceFirstname() {
		return invoiceFirstname;
	}
	
	/**
	 * Sets the invoice firstname.
	 *
	 * @param invoiceFirstname the new invoice firstname
	 */
	public void setInvoiceFirstname(String invoiceFirstname) {
		this.invoiceFirstname = invoiceFirstname;
	}
	
	/**
	 * Gets the order number.
	 *
	 * @return the order number
	 */
	public String getOrderNo() {
		return orderNo;
	}
	
	/**
	 * Sets the order number.
	 *
	 * @param orderNo the new order number
	 */
	public void setOrderNo(String orderNo) {
		this.orderNo = orderNo;
	}
	
	/**
	 * Gets the customer number.
	 *
	 * @return the customer number
	 */
	public String getCustomerNo() {
		return customerNo;
	}
	
	/**
	 * Sets the customer number.
	 *
	 * @param customerNo the new customer number
	 */
	public void setCustomerNo(String customerNo) {
		this.customerNo = customerNo;
	}
	
	/**
	 * Gets the status.
	 *
	 * @return the status
	 */
	public String getStatus() {
		return status;
	}
	
	/**
	 * Sets the status.
	 *
	 * @param status the new status
	 */
	public void setStatus(String status) {
		this.status = status;
	}
	
	/**
	 * Gets the delivery street.
	 *
	 * @return the delivery street
	 */
	public String getDeliveryStreet() {
		return deliveryStreet;
	}
	
	/**
	 * Sets the delivery street.
	 *
	 * @param deliveryStreet the new delivery street
	 */
	public void setDeliveryStreet(String deliveryStreet) {
		this.deliveryStreet = deliveryStreet;
	}
	
	/**
	 * Gets the delivery house number.
	 *
	 * @return the delivery house number
	 */
	public String getDeliveryHouseNumber() {
		return deliveryHouseNumber;
	}
	
	/**
	 * Sets the delivery house number.
	 *
	 * @param deliveryHouseNumber the new delivery house number
	 */
	public void setDeliveryHouseNumber(String deliveryHouseNumber) {
		this.deliveryHouseNumber = deliveryHouseNumber;
	}
	
	/**
	 * Gets the delivery postal code.
	 *
	 * @return the delivery postal code
	 */
	public String getDeliveryPostalCode() {
		return deliveryPostalCode;
	}
	
	/**
	 * Sets the delivery postal code.
	 *
	 * @param deliveryPostalCode the new delivery postal code
	 */
	public void setDeliveryPostalCode(String deliveryPostalCode) {
		this.deliveryPostalCode = deliveryPostalCode;
	}
	
	/**
	 * Gets the delivery town.
	 *
	 * @return the delivery town
	 */
	public String getDeliveryTown() {
		return deliveryTown;
	}
	
	/**
	 * Sets the delivery town.
	 *
	 * @param deliveryTown the new delivery town
	 */
	public void setDeliveryTown(String deliveryTown) {
		this.deliveryTown = deliveryTown;
	}
	
	/**
	 * Gets the invoice street.
	 *
	 * @return the invoice street
	 */
	public String getInvoiceStreet() {
		return invoiceStreet;
	}
	
	/**
	 * Sets the invoice street.
	 *
	 * @param invoiceStreet the new invoice street
	 */
	public void setInvoiceStreet(String invoiceStreet) {
		this.invoiceStreet = invoiceStreet;
	}
	
	/**
	 * Gets the invoice house number.
	 *
	 * @return the invoice house number
	 */
	public String getInvoiceHouseNumber() {
		return invoiceHouseNumber;
	}
	
	/**
	 * Sets the invoice house number.
	 *
	 * @param invoiceHouseNumber the new invoice house number
	 */
	public void setInvoiceHouseNumber(String invoiceHouseNumber) {
		this.invoiceHouseNumber = invoiceHouseNumber;
	}
	
	/**
	 * Gets the invoice postal code.
	 *
	 * @return the invoice postal code
	 */
	public String getInvoicePostalCode() {
		return invoicePostalCode;
	}
	
	/**
	 * Sets the invoice postal code.
	 *
	 * @param invoicePostalCode the new invoice postal code
	 */
	public void setInvoicePostalCode(String invoicePostalCode) {
		this.invoicePostalCode = invoicePostalCode;
	}
	
	/**
	 * Gets the invoice town.
	 *
	 * @return the invoice town
	 */
	public String getInvoiceTown() {
		return invoiceTown;
	}
	
	/**
	 * Sets the invoice town.
	 *
	 * @param invoiceTown the new invoice town
	 */
	public void setInvoiceTown(String invoiceTown) {
		this.invoiceTown = invoiceTown;
	}
	
	/**
	 * Gets the creation date.
	 *
	 * @return the creation date
	 */
	public String getCreationDate() {
		return creationDate;
	}
	
	/**
	 * Sets the creation date.
	 *
	 * @param creationDate the new creation date
	 */
	public void setCreationDate(String creationDate) {
		this.creationDate = creationDate;
	}
	
	/**
	 * Gets the payment method.
	 *
	 * @return the payment method
	 */
	public String getPaymentMethod() {
		return paymentMethod;
	}
	
	/**
	 * Sets the payment method.
	 *
	 * @param paymentMethod the new payment method
	 */
	public void setPaymentMethod(String paymentMethod) {
		this.paymentMethod = paymentMethod;
	}
	
	/**
	 * Gets the dispatch type.
	 *
	 * @return the dispatch type
	 */
	public String getDispatchType() {
		return dispatchType;
	}
	
	/**
	 * Sets the dispatch type.
	 *
	 * @param dispatchType the new dispatch type
	 */
	public void setDispatchType(String dispatchType) {
		this.dispatchType = dispatchType;
	}
	
	/**
	 * Gets the delivery name.
	 *
	 * @return the delivery name
	 */
	public String getDeliveryName() {
		return deliveryName;
	}
	
	/**
	 * Sets the delivery name.
	 *
	 * @param deliveryName the new delivery name
	 */
	public void setDeliveryName(String deliveryName) {
		this.deliveryName = deliveryName;
	}
	
	/**
	 * Gets the delivery firstname.
	 *
	 * @return the delivery firstname
	 */
	public String getDeliveryFirstname() {
		return deliveryFirstname;
	}
	
	/**
	 * Sets the delivery firstname.
	 *
	 * @param deliveryFirstname the new delivery firstname
	 */
	public void setDeliveryFirstname(String deliveryFirstname) {
		this.deliveryFirstname = deliveryFirstname;
	}
}
