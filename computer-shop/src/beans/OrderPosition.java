package beans;

/**
 * The Class OrderPosition. It represents one row of the database table 'orderpositions'.
 * 
 * @author John Edwin Braun
 */
public class OrderPosition {
	
	/** The order no. */
	private String orderNo;
	
	/** The article no. */
	private String articleNo;
	
	/** The amount. */
	private String amount;
	
	/** The price per piece. */
	private String pricePerPiece;
	
	/**
	 * Gets the order no.
	 *
	 * @return the order no
	 */
	public String getOrderNo() {
		return orderNo;
	}
	
	/**
	 * Sets the order no.
	 *
	 * @param orderNo the new order no
	 */
	public void setOrderNo(String orderNo) {
		this.orderNo = orderNo;
	}
	
	/**
	 * Gets the article no.
	 *
	 * @return the article no
	 */
	public String getArticleNo() {
		return articleNo;
	}
	
	/**
	 * Sets the article no.
	 *
	 * @param articleNo the new article no
	 */
	public void setArticleNo(String articleNo) {
		this.articleNo = articleNo;
	}
	
	/**
	 * Gets the amount.
	 *
	 * @return the amount
	 */
	public String getAmount() {
		return amount;
	}
	
	/**
	 * Sets the amount.
	 *
	 * @param amount the new amount
	 */
	public void setAmount(String amount) {
		this.amount = amount;
	}
	
	/**
	 * Gets the price per piece.
	 *
	 * @return the price per piece
	 */
	public String getPricePerPiece() {
		return pricePerPiece;
	}
	
	/**
	 * Sets the price per piece.
	 *
	 * @param pricePerPiece the new price per piece
	 */
	public void setPricePerPiece(String pricePerPiece) {
		this.pricePerPiece = pricePerPiece;
	}
	
	

}
