package beans;

/**
 * The Class Question. It represents one row of the database table 'questionsandanswers'.
 * 
 * @author Oliver Neumann
 */
public class Question {
	
	/** The id. */
	private String ID;
	
	/** The questioner. */
	private String questioner;
	
	/** The question. */
	private String question;
	
	/** The answer. */
	private String answer;
	
	/** The title. */
	private String title;
	
	/** The date. */
	private String date;
	
	/** The active. */
	private String active;
	
	
	/**
	 * Gets the id.
	 *
	 * @return the id
	 */
	public String getID() {
		return ID;
	}
	
	/**
	 * Sets the id.
	 *
	 * @param iD the new id
	 */
	public void setID(String iD) {
		ID = iD;
	}
	
	/**
	 * Gets the questioner.
	 *
	 * @return the questioner
	 */
	public String getQuestioner() {
		return questioner;
	}
	
	/**
	 * Sets the questioner.
	 *
	 * @param questioner the new questioner
	 */
	public void setQuestioner(String questioner) {
		this.questioner = questioner;
	}
	
	/**
	 * Gets the question.
	 *
	 * @return the question
	 */
	public String getQuestion() {
		return question;
	}
	
	/**
	 * Sets the question.
	 *
	 * @param question the new question
	 */
	public void setQuestion(String question) {
		this.question = question;
	}
	
	/**
	 * Gets the answer.
	 *
	 * @return the answer
	 */
	public String getAnswer() {
		return answer;
	}
	
	/**
	 * Sets the answer.
	 *
	 * @param answer the new answer
	 */
	public void setAnswer(String answer) {
		this.answer = answer;
	}
	
	/**
	 * Gets the title.
	 *
	 * @return the title
	 */
	public String getTitle() {
		return title;
	}
	
	/**
	 * Sets the title.
	 *
	 * @param title the new title
	 */
	public void setTitle(String title) {
		this.title = title;
	}
	
	/**
	 * Gets the date.
	 *
	 * @return the date
	 */
	public String getDate() {
		return date;
	}
	
	/**
	 * Sets the date.
	 *
	 * @param date the new date
	 */
	public void setDate(String date) {
		this.date = date;
	}
	
	/**
	 * Gets the active.
	 *
	 * @return the active
	 */
	public String getActive() {
		return active;
	}
	
	/**
	 * Sets the active.
	 *
	 * @param active the new active
	 */
	public void setActive(String active) {
		this.active = active;
	}

}
