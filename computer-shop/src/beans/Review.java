package beans;

/**
 * The Class Review. It represents one row of the database table 'reviews'.
 * 
 * @author John Edwin Braun
 */
public class Review {
	
	/** The customer name. */
	private String customerName;
	
	/** The article no. */
	private String articleNo;
	
	/** The number of stars. */
	private String numberOfStars;
	
	/** The text. */
	private String text;
	
	/** The title. */
	private String title;
	
	/** The date. */
	private String date;
	
	/**
	 * Gets the date.
	 *
	 * @return the date
	 */
	public String getDate() {
		return date;
	}
	
	/**
	 * Sets the date.
	 *
	 * @param date the new date
	 */
	public void setDate(String date) {
		this.date = date;
	}
	
	/**
	 * Gets the customer name.
	 *
	 * @return the customer name
	 */
	public String getCustomerName() {
		return customerName;
	}
	
	/**
	 * Sets the customer name.
	 *
	 * @param customerNo the new customer name
	 */
	public void setCustomerName(String customerNo) {
		this.customerName = customerNo;
	}
	
	/**
	 * Gets the article no.
	 *
	 * @return the article no
	 */
	public String getArticleNo() {
		return articleNo;
	}
	
	/**
	 * Sets the article no.
	 *
	 * @param articleNo the new article no
	 */
	public void setArticleNo(String articleNo) {
		this.articleNo = articleNo;
	}
	
	/**
	 * Gets the number of stars.
	 *
	 * @return the number of stars
	 */
	public String getNumberOfStars() {
		return numberOfStars;
	}
	
	/**
	 * Sets the number of stars.
	 *
	 * @param numberOfStars the new number of stars
	 */
	public void setNumberOfStars(String numberOfStars) {
		this.numberOfStars = numberOfStars;
	}
	
	/**
	 * Gets the text.
	 *
	 * @return the text
	 */
	public String getText() {
		return text;
	}
	
	/**
	 * Sets the text.
	 *
	 * @param text the new text
	 */
	public void setText(String text) {
		this.text = text;
	}
	
	/**
	 * Gets the title.
	 *
	 * @return the title
	 */
	public String getTitle() {
		return title;
	}
	
	/**
	 * Sets the title.
	 *
	 * @param title the new title
	 */
	public void setTitle(String title) {
		this.title = title;
	}
}
