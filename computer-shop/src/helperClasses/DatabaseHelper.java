package helperClasses;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.SQLIntegrityConstraintViolationException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpSession;

import beans.Article;
import beans.Customer;
import beans.Order;
import beans.OrderPosition;
import beans.Question;
import beans.Review;

/**
 * The Class DatabaseHelper.
 * This class provides methods to handle all connections to the database.
 * 
 * @author John Edwin Braun
 * @author Katharina Unruhe
 * @author Leon Stollberg
 * @author Jan-Niklas Posselt
 * @author Oliver Neumann
 */

public class DatabaseHelper {
	
	/** The connection. */
	private Connection conn = null;
	
	/** The driver. */
	@SuppressWarnings("unused")
	private Object driver = null;
	
	/** The host name. */
	private final String hostname = "localhost";
	
	/** The port. */
	private final String port = "3306";
	
	/** The database name. */
	private final String dbname ="db_computershop?serverTimezone=UTC";
	
	/** The user. */
	private final String user = "root";
	
	/** The password. */
	private final String password = "1234";

	
	/**
	 * Instantiates a new database helper.
	 * It creates the connection to the database.
	 * 
	 */
	public DatabaseHelper() {
		try {
			
			try { 
				// Loading the driver (Java Database Connector)	
				driver = Class.forName("com.mysql.cj.jdbc.Driver");
			
			} 
	        catch (Exception e) { 
	            System.err.println("Unable to load driver."); 
	            e.printStackTrace();
	        }	        
				
			try { 
			// Open the connection
		
			    String url = "jdbc:mysql://"+hostname+":"+port+"/"+dbname; 
			    conn = DriverManager.getConnection(url, user, password); 
			    
	        } 
	        catch (SQLException sqle) { 
	            System.out.println("SQLException: " + sqle.getMessage()); 
	            System.out.println("SQLState: " + sqle.getSQLState()); 
	            System.out.println("VendorError: " + sqle.getErrorCode()); 
	            sqle.printStackTrace(); 
	        }
			
		}catch(Exception ex) {
			ex.printStackTrace();
		}
		

	}
	
	/**
	 * Gets the accessories from the database ordered after the sort criteria.
	 *
	 * @param p_sortCrit The sort criteria (Name_asc, Name_desc, Price_desc or Price_asc)
	 * @return List of all accessories
	 * @throws SQLException SQL exception
	 */
		public List<Article> getAccessory (String p_sortCrit) throws SQLException {
			Statement stmt = null;
			ResultSet rst = null;
			List<Article> articleList = new ArrayList<Article>();
			
			// Converting the parameter to a valid SQL expression.
			if(p_sortCrit.equals("Name_asc")) {
				p_sortCrit = "Name asc";
			}
			else if(p_sortCrit.equals("Name_desc")) {
				p_sortCrit = "Name desc";
			}
			else if(p_sortCrit.equals("Price_desc")) {
				p_sortCrit = "Price desc";
			}
			else if(p_sortCrit.equals("Price_asc")) {
				p_sortCrit = "Price asc";
			}
			
			try {
				
				// Executing the SQL Statement
				stmt = conn.createStatement();
				
				rst = stmt.executeQuery("SELECT *, (select round(avg(NumberOfStars),1) from reviews where ArticleNo = e.ArticleNo) FROM articles as e inner JOIN articlecategories ON \"true\" = articlecategories.Accessory where e.Category = articlecategories.ID order by " + p_sortCrit + ";");
				
				// Looping through the result set from the database
				while(rst.next()) {
					Article current = new Article();
					
					// Setting all the attribute of the current article
					current.setArticleNo(rst.getString(1));
					current.setName(rst.getString(2));
					current.setDescription(rst.getString(3));
					current.setPrice(rst.getString(4));
					current.setCategory(rst.getString(5));
					current.setPictureLink(rst.getString(6));
					current.setMinStock(rst.getString(7));
					current.setCurrentStock(rst.getString(8));

					// Setting the attribute averageStars of the current article
					if(rst.getString(13) != null) {
						current.setAverageStars(rst.getString(13) + " von 5 Sternen");

					}
					else if(rst.getString(10) == null) {
						current.setAverageStars("Keine Bewertungen!");;
					}
					
					// Adding the current article to the result list
					articleList.add(current);
				}
			}
			catch (SQLException ex){
				ex.printStackTrace();
			}

			// Returning the list of the accessories
			return articleList;
		}

	
	/**
	 * Gets the article from the database with the article number from the parameter.
	 *
	 * @param p_artikelNr Article number
	 * @return The article
	 * @throws SQLException SQL exception
	 */
	public Article getArticle (String p_artikelNr) throws SQLException { 
		Statement stmt = null;
		ResultSet rst = null;
		Article article = null;
		
		try {
			// Execute SQL statement
			stmt = conn.createStatement();
			rst = stmt.executeQuery("select *, (select round(avg(NumberOfStars),1) from reviews where ArticleNo = e.ArticleNo) from articles e where ArticleNo = " + p_artikelNr + ";");
			
			// If an article was found -> set all the attributes
			// Else -> Throw Exception
			if(rst.next() && rst != null) {
				article = new Article();
				article.setArticleNo(rst.getString(1));
				article.setName(rst.getString(2));
				article.setDescription(rst.getString(3));
				article.setPrice(rst.getString(4));
				article.setCategory(rst.getString(5));
				article.setPictureLink(rst.getString(6));
				article.setMinStock(rst.getString(7));
				article.setCurrentStock(rst.getString(8));
				
				// Set the attribute averageStars
				if(rst.getString(10) != null) {
					article.setAverageStars(rst.getString(10) + " von 5 Sternen");
				}
				else if(rst.getString(10) == null) {
					article.setAverageStars("Keine Bewertungen!");;
				}
				
			}
			else {
				throw new SQLException("Statement not working / Article not found");
			}

		}
		catch (SQLException ex){
			ex.printStackTrace();
		}
		
		// Return the article
		return article;
	}
	
	
	/**
	 * Gets the customer from the database with the customer number from the parameter.
	 *
	 * @param p_customerNo Customer number
	 * @return Customer
	 * @throws SQLException SQL exception
	 */
		public Customer getCustomer (String p_customerNo) throws SQLException { 
			Statement stmt = null;
			ResultSet rst = null;
			Customer customer = null;
			
			try {
				// Execute the SQL statement
				stmt = conn.createStatement();
				rst = stmt.executeQuery("select * from users where CustomerNo = " + p_customerNo + ";");
				
				// If customer was found -> set all attributes
				// Else -> throw exception
				if(rst.next() && rst != null) {
					customer = new Customer();
					customer.setCustomerNo(rst.getString(1));
					customer.setEmail(rst.getString(2));
					customer.setPassword(rst.getString(3));
					customer.setName(rst.getString(4));
					customer.setFirstname(rst.getString(5));
					customer.setTitle(rst.getString(6));
					customer.setFormOfAddress(rst.getString(7));
					customer.setInvoiceStreet(rst.getString(8));
					customer.setInvoiceHouseNumber(rst.getString(9));
					customer.setInvoicePostalCode(rst.getString(10));
					customer.setInvoiceTown(rst.getString(11));
					customer.setOneTimeCustomer(rst.getString(12));
					customer.setAdmin(rst.getString(13));
					
				}
				else {
					throw new SQLException("Statement not working / Customer not found");
				}

			}
			catch (SQLException ex){
				ex.printStackTrace();
			}
			
			// Return the customer
			return customer;
		}
	
	
	/**
	 * Gets all articles from the database ordered by a sort criteria.
	 *
	 * @param p_sortCrit The sort criteria (Name_asc, Name_desc, Price_desc or Price_asc)
	 * @return List of all articles
	 * @throws SQLException SQL exception
	 */
	public List<Article> getAllArticles (String p_sortCrit) throws SQLException {
		Statement stmt = null;
		ResultSet rst = null;
		List<Article> articleList = new ArrayList<Article>();
		
		// Setting a standard sort criteria to prevent NullPointerException 
		if(p_sortCrit == null) {
			p_sortCrit = "Name_asc";
		}
		
		// Convert parameter to a SQL statement
		if(p_sortCrit.equals("Name_asc")) {
			p_sortCrit = "Name asc";
		}
		else if(p_sortCrit.equals("Name_desc")) {
			p_sortCrit = "Name desc";
		}
		else if(p_sortCrit.equals("Price_desc")) {
			p_sortCrit = "Price desc";
		}
		else if(p_sortCrit.equals("Price_asc")) {
			p_sortCrit = "Price asc";
		}
		
		try {
			// Executing SQL statement
			stmt = conn.createStatement();
			rst = stmt.executeQuery("SELECT * ,(select round(avg(NumberOfStars),1) from reviews where ArticleNo = e.ArticleNo) FROM articles e order by " + p_sortCrit + ";");
			
			// Looping through the result set to fill the list
			while(rst.next()) {
				Article current = new Article();
				
				// Setting the attributes of the article
				current.setArticleNo(rst.getString(1));
				current.setName(rst.getString(2));
				current.setDescription(rst.getString(3));
				current.setPrice(rst.getString(4));
				current.setCategory(rst.getString(5));
				current.setPictureLink(rst.getString(6));
				current.setMinStock(rst.getString(7));
				current.setCurrentStock(rst.getString(8));
				
				// Setting the attribute averageStars
				if(rst.getString(10) != null) {
					current.setAverageStars(rst.getString(10) + " von 5 Sternen");
				}
				else if(rst.getString(10) == null) {
					current.setAverageStars("Keine Bewertungen!");;
				}

				// Adding the article to the list
				articleList.add(current);
			}
		}
		catch (SQLException ex){
			ex.printStackTrace();
		}

		// Returning the article list
		return articleList;
	}
	
	/**
	 * Gets all active articles from a category from the database ordered by sort criteria.
	 *
	 * @param p_sortCrit Sort criteria
	 * @param p_category Category
	 * @return List of all active articles
	 * @throws SQLException SQL exception
	 */
	public List<Article> getAllActiveArticles (String p_sortCrit, String p_category) throws SQLException {
		Statement stmt = null;
		ResultSet rst = null;
		List<Article> articleList = new ArrayList<Article>();
		
		// Converting the parameter to a SQL statement
		if(p_sortCrit.equals("Name_asc")) {
			p_sortCrit = "Name asc";
		}
		else if(p_sortCrit.equals("Name_desc")) {
			p_sortCrit = "Name desc";
		}
		else if(p_sortCrit.equals("Price_desc")) {
			p_sortCrit = "Price desc";
		}
		else if(p_sortCrit.equals("Price_asc")) {
			p_sortCrit = "Price asc";
		}
		
		try {
			// Executing SQL statement
			stmt = conn.createStatement();		
			rst = stmt.executeQuery("select * ,(select round(avg(NumberOfStars),1) from reviews where ArticleNo = e.ArticleNo) from articles e where Status in ('active') and Category in (" + p_category + ")  order by " + p_sortCrit + ";");
			
			// Looping through result set and adding article to list
			while(rst.next()) {
				Article current = new Article();
				current.setArticleNo(rst.getString(1));
				current.setName(rst.getString(2));
				current.setDescription(rst.getString(3));
				current.setPrice(rst.getString(4));
				current.setCategory(rst.getString(5));
				current.setPictureLink(rst.getString(6));
				current.setMinStock(rst.getString(7));
				current.setCurrentStock(rst.getString(8));
				if(rst.getString(10) != null) {
					current.setAverageStars(rst.getString(10) + " von 5 Sternen");
				}
				else if(rst.getString(10) == null) {
					current.setAverageStars("Keine Bewertungen!");;
				}


				
				
				articleList.add(current);
			}
		}
		catch (SQLException ex){
			ex.printStackTrace();
		}

		return articleList;
	}
	
	
	
	/**
	 * Gets all customers from the database.
	 *
	 * @return List of all customers
	 * @throws SQLException SQL exception
	 */
		public List<Customer> getAllCustomers () throws SQLException {
			Statement stmt = null;
			ResultSet rst = null;
			List<Customer> customerList = new ArrayList<Customer>();
			
			try {
				// Executing SQL statement
				stmt = conn.createStatement();
				rst = stmt.executeQuery("SELECT * FROM db_computershop.users;");
				
				// Looping throuhg result set and adding customer to list
				while(rst.next()) {
					Customer current = new Customer();
					current.setCustomerNo(rst.getString(1));
					current.setEmail(rst.getString(2));
					current.setPassword(rst.getString(3));
					current.setName(rst.getString(4));
					current.setFirstname(rst.getString(5));
					current.setTitle(rst.getString(6));
					current.setFormOfAddress(rst.getString(7));
					current.setInvoiceStreet(rst.getString(8));
					current.setInvoiceHouseNumber(rst.getString(9));
					current.setInvoicePostalCode(rst.getString(10));
					current.setInvoiceTown(rst.getString(11));
					current.setOneTimeCustomer(rst.getString(12));
					
					
					customerList.add(current);
				}
			}
			catch (SQLException ex){
				ex.printStackTrace();
			}

			return customerList;
		}
	
	
	/**
	 * Gets all reviews to one article.
	 *
	 * @param p_articleNo Article number
	 * @return List of reviews
	 * @throws SQLException SQL exception
	 */
	public List<Review> getReviewsByArticleNo(String p_articleNo) throws SQLException{
		Statement stmt = null;
		ResultSet rst = null;
		List<Review> reviewList = new ArrayList<Review>();
		
		try {
			// Executing SQL statement
			stmt = conn.createStatement();
			rst = stmt.executeQuery("SELECT concat(c.Firstname, ' ', c.Name) as CustomerName, "
					+ "r.ArticleNo, r.NumberOfStars, r.Text, r.Title FROM reviews r, "
					+ "users  c where r.ArticleNo = " + p_articleNo + " and r.CustomerNo = c.CustomerNo;");
			
			// Looping through result set and adding review to list
			while(rst.next()) {
				Review current = new Review();
				current.setCustomerName(rst.getString(1));
				current.setArticleNo(rst.getString(2));
				current.setNumberOfStars(rst.getString(3));
				current.setText(rst.getString(4));
				current.setTitle(rst.getString(5));
				
				reviewList.add(current);
			}
		}
		catch (SQLException ex){
			ex.printStackTrace();
		}

		return reviewList;
		}
	
	
	
	
	/**
	 * Insert an one time customer into the database.
	 *
	 * @param p_customer Customer
	 * @return The ID of the newly inserted customer
	 * @throws SQLException SQL exception
	 * @throws SQLIntegrityConstraintViolationException SQL integrity constraint violation exception
	 */
	public String insertOneTimeCustomer(Customer p_customer) throws SQLException, SQLIntegrityConstraintViolationException{
		
		try {
			// Executing the SQL statement
			Statement stmt = conn.createStatement();			
			stmt.executeUpdate("insert into users(EMail, Name, Firstname, Title, FormOfAddress, InvoiceStreet, InvoiceHouseNumber, InvoicePostalCode, InvoiceTown, OneTimeCustomer) values ('"
					+ p_customer.getEmail() + "','"
					+ p_customer.getName() +"','"
					+ p_customer.getFirstname() +"','"
					+ p_customer.getTitle() +"','"
					+ p_customer.getFormOfAddress() +"','"
					+ p_customer.getInvoiceStreet() +"','"
					+ p_customer.getInvoiceHouseNumber() +"','"
					+ p_customer.getInvoicePostalCode() +"','"
					+ p_customer.getInvoiceTown() +"','"
					+ p_customer.getOneTimeCustomer() +"');");
		}
		catch (Exception ex){
			ex.printStackTrace();
			throw new SQLIntegrityConstraintViolationException();
		}
		return lastInsertID();
	}

	/**
	 * Insert a customer into the database.
	 *
	 * @param p_customer Customer
	 * @return true, if customer was successfully inserted
	 * @throws SQLException SQL exception
	 * @throws SQLIntegrityConstraintViolationException SQL integrity constraint violation exception
	 */
	public boolean insertCustomer(Customer p_customer) throws SQLException, SQLIntegrityConstraintViolationException{
		
		try {
			// Executing SQL statement
			Statement stmt = conn.createStatement();			
			stmt.executeUpdate("insert into users(EMail, Password, Name, Firstname, Title, FormOfAddress, InvoiceStreet, InvoiceHouseNumber, InvoicePostalCode, InvoiceTown, OneTimeCustomer) values ('"
					+ p_customer.getEmail() + "','"
					+ p_customer.getPassword() +"','"
					+ p_customer.getName() +"','"
					+ p_customer.getFirstname() +"','"
					+ p_customer.getTitle() +"','"
					+ p_customer.getFormOfAddress() +"','"
					+ p_customer.getInvoiceStreet() +"','"
					+ p_customer.getInvoiceHouseNumber() +"','"
					+ p_customer.getInvoicePostalCode() +"','"
					+ p_customer.getInvoiceTown() +"','"
					+ p_customer.getOneTimeCustomer() +"');");
			return true;
		}
		catch (Exception ex){
			ex.printStackTrace();
			throw new SQLIntegrityConstraintViolationException();
		}
		
	}

	
	
	/**
	 * Gets all active questions from the database.
	 *
	 * @return List of all active questions
	 * @throws SQLException SQL exception
	 */
	public List<Question> getAllActiveQuestions () throws SQLException {
		Statement stmt = null;
		ResultSet rst = null;
		List<Question> questionList = new ArrayList<Question>();
		
		try {
			
			// Executing SQL statement
			stmt = conn.createStatement();
			rst = stmt.executeQuery("select * from questionsandanswers where Active = 'true';");
			
			//Looping through result set and adding question to list
			while(rst.next()) {
				Question current = new Question();
				current.setQuestioner(rst.getString(2));
				current.setQuestion(rst.getString(3));
				current.setAnswer(rst.getString(4));
				current.setTitle(rst.getString(5));
				current.setDate(rst.getString(6));
				current.setID(rst.getString(1));
				current.setActive(rst.getString(7));
				
				questionList.add(current);
			}
		}
		catch (SQLException ex){
			ex.printStackTrace();
		}

		return questionList;
	}
	
	/**
	 * Insert a question into the database.
	 *
	 * @param p_question Question
	 * @return true, if question was successfully inserted
	 * @throws SQLException SQL exception
	 */
	public boolean insertQuestion(Question p_question) throws SQLException{
		
		try {
			
			// Execute SQL statement
			Statement stmt = conn.createStatement();			
			stmt.executeUpdate("insert into questionsandanswers(Questioner, Question, Title, `Date`, Active) values ('"
					+ p_question.getQuestioner() + "','"
					+ p_question.getQuestion() +"','"
					+ p_question.getTitle() +"',"
					+ "DATE_FORMAT(SYSDATE(), '%Y-%m-%d'),"
					+ "'false');");
			
			return true;
					
		}
		catch (Exception ex){
			ex.printStackTrace();
			
			return false;
		}
		
	}

	/**
	 * Check if the combination of the email and the password is correct
	 *
	 * @param p_email Email
	 * @param p_password Password
	 * @return true, if the password and the email is correct
	 * @throws SQLException SQL exception
	 */
	public boolean checkPasswordAndEmail(String p_email, String p_password) throws SQLException{
		Statement stmt = null;
		ResultSet rst = null;
		String passwordFromDB = "";
		boolean rightPassword = false;
		
		try {
			// Get password from database
			stmt = conn.createStatement();
			rst = stmt.executeQuery("select Password from users where EMail = '" + p_email + "';");
			
			// Checking if the result set contains an entry (email is correct if an entry was found)
			if(rst.next() && rst != null) {
				passwordFromDB = rst.getString(1);
				
			}
			else {
				throw new SQLException("Statement not working / email wrong");
			}
			
			// Checking if password matches
			if(passwordFromDB.equals(p_password)) {
				rightPassword = true;
			}

		}
		catch (SQLException ex){
			ex.printStackTrace();
		}

		return rightPassword;
	}
	
	
	/**
	 * Insert an article into the database.
	 *
	 * @param p_article Article
	 * @return Article number of the newly created article
	 * @throws SQLException SQL exception
	 */
	public String insertArticle(Article p_article) throws SQLException{
		
		try {
			// Execute SQL statement
			Statement stmt = conn.createStatement();			
			stmt.executeUpdate("insert into articles(`Name`,`Description`,Price,Category,PictureLink,MinStock,CurrentStock,Status) values ('"
					+ p_article.getName() + "','"
					+ p_article.getDescription() +"','"
					+ p_article.getPrice() +"','"
					+ p_article.getCategory() +"','"
					+ p_article.getPictureLink() + "','"
					+ p_article.getMinStock() +"','"
					+ p_article.getCurrentStock() +"','"
					+ p_article.getStatus() + "');");
			
					
		}
		catch (Exception ex){
			ex.printStackTrace();
		}
		
		return lastInsertID();
		
	}
	
	
	/**
	 * Gets the search results (articles) from the database. The result is ordered by a sort criteria and from a specific category.
	 *
	 * @param p_search Search string
	 * @param p_sort Sort criteria
	 * @param p_category Category
	 * @return List of articles that match the search string
	 * @throws SQLException SQL exception
	 */
	public List<Article> getSearchResults (String p_search, String p_sort, String p_category) throws SQLException {
		Statement stmt = null;
		ResultSet rst = null;
		List<Article> articleList = new ArrayList<Article>();
		
		// Convert textual category to the category number
		if(p_category.equals("PC")) {
			p_category = "1";
		}
		else if(p_category.equals("Maus")) {
			p_category = "2";
		}
		else if(p_category.equals("Monitor")) {
			p_category = "3";
		}
		else if(p_category.equals("Laptop")) {
			p_category = "4";
		}
		else if(p_category.equals("Tastatur")) {
			p_category = "5";
		}
		else {
			p_category = "1,2,3,4,5,6,7,8,9,10,11,12,13";
		}
		
		// Convert the sort criteria to an SQL statement
		if(p_sort.equals("Name_asc")) {
			p_sort = "Name asc";
		}
		else if(p_sort.equals("Name_desc")) {
			p_sort = "Name desc";
		}
		else if(p_sort.equals("Price_asc")) {
			p_sort = "Price asc";
		}
		else if(p_sort.equals("Price_desc")) {
			p_sort = "Price desc";
		}
		
		try {
			// Execute SQL statement
			stmt = conn.createStatement();
			rst = stmt.executeQuery("select *, (select round(avg(NumberOfStars),1) from reviews where ArticleNo = e.ArticleNo)"
					+ " from articles e where match (`Description`,`Name`) against ('" + p_search + "') and Category"
					+ " in (" + p_category + ") and Status = 'active' order by " + p_sort + ";");
					
			// Loop through result set and adding article to list
			while(rst.next()) {
				Article current = new Article();
				current.setArticleNo(rst.getString(1));
				current.setName(rst.getString(2));
				current.setDescription(rst.getString(3));
				current.setPrice(rst.getString(4));
				current.setCategory(rst.getString(5));
				current.setPictureLink(rst.getString(6));
				current.setMinStock(rst.getString(7));
				current.setCurrentStock(rst.getString(8));
				
				if(rst.getString(10) != null) {
					current.setAverageStars(rst.getString(10) + " von 5 Sternen");
				}
				else if(rst.getString(10) == null) {
					current.setAverageStars("Keine Bewertungen!");;
				}
				
				
				
				articleList.add(current);
			}
		}
		catch (SQLException ex){
			ex.printStackTrace();
		}

		return articleList;
	}
	
	 
	/**
	 * Delete a specific customer (the customer will be anonymised).
	 *
	 * @param p_customerNo Customer number
	 * @return true, if customer was successfully deleted
	 * @throws SQLException SQL exception
	 */
		public boolean deleteCustomer(String p_customerNo) throws SQLException{
			
		
			try {
				Customer customer = new Customer(); 
				
				
				// Anonymise the data
				customer.setTitle("0");
				customer.setFormOfAddress("Anderes");
				customer.setName("Anonym");
				customer.setFirstname("0");
				customer.setEmail(p_customerNo);
				customer.setInvoiceStreet("0");
				customer.setInvoiceHouseNumber("0");
				customer.setInvoiceTown("0");
				customer.setInvoicePostalCode("0");
				customer.setOneTimeCustomer("false");
								
				// Execute SQL statement
				Statement stmt = conn.createStatement();			
				stmt.executeUpdate("UPDATE users SET EMail = '" + customer.getEmail()  +"', Password = '" + customer.getPassword() + "', Name = '" +customer.getName() + "', Firstname ='" + customer.getFirstname() + "', Title = '" + customer.getTitle() + "', FormOfAddress = '" + customer.getFormOfAddress() + "', InvoiceStreet = '" + customer.getInvoiceStreet() +"', InvoiceHouseNumber = '" + customer.getInvoiceHouseNumber() +"', InvoicePostalCode = '" + customer.getInvoicePostalCode() +"', InvoiceTown = '" + customer.getInvoiceTown() +"', OneTimeCustomer = '" + customer.getOneTimeCustomer() +"' where CustomerNo ='"  + customer.getEmail() + "';"); 
														
				return true;
			}
			catch (Exception ex){
				ex.printStackTrace();
				return false;
			}
			
	}
				
	/**
	 * Gets the customer number by the email.
	 *
	 * @param p_email Email
	 * @return Customer number
	 */
	public String getCustomerNoByEMail(String p_email) {
		Statement stmt = null;
		ResultSet rst = null;
		String customerNo = "";
	
		try {
			// Executing SQL statement
			stmt = conn.createStatement();
			rst = stmt.executeQuery("select CustomerNo from users where EMail = '" + p_email + "';");
			if(rst.next()) {
				customerNo = rst.getString(1);
			}
			
		}
		catch (SQLException ex) {
			ex.printStackTrace();
		}
		
		return customerNo;
	}
	
	/**
	 * Checks if a customer already reviewed an article.
	 *
	 * @param p_articleNo Article number
	 * @param p_customerNo Customer number
	 * @return true, if the customer already reviewed the article
	 */
	public boolean customerReviewedArticle(String p_articleNo, String p_customerNo) {
		boolean result = true;
		Statement stmt = null;
		ResultSet rst = null;
		
		try {
			// Get the review for the article from the customer
			stmt = conn.createStatement();
			rst = stmt.executeQuery("select 1 from reviews where CustomerNo = '" +p_customerNo+ "' and ArticleNo = '" +p_articleNo+ "';");
			
			// If the result set is not empty (customer reviewed article) -> true
			// else -> false
			if(rst.isBeforeFirst()) {
				result = true;
			}
			else {
				result = false;
			}
		}
		catch (SQLException ex) {
			ex.printStackTrace();
		}
		
		return result;
	}
	
	/**
	 * Insert review into the database.
	 *
	 * @param p_review Review
	 * @param p_customerNo Customer number
	 * @throws SQLException SQL exception
	 */
	public void insertReview(Review p_review, String p_customerNo) throws SQLException{
		try {
			// Executing SQL statement
			Statement stmt = conn.createStatement();			
			stmt.executeUpdate("insert into reviews (CustomerNo, ArticleNo, NumberOfStars, Text, Title, Date) values ('"
					+ p_customerNo + "','"
					+ p_review.getArticleNo() + "','"
					+ p_review.getNumberOfStars() + "','"
					+ p_review.getText() + "','"
					+ p_review.getTitle() + "', "
					+ "DATE_FORMAT(SYSDATE(), '%Y-%m-%d'));");
		}
		catch (Exception ex){
			ex.printStackTrace();
		}
	}
	
	
	/**
	 * Update an article in the database.
	 *
	 * @param p_article Article
	 * @return true, if article was updated successfully
	 * @throws SQLException SQL exception
	 */
	public boolean updateArticle(Article p_article) throws SQLException{
		
		try {
			// Executing SQL statement
			Statement stmt = conn.createStatement();			
			stmt.executeUpdate("UPDATE articles SET `Name` = '" + p_article.getName()  +"', `Description` = '" + p_article.getDescription() + "', Price = '" +p_article.getPrice() + "', Category ='" + p_article.getCategory() + "', MinStock = '" + p_article.getMinStock() + "', CurrentStock = '" + p_article.getCurrentStock() + "', Status='" + p_article.getStatus() +"' where ArticleNo =" + p_article.getArticleNo() + ";");
					
			return true;
		}
		catch (Exception ex){
			ex.printStackTrace();
			return false;
		}
		
	}
	
	/**
	 * Change the password of an user.
	 *
	 * @param p_customer Customer
	 * @return true, if password was updated successfully
	 * @throws SQLException SQL exception
	 */
		public boolean changePassword(Customer p_customer) throws SQLException{  	
		
			try {
				// Executing SQL statement
				Statement stmt = conn.createStatement();			
				stmt.executeUpdate("UPDATE users SET Password = '" + p_customer.getPassword() + "'where CustomerNo ='"  + p_customer.getCustomerNo() + "';"); 				
			
				return true;
			}
			catch (Exception ex){
				ex.printStackTrace();
				return false;
			}
			
		}
	
	/**
	 * Change the email of an user.
	 *
	 * @param p_customer Customer
	 * @return true, if email was updated successfully
	 * @throws SQLException SQL exception
	 */
		public boolean changeEmail(Customer p_customer) throws SQLException{  	
		
			try {
				
				// Executing SQL statement
				Statement stmt = conn.createStatement();			
				stmt.executeUpdate("UPDATE users SET EMail = '" + p_customer.getEmail() + "'where CustomerNo ='"  + p_customer.getCustomerNo() + "';"); 				
				return true;
			}
			catch (Exception ex){
				ex.printStackTrace();
				throw new SQLIntegrityConstraintViolationException();
			}
			
		}
		
		/**
		 * Change the details of a customer.
		 *
		 * @param p_customer Customer
		 * @return true, if customer details was updated successfully
		 * @throws SQLException SQL exception
		 */
		public boolean changeDetails(Customer p_customer) throws SQLException{  	
		
				try {
					// Executing SQL statement
					Statement stmt = conn.createStatement();			
					stmt.executeUpdate("UPDATE users SET Name = '" + p_customer.getName() + "', Firstname = '" + p_customer.getFirstname() + "', Title = '" + p_customer.getTitle() +"', FormOfAddress = '" + p_customer.getFormOfAddress() +"'where CustomerNo ='"  + p_customer.getCustomerNo() + "';");			
					
					return true;
				}
				catch (Exception ex){
					ex.printStackTrace();
					return false;
				}
				
		}
		
		/**
		 * Change the address of a customer.
		 *
		 * @param p_customer Customer
		 * @return true, if the address was updated successfully
		 * @throws SQLException SQL exception
		 */
		public boolean changeAddress(Customer p_customer) throws SQLException{  	
		
				try {
					// Executing SQL statement
					Statement stmt = conn.createStatement();			
					stmt.executeUpdate("UPDATE users SET InvoiceStreet = '" + p_customer.getInvoiceStreet() + "', InvoiceHouseNumber = '" + p_customer.getInvoiceHouseNumber() + "', InvoicePostalCode = '" + p_customer.getInvoicePostalCode() +"', InvoiceTown = '" + p_customer.getInvoiceTown() +"'where CustomerNo ='"  + p_customer.getCustomerNo() + "';");			
					return true;
				}
				catch (Exception ex){
					ex.printStackTrace();
					return false;
				}
				
		}
		
		/**
		 * Gets the password from an user from the database.
		 *
		 * @param p_email Email
		 * @return Password
		 */
		public String getPassword(String p_email) {
			Statement stmt = null;
			ResultSet rst = null;
			String password = "";
			
			try {
				// Executing SQL statement
				stmt = conn.createStatement();
				rst = stmt.executeQuery("select Password from users where EMail = '" + p_email + "';");
				
				// Getting password from result set
				rst.next();
				password = rst.getString(1);
			}
			catch (SQLException ex) {
				ex.printStackTrace();
			}
			
			return password;
		}
		
		/**
		 * Creates an order in the database.
		 *
		 * @param p_order Order
		 * @param p_session Session
		 * @throws NullPointerException Null pointer exception
		 */
		public void createOrder(Order p_order, HttpSession p_session) throws NullPointerException{
			try {
				Statement stmt = conn.createStatement();	
				String orderNo = "";
				String dispatchTime = "";
				ShoppingCartHelper shoppingCart = new ShoppingCartHelper();
				List<ShoppingCartItem> shoppingCartList = new ArrayList<ShoppingCartItem>();
				
				// Setting dispatch time according to dispatch type
				if(p_order.getDispatchType().equals("Premium-Versand (1-3 Tage) +3 EUR")) {
					dispatchTime = "3";
				}
				else {
					dispatchTime = "5";
				}
				
				// Executing SQL statement to insert order
				stmt.executeUpdate("insert into orders (CustomerNo, Status, DeliveryStreet, DeliveryHouseNumber, DeliveryPostalCode, DeliveryTown, "
						+ "InvoiceStreet, InvoiceHouseNumber, InvoicePostalCode, InvoiceTown, CreationDate, PaymentMethod, DispatchType, "
						+ "DeliveryName, DeliveryFirstname, InvoiceName, InvoiceFirstname, DeliveryDate) values ("
						+ p_order.getCustomerNo() + ", "
						+ "1" + ", "
						+ "'" + p_order.getDeliveryStreet() + "', "
						+ "'" + p_order.getDeliveryHouseNumber() + "', "
						+ "'" + p_order.getDeliveryPostalCode() + "', "
						+ "'" + p_order.getDeliveryTown() + "', "
						+ "'" + p_order.getInvoiceStreet() + "', "
						+ "'" + p_order.getInvoiceHouseNumber() + "', "
						+ "'" + p_order.getInvoicePostalCode() + "', "
						+ "'" + p_order.getInvoiceTown() + "', "
						+ "DATE_FORMAT(SYSDATE(), '%Y-%m-%d'), "
						+ "'" + p_order.getPaymentMethod() + "', "
						+ "'" + p_order.getDispatchType() + "', "
						+ "'" + p_order.getDeliveryName() + "', "
						+ "'" + p_order.getDeliveryFirstname() + "', "
						+ "'" + p_order.getInvoiceName() + "', "
						+ "'" + p_order.getInvoiceFirstname() + "', "
						+ "DATE_ADD(DATE_FORMAT(SYSDATE(), '%Y-%m-%d'), INTERVAL " + dispatchTime + " DAY) "
						+ ");");
				
				// Getting the order number
				orderNo = lastInsertID();
				
				// Getting the shopping cart from the session
				shoppingCartList = shoppingCart.getShoppingCartItemList(p_session);
				
				//Looping through the shopping cart and creating order positions
				for(ShoppingCartItem temp : shoppingCartList) {
					stmt.executeUpdate("insert into orderpositions (OrderNo, ArticleNo, Amount, PricePerPiece) values ("
							+ orderNo + ", "
							+ temp.getArticleNo() + ", "
							+ temp.getAmount() + ", "
							+ temp.getPrice() + ");");
				}
				
			}
			catch (Exception ex){
				ex.printStackTrace();
			}
		}
		
		/**
		 * Update the picture link of an article.
		 *
		 * @param article Article
		 */
		public void updatePicture(Article article) {
			try {
				// Executing SQL statement
				Statement stmt = conn.createStatement();			
				stmt.executeUpdate("update articles set Picturelink ='" + article.getPictureLink() + "' where ArticleNo = " + article.getArticleNo());
			}
			catch (Exception ex){
				ex.printStackTrace();
			}
		}
		
	/**
	 * Gets the orders from one customer.
	 *
	 * @param p_customerNo Customer number
	 * @return List of orders
	 */
	public List<Order> getOrderListByCustomerNo(String p_customerNo){
		List <Order> orderList = new ArrayList<Order>();
		Statement stmtOrders = null;
		Statement stmtStatus = null;
		ResultSet rstOrders = null;
		ResultSet rstStatus = null;
		
		try {
			// Executing SQL statement to get the orders
			stmtOrders = conn.createStatement();
			stmtStatus = conn.createStatement();
			rstOrders = stmtOrders.executeQuery("select * from orders where CustomerNo = '" + p_customerNo + "' order by CreationDate desc;");
			
			// Looping through result set of orders
			while(rstOrders.next()) {
				Order tempOrder = new Order();
				
				// Executing SQL statement to get the status text for the order
				rstStatus = stmtStatus.executeQuery("select Statustext from orderstatuses where ID = "+ rstOrders.getString(3) +";");
				rstStatus.next();
				
				tempOrder.setOrderNo(rstOrders.getString(1));
				tempOrder.setCustomerNo(rstOrders.getString(2));
				tempOrder.setStatus(rstStatus.getString(1));
				tempOrder.setDeliveryStreet(rstOrders.getString(4));
				tempOrder.setDeliveryHouseNumber(rstOrders.getString(5));
				tempOrder.setDeliveryPostalCode(rstOrders.getString(6));
				tempOrder.setDeliveryTown(rstOrders.getString(7));
				tempOrder.setInvoiceStreet(rstOrders.getString(8));
				tempOrder.setInvoiceHouseNumber(rstOrders.getString(9));
				tempOrder.setInvoicePostalCode(rstOrders.getString(10));
				tempOrder.setInvoicePostalCode(rstOrders.getString(11));
				tempOrder.setCreationDate(rstOrders.getString(12));
				tempOrder.setPaymentMethod(rstOrders.getString(13));
				tempOrder.setDispatchType(rstOrders.getString(14));
				tempOrder.setDeliveryName(rstOrders.getString(15));
				tempOrder.setDeliveryFirstname(rstOrders.getString(16));
				tempOrder.setInvoiceName(rstOrders.getString(17));
				tempOrder.setInvoiceFirstname(rstOrders.getString(18));
				tempOrder.setDeliveryDate(rstOrders.getString(19));
				
				// Adding order to list
				orderList.add(tempOrder);
				
			}
		}
		catch (SQLException ex) {
			ex.printStackTrace();
		}
		
		return orderList;
	}
	
	/**
	 * Gets all order positions from one customer.
	 *
	 * @param p_customerNo Customer number
	 * @return List of order positions
	 */
	public List<OrderPosition> getOrderPositionListByCustomerNo(String p_customerNo){
		List <OrderPosition> orderPositionList = new ArrayList<OrderPosition>();
		Statement stmt = null;
		ResultSet rst = null;
		
		try {
			// Executing the SQL statement
			stmt = conn.createStatement();
			rst = stmt.executeQuery("select * from orderpositions where OrderNo IN "
					+ "(Select OrderNo from orders where CustomerNo = " + p_customerNo + ");");
			
			// Looping through result set and adding order position to list
			while(rst.next()) {
				OrderPosition tempOrderPosition = new OrderPosition();
				
				tempOrderPosition.setOrderNo(rst.getString(1));
				tempOrderPosition.setArticleNo(rst.getString(2));
				tempOrderPosition.setAmount(rst.getString(3));
				tempOrderPosition.setPricePerPiece(rst.getString(4));
				
				orderPositionList.add(tempOrderPosition);
			}
		}
		catch (SQLException ex) {
			ex.printStackTrace();
		}
		
		return orderPositionList;
	}
	
	/**
	 * Gets all ordered articles from one customer.
	 *
	 * @param p_customerNo Customer number
	 * @return List of articles
	 */
	public List<Article> getOrderedArticles(String p_customerNo){
		List<Article> articleList = new ArrayList<Article>();
		
		Statement stmt = null;
		ResultSet rst = null;
		
		try {
			// Executing SQL statement
			stmt = conn.createStatement();
			rst = stmt.executeQuery("select ArticleNo, Name, Category, PictureLink from articles where ArticleNo in "
					+ "(select ArticleNo from orderpositions where OrderNo in "
					+ "(select OrderNo from orders where CustomerNo = "+ p_customerNo +"));");
			
			// Looping through result set and adding article to list
			while(rst.next()) {
				Article tempArticle = new Article();
				
				tempArticle.setArticleNo(rst.getString(1));
				tempArticle.setName(rst.getString(2));
				tempArticle.setCategory(rst.getString(3));
				tempArticle.setPictureLink(rst.getString(4));
				
				articleList.add(tempArticle);
			}
		}
		catch (SQLException ex) {
			ex.printStackTrace();
		}
		
		return articleList;
	}
	
	/**
	 * Gets all questions from the database.
	 *
	 * @return List of questions
	 */
	public List<Question> getAllQuestions(){
		List<Question> questionList = new ArrayList<Question>();
		
		Statement stmt = null;
		ResultSet rst = null;
		
		try {
			// Executing SQL statement
			stmt = conn.createStatement();
			rst = stmt.executeQuery("select * from questionsandanswers;");
			
			// Looping through result set and adding question to list
			while(rst.next()) {
				Question tempQuestion = new Question();
				
				tempQuestion.setID(rst.getString(1));
				tempQuestion.setQuestioner(rst.getString(2));
				tempQuestion.setQuestion(rst.getString(3));
				tempQuestion.setAnswer(rst.getString(4));
				tempQuestion.setTitle(rst.getString(5));
				tempQuestion.setDate(rst.getString(6));
				
				// Converting database entry to readable data
				if(rst.getString(7).equals("true")) {
					tempQuestion.setActive("Aktiv");
				}
				else {
					tempQuestion.setActive("Inaktiv");
				}
				
				questionList.add(tempQuestion);
			}
		}
		catch (SQLException ex) {
			ex.printStackTrace();
		}
		
		return questionList;
	}
	
	/**
	 * Gets an question by a specific ID.
	 *
	 * @param p_ID ID
	 * @return Question
	 */
	public Question getQuestion (String p_ID) {
		Question question = new Question();
		
		Statement stmt = null;
		ResultSet rst = null;
		
		try {
			// Executing SQL statement
			stmt = conn.createStatement();
			rst = stmt.executeQuery("select * from questionsandanswers where ID = " + p_ID + ";");
			
			// If an entry was found -> get data from result set
			if(rst.next()) {
				question.setID(rst.getString(1));
				question.setQuestioner(rst.getString(2));
				question.setQuestion(rst.getString(3));
				question.setAnswer(rst.getString(4));
				question.setTitle(rst.getString(5));
				question.setDate(rst.getString(6));
				
				if(rst.getString(7).equals("true")) {
					question.setActive("Aktiv");
				}
				else {
					question.setActive("Inaktiv");
				}
				
			}
			
			
		}
		catch (SQLException ex) {
			ex.printStackTrace();
		}
		
		return question;
	}
	
	/**
	 * Update a question in the database.
	 *
	 * @param p_question Question
	 * @return true, if question was updated successfully
	 */
	public boolean updateQuestion (Question p_question) {
			
		try {
			// Executing SQL statement
			Statement stmt = conn.createStatement();			
			stmt.executeUpdate("update questionsandanswers set "
					+ "Questioner = '"+ p_question.getQuestioner() +"', "
					+ "Question = '"+ p_question.getQuestion() +"', "
					+ "Answer = '"+ p_question.getAnswer() +"', "
					+ "Title = '"+ p_question.getTitle() +"', "
					+ "Active = '"+ p_question.getActive() +"' "
					+ "where ID = "+ p_question.getID() +";");	
			
			return true;
			
		}
		catch (Exception ex){
			ex.printStackTrace();
			return false;
		}
	
	}
	
	/**
	 * Get the last inserted automatically created ID from the database.
	 *
	 * @return Last inserted ID
	 */
	private String lastInsertID() {
		Statement stmt = null;
		ResultSet rst = null;
		String lastInsertID = "";
		
		try {
			
			// Executing SQL statement
			stmt = conn.createStatement();
			rst = stmt.executeQuery("select last_insert_id();");
			
			// Getting data from result set
			rst.next();
			lastInsertID = rst.getString(1);
		}
		catch (SQLException ex) {
			ex.printStackTrace();
		}
		
		return lastInsertID;
	}
	
}