package helperClasses;

import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
 
/**
 * The Class EncryptPasswordHelper. This class helps to hash the password.
 * 
 * @author Katharina Unruhe
 * @author Lokesh Gupta (https://howtodoinjava.com/security/how-to-generate-secure-password-hash-md5-sha-pbkdf2-bcrypt-examples/)
 */
public class EncryptPasswordHelper{
	
	/**
	 * Hashes the password with MD5.
	 *
	 * @param text Password
	 * @return Hashed password
	 * @throws NoSuchAlgorithmException No such algorithm exception
	 * @throws UnsupportedEncodingException Unsupported encoding exception
	 */
	public static String MD5(String text) throws NoSuchAlgorithmException, UnsupportedEncodingException{
		MessageDigest md;
        md = MessageDigest.getInstance("MD5");
        byte[] md5Hash = new byte[40];
        md.update(text.getBytes());
        md5Hash = md.digest();
        return convertToHex(md5Hash);
	}
	
	/**
	 * Convert to hex.
	 *
	 * @param data the data
	 * @return the string
	 */
	private static String convertToHex(byte[] data)	{ 
		StringBuilder buf = new StringBuilder();
		
		for(int i=0; i< data.length ;i++)
        {
            buf.append(Integer.toString((data[i] & 0xff) + 0x100, 16).substring(1));
        }
		
		return buf.toString();
		
	}
	
	
	
}