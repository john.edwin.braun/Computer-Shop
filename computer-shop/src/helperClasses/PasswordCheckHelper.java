package helperClasses;

/**
 * The Class PasswordCheckHelper. It helps to check if the password meets the requirements.
 * 
 * @author Katharina Unruhe
 */
public class PasswordCheckHelper {
	
	
	/**
	 * Checks if password meets requirements.
	 *
	 * @param passwort Password
	 * @return true, if Password meets requirements
	 */
	public boolean isOK(String passwort) {
		
		// Exactly 8 characters
		if (passwort.length() < 8 || passwort.length() > 8) {
			return false;
		}
		
		// Minimum of 1 capital letter
		if (numberOfCapitalLetters(passwort) < 1) {
			return false;
		}
		
		// Minimum of 1 lower case letter
		if (numberOfLowerCaseLetters(passwort) < 1) {
			return false;
		}
		
		// Minimum of 1 digit
		if (numberOfDigits(passwort) < 1) {
			return false;
		}
		
		// Minimum of 1 special character
		if (numberOfSpecialCharacters(passwort) < 1) {
			return false;
		}
		
		return true;
	} 


	/**
	 * Checks how many capital letters are in the password.
	 *
	 * @param pass Password
	 * @return Number of capital letters
	 */
	private int numberOfCapitalLetters(String pass) {
		return numberOfCharactersInIntervall(pass, 'A', 'Z');
	}
		 

	/**
	 * Checks how many lower case letters are in the password.
	 *
	 * @param pass Password
	 * @return Number of lower case letters
	 */
	private int numberOfLowerCaseLetters(String pass) {
		return numberOfCharactersInIntervall(pass, 'a', 'z');
	}



	/**
	 * Checks how many digits are in the password.
	 *
	 * @param pass Password
	 * @return Number of digits
	 */
	private int numberOfDigits(String pass) {
		return numberOfCharactersInIntervall(pass, '0', '9');
	}



	/**
	 * Checks how many special characters are in the password.
	 *
	 * @param pass Password
	 * @return Number of special characters
	 */
	private int numberOfSpecialCharacters(String pass) {
		return pass.length() -
				numberOfCapitalLetters(pass) -
				numberOfLowerCaseLetters(pass) -
				numberOfDigits(pass);
	}


		 
	/**
	 * Checks how many characters are in the given intervall.
	 *
	 * @param pass Password
	 * @param min Minimum character
	 * @param max Maximum character
	 * @return Number of characters in the intervall
	 */
	private int numberOfCharactersInIntervall(String pass, char min, char max) {
		int amount = 0;
		// Looping through password
		for (char c : pass.toCharArray()) {
			// Checking the current character
			if (c >= min && c <= max) {
				amount = amount + 1;
			}
		}
		return amount;
	}
}
