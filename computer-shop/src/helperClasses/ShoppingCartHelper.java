package helperClasses;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.http.HttpSession;

import beans.Article;

/**
 * The Class ShoppingCartHelper. It contains methods to handle the shopping cart.
 * 
 * @author John Edwin Braun
 */
public class ShoppingCartHelper {
	
	/**
	 * Adds an article to the shopping cart
	 *
	 * @param p_session Session
	 * @param p_articleNo Article number
	 * @param p_amount Amount
	 */
	@SuppressWarnings("unchecked")
	public void addToShoppingCart(HttpSession p_session, String p_articleNo, String p_amount) {
		boolean notFound = true;
		ShoppingCartItem shoppingCartItem = new ShoppingCartItem();
		List<ShoppingCartItem> shoppingCartItemList = new ArrayList<ShoppingCartItem>();
		
		// Getting the current shopping cart from the session
		shoppingCartItemList = (List<ShoppingCartItem>) p_session.getAttribute("shoppingCartItemList");
		
		// If shopping cart was not created, create one
		if(shoppingCartItemList == null) {
			shoppingCartItemList = new ArrayList<ShoppingCartItem>();
		}
		
		// Creating the shopping cart item
		shoppingCartItem.setAmount(p_amount);
		shoppingCartItem.setArticleNo(p_articleNo);
		
		// Looping through shopping cart item to look for the article to add
		for(ShoppingCartItem item : shoppingCartItemList) {
			// If the article to add is already in the shopping cart
			if(shoppingCartItem.getArticleNo().equals(item.getArticleNo())) {
				// Increase amount
				
				int oldAmount = Integer.parseInt(item.getAmount());
				int newAmount = oldAmount + Integer.parseInt(shoppingCartItem.getAmount());
				
				item.setAmount(Integer.toString(newAmount));
				notFound = false;
			}
		}
		
		// If the article was not found add it to the shopping cart
		if(notFound){
			shoppingCartItemList.add(shoppingCartItem);
		}
		
		// Putting the shopping cart back into the session
		p_session.setAttribute("shoppingCartItemList", shoppingCartItemList);
		
	}
	
	/**
	 * Get all shopping cart items from the shopping cart.
	 *
	 * @param p_session Session
	 * @return List of shopping cart items
	 */
	@SuppressWarnings("unchecked")
	public List<ShoppingCartItem> getShoppingCartItemList(HttpSession p_session){
		List<ShoppingCartItem> shoppingCartItemList = new ArrayList<ShoppingCartItem>();
		DatabaseHelper db_access = null;
		Integer counter = 1;
		
		// Getting shopping cart item list from session
		shoppingCartItemList = (List<ShoppingCartItem>) p_session.getAttribute("shoppingCartItemList");
		
		// If there are no shopping cart items return null
		if(shoppingCartItemList == null) {
			return null;
		}
		else {
			try {
				db_access = new DatabaseHelper();
				
				// Looping through the list of shopping cart items and setting more data
				for(ShoppingCartItem temp : shoppingCartItemList) {
					Article article = new Article();
					article = db_access.getArticle(temp.getArticleNo());
					temp.setArticleName(article.getName());
					temp.setPrice(article.getPrice());
					temp.setPositionNo(counter.toString());
					temp.setPictureLink(article.getPictureLink());
					counter++;
				}
				
				return shoppingCartItemList;
			}
			catch (NullPointerException|SQLException ex) {
				ex.printStackTrace();
			}
			
			return null;
		}
		
	}
	
	/**
	 * Delete a position from the shopping cart.
	 *
	 * @param p_session Session
	 * @param p_positionNo Position number
	 */
	public void deletePosition(HttpSession p_session, String p_positionNo) {
		// Getting the shopping cart from the session
		@SuppressWarnings("unchecked")
		List<ShoppingCartItem> shoppingCartItemList = (List<ShoppingCartItem>) p_session.getAttribute("shoppingCartItemList");
		
		// Removing the position from the shopping cart
		shoppingCartItemList.remove(Integer.parseInt(p_positionNo)-1);
	}
	
	/**
	 * Deleting all positions from the shopping cart.
	 *
	 * @param p_session Session
	 */
	public void deleteAll(HttpSession p_session) {
		// Getting the shopping cart from the session
		@SuppressWarnings("unchecked")
		List<ShoppingCartItem> shoppingCartItemList = (List<ShoppingCartItem>) p_session.getAttribute("shoppingCartItemList");
		
		// Clearing the shopping cart
		shoppingCartItemList.clear();
	}
}
