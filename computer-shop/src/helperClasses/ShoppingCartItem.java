package helperClasses;

/**
 * The Class ShoppingCartItem. This class represents one item from a shopping cart.
 */
public class ShoppingCartItem {
	
	/** The article number. */
	private String articleNo;
	
	/** The article name. */
	private String articleName;
	
	/** The price. */
	private String price;
	
	/** The amount. */
	private String amount;
	
	/** The position number. */
	private String positionNo;
	
	/** The picture link. */
	private String pictureLink;
	
	/**
	 * Gets the picture link.
	 *
	 * @return the picture link
	 */
	public String getPictureLink() {
		return pictureLink;
	}
	
	/**
	 * Sets the picture link.
	 *
	 * @param pictureLink the new picture link
	 */
	public void setPictureLink(String pictureLink) {
		this.pictureLink = pictureLink;
	}
	
	/**
	 * Gets the position number.
	 *
	 * @return the position number
	 */
	public String getPositionNo() {
		return positionNo;
	}
	
	/**
	 * Sets the position number.
	 *
	 * @param positionNo the new position number
	 */
	public void setPositionNo(String positionNo) {
		this.positionNo = positionNo;
	}
	
	/**
	 * Gets the article name.
	 *
	 * @return the article name
	 */
	public String getArticleName() {
		return articleName;
	}
	
	/**
	 * Sets the article name.
	 *
	 * @param articleName the new article name
	 */
	public void setArticleName(String articleName) {
		this.articleName = articleName;
	}
	
	/**
	 * Gets the price.
	 *
	 * @return the price
	 */
	public String getPrice() {
		return price;
	}
	
	/**
	 * Sets the price.
	 *
	 * @param price the new price
	 */
	public void setPrice(String price) {
		this.price = price;
	}
	
	/**
	 * Gets the article number.
	 *
	 * @return the article number
	 */
	public String getArticleNo() {
		return articleNo;
	}
	
	/**
	 * Sets the article number.
	 *
	 * @param articleNo the new article number
	 */
	public void setArticleNo(String articleNo) {
		this.articleNo = articleNo;
	}
	
	/**
	 * Gets the amount.
	 *
	 * @return the amount
	 */
	public String getAmount() {
		return amount;
	}
	
	/**
	 * Sets the amount.
	 *
	 * @param amount the new amount
	 */
	public void setAmount(String amount) {
		this.amount = amount;
	}
}
