package servlets;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class AdditionalOrderData.
 * 
 * @author John Edwin Braun
 */
@WebServlet("/AdditionalOrderData")
public class AdditionalOrderData extends HttpServlet {
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;
       
    /**
     * Instantiates a new Servlet.
     *
     * @see HttpServlet#HttpServlet()
     */
    public AdditionalOrderData() {
        super();
    }

	/**
	 * Do get.
	 *
	 * @param request the request
	 * @param response the response
	 * @throws ServletException the servlet exception
	 * @throws IOException Signals that an I/O exception has occurred.
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// Call JSP
		request.getRequestDispatcher("WEB-INF/AdditionalOrderData.jsp").forward(request, response);
	}

	/**
	 * Do post.
	 *
	 * @param request the request
	 * @param response the response
	 * @throws ServletException the servlet exception
	 * @throws IOException Signals that an I/O exception has occurred.
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");
		
		String deliveryStreet = "";
		String deliveryHouseNumber = "";
		String deliveryTown = "";
		String deliveryPostalCode = "";
		String dispatchType = "";
		String paymentMethod = "";
		String deliveryFirstname = "";
		String deliveryName = "";
		
		// Getting the data from the JSP
		deliveryStreet = request.getParameter("deliveryStreet");
		deliveryHouseNumber = request.getParameter("deliveryHouseNumber");
		deliveryTown = request.getParameter("deliveryTown");
		deliveryPostalCode = request.getParameter("deliveryPostalCode");
		dispatchType = request.getParameter("dispatchType");
		paymentMethod = request.getParameter("paymentMethod");
		deliveryFirstname = request.getParameter("deliveryFirstname");
		deliveryName = request.getParameter("deliveryName");
		
		// Saving the data into the request
		request.setAttribute("deliveryStreet", deliveryStreet);
		request.setAttribute("deliveryHouseNumber", deliveryHouseNumber);
		request.setAttribute("deliveryTown", deliveryTown);
		request.setAttribute("deliveryPostalCode", deliveryPostalCode);
		request.setAttribute("dispatchType", dispatchType);
		request.setAttribute("paymentMethod", paymentMethod);
		request.setAttribute("deliveryFirstname", deliveryFirstname);
		request.setAttribute("deliveryName", deliveryName);
		
		// Setting the attribute destination in the request to make sure the handling in the following Servlet
		request.setAttribute("destination", "OrderOverview");
		
		// Forwarding the request to the order overview
		request.getRequestDispatcher("OrderOverview").forward(request, response);
	}

}
