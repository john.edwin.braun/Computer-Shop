package servlets;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.Article;
import helperClasses.DatabaseHelper;

/**
 * Servlet implementation class ArticleOverview.
 * 
 * @author Oliver Neumann
 */
@WebServlet("/AdminArticleOverview")
public class AdminArticleOverview extends HttpServlet {
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;
       
    /**
     * Instantiates a new Servlet.
     *
     * @see HttpServlet#HttpServlet()
     */
    public AdminArticleOverview() {
        super();
    }

	/**
	 * Do get.
	 *
	 * @param request the request
	 * @param response the response
	 * @throws ServletException the servlet exception
	 * @throws IOException Signals that an I/O exception has occurred.
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession();
		DatabaseHelper db_access = null;
		String sortCrit = "Name asc";
		List<Article> articleList = new ArrayList<Article>();
		
		// Preventing NullPointerException
		if(session.getAttribute("admin") == null) {
			session.setAttribute("admin", "false");
		}
		
		// Checking of user is an admin
		if(session.getAttribute("admin").equals("true")) {
			
			db_access =  new DatabaseHelper();
			
			sortCrit = request.getParameter("sortCrit");
			
			try {
				// Getting the article from the database
				articleList = db_access.getAllArticles(sortCrit);
			
				// Saving the articles in the request
				request.setAttribute("articleList", articleList);
				request.setAttribute("title", "Alle Artikel");
				
				// Call JSP
				request.getRequestDispatcher("WEB-INF/AdminArticleOverview.jsp").forward(request, response);
			} 
			catch (SQLException|NullPointerException e) {
				e.printStackTrace();
				request.setAttribute("error", "Fehler beim Lesen der Datenbank");
				request.getRequestDispatcher("WEB-INF/Error.jsp").forward(request, response);
			}
			
		}
		else{
			// Forwarding to error page
			request.setAttribute("error", "Keine Zugruffsberechtigung!");
			request.getRequestDispatcher("WEB-INF/Error.jsp").forward(request, response);
		}
		
		

			
	}

	/**
	 * Do post.
	 *
	 * @param request the request
	 * @param response the response
	 * @throws ServletException the servlet exception
	 * @throws IOException Signals that an I/O exception has occurred.
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		doGet(request, response);
		
	}

}
