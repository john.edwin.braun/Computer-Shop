package servlets;

import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.Article;
import beans.Review;
import helperClasses.DatabaseHelper;

/**
 * Servlet implementation class ArticleView.
 * 
 * @author Oliver Neumann
 */
@WebServlet("/AdminArticleView")
public class AdminArticleView extends HttpServlet {
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;
       
    /**
     * Instantiates a new Servlet.
     *
     * @see HttpServlet#HttpServlet()
     */
    public AdminArticleView() {
        super();
    }

	/**
	 * Do get.
	 *
	 * @param request the request
	 * @param response the response
	 * @throws ServletException the servlet exception
	 * @throws IOException Signals that an I/O exception has occurred.
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String articleNo = null;
		Article article = null;
		DatabaseHelper db_access = null;
		List<Review> reviewList = null;
		HttpSession session = null;
		
		session = request.getSession();
		
		// Preventing NullPointerException
		if(session.getAttribute("admin") == null) {
			session.setAttribute("admin", "false");
		}
		
		// Check if user is an admin
		if(session.getAttribute("admin").equals("true")) {
			
			// Getting the parameter from the URL
			articleNo = request.getParameter("ArticleNo");
			
			// Handling the situation when there is no article number in the URL (when changes where submitted)
			if(articleNo == null) {
				articleNo = (String) session.getAttribute("articleNo");
				session.setAttribute("ArticleNo", articleNo);
			}

			try {
				// Creating database connection
				db_access = new DatabaseHelper();
				
				// Getting the articles and the reviews from the database
				article = db_access.getArticle(articleNo);
				reviewList = db_access.getReviewsByArticleNo(articleNo);
				
				if(article != null) {
					// Forwarding the data to the JSP
					request.setAttribute("article", article);
					request.setAttribute("reviews", reviewList);
					
					request.getRequestDispatcher("WEB-INF/AdminArticleView.jsp" + "?ArticleNo=" + articleNo).forward(request, response);
					request.removeAttribute("articleChanged");
				}
				else {
					request.setAttribute("error", "Keine Artikel mit der ID aus der URL");
					request.getRequestDispatcher("WEB-INF/Error.jsp").forward(request, response);
				}
				
			}
			catch (SQLException|NullPointerException e) {
				e.printStackTrace();
				request.setAttribute("error", "Fehler beim Lesen der Datenbank");
				request.getRequestDispatcher("WEB-INF/Error.jsp").forward(request, response);
			}	
		}
		else {
			// Forwarding to error page
			request.setAttribute("error", "Keine Zugruffsberechtigung!");
			request.getRequestDispatcher("WEB-INF/Error.jsp").forward(request, response);
		}
		
	}

	/**
	 * Do post.
	 *
	 * @param request the request
	 * @param response the response
	 * @throws ServletException the servlet exception
	 * @throws IOException Signals that an I/O exception has occurred.
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");
		
		Article article = new Article();
		DatabaseHelper db_access = null;
		HttpSession session = null;
		
		session = request.getSession();
		
		// Getting data from JSP
		article.setName(request.getParameter("adminName"));
		article.setPrice(request.getParameter("adminPrice"));
		article.setCategory(request.getParameter("adminCategory"));
		article.setDescription(request.getParameter("adminDescription"));
		article.setArticleNo((String)session.getAttribute("articleNo"));
		article.setMinStock(request.getParameter("adminMinStock"));
		article.setCurrentStock(request.getParameter("adminCurrentStock"));
		article.setPictureLink("PicrureLink");
		article.setStatus(request.getParameter("adminStatus"));

		try {			
			// Update article in the database
			db_access  = new DatabaseHelper();
			db_access.updateArticle(article);
			
			// Setting the success message
			request.setAttribute("articleChanged", "true");
		} 
		
		catch (Exception e) {
			e.printStackTrace();
		}
		
		doGet(request, response);
	}
}