package servlets;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.Question;
import helperClasses.DatabaseHelper;

/**
 * Servlet implementation class AnswerQuestions.
 * 
 * @author John Edwin Braun
 */
@WebServlet("/AnswerQuestions")
public class AnswerQuestions extends HttpServlet {
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;
       
    /**
     * Instantiates a new Servlet.
     *
     * @see HttpServlet#HttpServlet()
     */
    public AnswerQuestions() {
        super();
    }

	/**
	 * Do get.
	 *
	 * @param request the request
	 * @param response the response
	 * @throws ServletException the servlet exception
	 * @throws IOException Signals that an I/O exception has occurred.
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession();
		List<Question> questionList = new ArrayList<Question>();
		
		// Preventing NullPointerException
		if(session.getAttribute("admin") == null) {
			session.setAttribute("admin", "false");
		}
		
		// Checking if user is an admin
		if(session.getAttribute("admin").equals("true")) {
			DatabaseHelper dbHelper = new DatabaseHelper();
			
			// Getting all questions from the database
			questionList = dbHelper.getAllQuestions();
			
			// Saving the data in the request
			request.setAttribute("questionList", questionList);
			
			// Calling JSP
			request.getRequestDispatcher("WEB-INF/AnswerQuestions.jsp").forward(request, response);
			request.removeAttribute("questionAnswered");
		}
		else{
			// Forwarding to error page
			request.setAttribute("error", "Keine Zugriffsberechtigung!");
			request.getRequestDispatcher("WEB-INF/Error.jsp").forward(request, response);
		}
		
	
	}

	/**
	 * Do post.
	 *
	 * @param request the request
	 * @param response the response
	 * @throws ServletException the servlet exception
	 * @throws IOException Signals that an I/O exception has occurred.
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

}
