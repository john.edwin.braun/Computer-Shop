package servlets;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.Question;
import helperClasses.DatabaseHelper;

/**
 * Servlet implementation class AnswerQuestionsDetail.
 * 
 * @author John Edwin Braun
 */
@WebServlet("/AnswerQuestionsDetail")
public class AnswerQuestionsDetail extends HttpServlet {
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;
       
    /**
     * Instantiates a new Servlet.
     *
     * @see HttpServlet#HttpServlet()
     */
    public AnswerQuestionsDetail() {
        super();
    }

	/**
	 * Do get.
	 *
	 * @param request the request
	 * @param response the response
	 * @throws ServletException the servlet exception
	 * @throws IOException Signals that an I/O exception has occurred.
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession();
		
		// Preventing NullPointerException
		if(session.getAttribute("admin") == null) {
			session.setAttribute("admin", "false");
		}
		
		// Checking if user is an admin
		if(session.getAttribute("admin").equals("true")) {
			DatabaseHelper dbHelper = new DatabaseHelper();
			Question question = new Question();
			
			// Getting the data from the URL and the question from the database
			question = dbHelper.getQuestion(request.getParameter("ID"));
			
			// Saving data in request
			request.setAttribute("question", question);
			
			// Call JSP
			request.getRequestDispatcher("WEB-INF/AnswerQuestionsDetail.jsp").forward(request, response);
		}
		else{
			// Forwarding to error page
			request.setAttribute("error", "Keine Zugriffsberechtigung!");
			request.getRequestDispatcher("WEB-INF/Error.jsp").forward(request, response);
		}
	}

	/**
	 * Do post.
	 *
	 * @param request the request
	 * @param response the response
	 * @throws ServletException the servlet exception
	 * @throws IOException Signals that an I/O exception has occurred.
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");
		
		Question question = new Question();
		DatabaseHelper dbHelper = new DatabaseHelper();
		
		// Getting data from JSP
		question.setID(request.getParameter("ID"));
		question.setAnswer(request.getParameter("answer"));
		question.setDate(request.getParameter("date"));
		question.setQuestion(request.getParameter("question"));
		question.setQuestioner(request.getParameter("questioner"));
		question.setTitle(request.getParameter("title"));
		question.setActive(request.getParameter("active"));
		
		//Update question in the database and if successful forward to AnswerQuestion Servlet
		if(dbHelper.updateQuestion(question)) {
			request.setAttribute("questionAnswered", "true");
			request.getRequestDispatcher("AnswerQuestions").forward(request, response);
		}
		else {
			// Calling error page
			request.setAttribute("message", "Frage konnte nicht beantwortet werden. Datenbankfehler");
			request.getRequestDispatcher("WEB-INF/Error.jsp").forward(request, response);
		}
		
		
	}

}
