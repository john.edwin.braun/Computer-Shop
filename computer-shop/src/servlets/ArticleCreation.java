package servlets;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.Article;
import helperClasses.DatabaseHelper;

/**
 * Servlet implementation class ArticleCreation.
 * 
 * @author Oliver Neumann
 */
@WebServlet("/ArticleCreation")
public class ArticleCreation extends HttpServlet {
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;
       
    /**
     * Instantiates a new Servlet.
     *
     * @see HttpServlet#HttpServlet()
     */
    public ArticleCreation() {
        super();
    }

	/**
	 * Do get.
	 *
	 * @param request the request
	 * @param response the response
	 * @throws ServletException the servlet exception
	 * @throws IOException Signals that an I/O exception has occurred.
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = null;
		session = request.getSession();
		
		// Preventing NullPointerException
		if(session.getAttribute("admin") == null) {
			session.setAttribute("admin", "false");
		}
		
		// Checking if user is an admin
		if(session.getAttribute("admin").equals("true")) {
			// Calling JSP
			request.getRequestDispatcher("/WEB-INF/ArticleCreation.jsp").forward(request, response);
		}
		else {
			// Calling error page
			request.setAttribute("error", "Keine Zugruffsberechtigung!");
			request.getRequestDispatcher("WEB-INF/Error.jsp").forward(request, response);
		}
		
		
	}

	/**
	 * Do post.
	 *
	 * @param request the request
	 * @param response the response
	 * @throws ServletException the servlet exception
	 * @throws IOException Signals that an I/O exception has occurred.
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");
		
		DatabaseHelper db_access = null;
		Article article = new Article();
		
		
		// Getting data from JSP
		article.setName(request.getParameter("name"));
		article.setPrice(request.getParameter("price"));
		article.setCategory(request.getParameter("category"));
		article.setDescription(request.getParameter("description"));
		article.setMinStock(request.getParameter("minStock"));
		article.setCurrentStock(request.getParameter("currentStock"));
		article.setPictureLink("/Pictures");
		article.setStatus(request.getParameter("status"));
		

		try {
			db_access  = new DatabaseHelper();
			
			// Insert article into database and get the article number
			article.setArticleNo(db_access.insertArticle(article));
		} 
		
		catch (Exception e) {
			e.printStackTrace();
		}
		
		// Redirect to UploadServlet
		response.sendRedirect("UploadServlet?ArticleID=" + article.getArticleNo());

	}

}
