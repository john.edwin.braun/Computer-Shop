package servlets;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import beans.Article;
import helperClasses.DatabaseHelper;

/**
 * Servlet implementation class ArticleOverview.
 * 
 * @author John Edwin Braun
 */
@WebServlet("/ArticleOverview")
public class ArticleOverview extends HttpServlet {
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
    /**
     * Instantiates a new Servlet.
     *
     * @see HttpServlet#HttpServlet()
     */
    public ArticleOverview() {
        super();
    }

	/**
	 * Do get.
	 *
	 * @param request the request
	 * @param response the response
	 * @throws ServletException the servlet exception
	 * @throws IOException Signals that an I/O exception has occurred.
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		DatabaseHelper db_access = null;
		String sortCrit = "";
		String category = "";
		String search = "";
		List<Article> articleList = new ArrayList<Article>();
		
		// Getting search Parameter from URL
		search = request.getParameter("search");
			
		// Getting sort criteria from URL
		if(request.getParameter("sortCrit").equals("")) {
			sortCrit = "Name asc";
		}
		else {
			sortCrit = request.getParameter("sortCrit");
		}
		
		// Getting category from URL
		if(request.getParameter("Category").equals("")) {
			category = "All";
		}
		else {
			category = request.getParameter("Category");
		}
	
		db_access =  new DatabaseHelper();
		
		// Preventing NullPointerException
		if(search == null) {
			search = "";
		}
		
		// If there is a search String in the URL
		if(!search.equals("")){
			try {
				
				// Getting the search result by the search string, the sort criteria and the category
				articleList = db_access.getSearchResults(search, sortCrit, category);
				
				// Setting the relevant data in the request
				request.setAttribute("title", "Suche: " + search);
				request.setAttribute("articleList", articleList);
				request.setAttribute("category", category);
				request.setAttribute("sortCrit", sortCrit);
				request.setAttribute("search", search);
				
				// Calling JSP
				request.getRequestDispatcher("WEB-INF/ArticleOverview.jsp").forward(request, response);
			}catch(SQLException ex) {
				ex.printStackTrace();
				
				// Calling error page
				request.setAttribute("error", "Fehler beim Lesen der Datenbank");
				request.getRequestDispatcher("WEB-INF/Error.jsp").forward(request, response);
			}
			
		}
		// Else (no search String in the URL)
		else {
			
			// If category equals all
			if(category.equals("All")) {
				try {
					// Getting all active articles from the database
					category = "1,2,3,4,5,6,7,8,9,10,11,12,13,14";
					articleList = db_access.getAllActiveArticles(sortCrit, category);

					// Setting relevant data in the reqeust
					request.setAttribute("articleList", articleList);
					request.setAttribute("title", "Alle Artikel");
					request.setAttribute("category", "All");
					request.setAttribute("sortCrit", sortCrit);
					
					// Calling JSP
					request.getRequestDispatcher("WEB-INF/ArticleOverview.jsp").forward(request, response);
				} 
				catch (SQLException|NullPointerException e) {
					e.printStackTrace();
					
					// Calling error page
					request.setAttribute("error", "Fehler beim Lesen der Datenbank");
					request.getRequestDispatcher("WEB-INF/Error.jsp").forward(request, response);
				}
			}
			
			// If category equals accesory
			else if (category.equals("Accessory")){
				try {
					// Getting active accessory from database
					articleList = db_access.getAccessory(sortCrit);
				
					// Setting relevant data in the request
					request.setAttribute("articleList", articleList);
					request.setAttribute("title", "Zubeh�r");
					request.setAttribute("category", "Accessory");
					request.setAttribute("sortCrit", sortCrit);
					
					// Calling JSP
					request.getRequestDispatcher("WEB-INF/ArticleOverview.jsp").forward(request, response);
				} 
				catch (SQLException|NullPointerException e) {
					e.printStackTrace();
					
					// Calling error page
					request.setAttribute("error", "Fehler beim Lesen der Datenbank");
					request.getRequestDispatcher("WEB-INF/Error.jsp").forward(request, response);
				}
			}
			
			// If category equals PC
			else if(category.equals("PC")) {
				try {
					// Getting the PCs from the database
					category = "1";
					articleList = db_access.getAllActiveArticles(sortCrit, category);

					// Setting relevant data in the request
					request.setAttribute("articleList", articleList);
					request.setAttribute("title", "PC");
					request.setAttribute("category", "PC");
					request.setAttribute("sortCrit", sortCrit);
					
					// Calling JSP
					request.getRequestDispatcher("WEB-INF/ArticleOverview.jsp").forward(request, response);
				} 
				catch (SQLException|NullPointerException e) {
					e.printStackTrace();
					
					// Calling error page
					request.setAttribute("error", "Fehler beim Lesen der Datenbank");
					request.getRequestDispatcher("WEB-INF/Error.jsp").forward(request, response);
				}
			}
			
			// If category equals Monitor
			else if(category.equals("Monitor")) {
				try {
					// Getting monitors from database
					category = "3";
					articleList = db_access.getAllActiveArticles(sortCrit, category);

					// Setting relevant data in request
					request.setAttribute("articleList", articleList);
					request.setAttribute("title", "Monitor");
					request.setAttribute("category", "Monitor");
					request.setAttribute("sortCrit", sortCrit);
					
					// Calling JSP
					request.getRequestDispatcher("WEB-INF/ArticleOverview.jsp").forward(request, response);
				} 
				catch (SQLException|NullPointerException e) {
					e.printStackTrace();
					
					// Calling error page
					request.setAttribute("error", "Fehler beim Lesen der Datenbank");
					request.getRequestDispatcher("WEB-INF/Error.jsp").forward(request, response);
				}
			}
			
			// If category equals laptop
			else if(category.equals("Laptop")) {
				try {
					// Getting notebooks from database
					category = "4";
					articleList = db_access.getAllActiveArticles(sortCrit, category);

					// Setting relevant data in the request
					request.setAttribute("articleList", articleList);
					request.setAttribute("title", "Laptop");
					request.setAttribute("category", "Laptop");
					request.setAttribute("sortCrit", sortCrit);
					
					// Calling JSP
					request.getRequestDispatcher("WEB-INF/ArticleOverview.jsp").forward(request, response);
				} 
				catch (SQLException|NullPointerException e) {
					e.printStackTrace();
					
					// Calling error page
					request.setAttribute("error", "Fehler beim Lesen der Datenbank");
					request.getRequestDispatcher("WEB-INF/Error.jsp").forward(request, response);
				}
			}
		}
		
		
		
		
	}

	/**
	 * Do post.
	 *
	 * @param request the request
	 * @param response the response
	 * @throws ServletException the servlet exception
	 * @throws IOException Signals that an I/O exception has occurred.
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
		
	}
}