package servlets;

import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.Article;
import beans.Review;
import helperClasses.DatabaseHelper;
import helperClasses.ShoppingCartHelper;

/**
 * Servlet implementation class ArticleView.
 * 
 * @author John Edwin Braun
 */
@WebServlet("/ArticleView")
public class ArticleView extends HttpServlet {
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	/** The number of reviews. */
	private final int numberOfReviews = 5;
       
    /**
     * Instantiates a new Servlet.
     *
     * @see HttpServlet#HttpServlet()
     */
    public ArticleView() {
        super();
    }

	/**
	 * Do get.
	 *
	 * @param request the request
	 * @param response the response
	 * @throws ServletException the servlet exception
	 * @throws IOException Signals that an I/O exception has occurred.
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String articleNo = null;
		Article article = null;
		DatabaseHelper db_access = null;
		List<Review> reviewList = null;
		HttpSession session = null;
		String customerNo = "";
		
		//Getting data from URL
		articleNo = request.getParameter("ArticleNo");
		if(articleNo == null) {
			articleNo = (String) request.getAttribute("articleNo");
		}

		try {
			db_access = new DatabaseHelper();
			
			// Getting the article and reviews from database
			article = db_access.getArticle(articleNo);
			reviewList = db_access.getReviewsByArticleNo(articleNo);
			
			// Getting the user from the session
			session = request.getSession();
			customerNo = db_access.getCustomerNoByEMail((String)session.getAttribute("user"));
			
			// If article was found
			if(article != null) {
				// Setting data in request
				request.setAttribute("article", article);
				request.setAttribute("reviews", reviewList);
				request.setAttribute("maxNoOfReviews", reviewList.size());
				
				// Setting the review status (not logged in, article already reviewed or article not reviewed)
				if(session.getAttribute("user") == null || session.getAttribute("user").equals("")) {
					request.setAttribute("reviewStatus", "notLoggedIn");
				}
				else if(db_access.customerReviewedArticle(article.getArticleNo(), customerNo)) {
					request.setAttribute("reviewStatus", "alreadyReviewed");
				}
				else {
					request.setAttribute("reviewStatus", "notReviewed");
				}
				
				// Handling: Article added to shopping cart
				String addedToShoppingCart = (String) request.getAttribute("addedToShoppingCart");
				request.setAttribute("addedToShoppingCart", addedToShoppingCart);
				
				// Handling: Review saved
				String savedReview = (String) request.getAttribute("savedReview");
				request.setAttribute("savedReview", savedReview);
				
				// Initial amount of reviews showed on site
				if(request.getAttribute("numberOfReviews") == null) {
					request.setAttribute("numberOfReviews", numberOfReviews);
				}
				// Else (Increased amount of reviews on site)
				else {
					int currentNoOfReviews = (int) request.getAttribute("numberOfReviews");
					request.setAttribute("numberOfReviews", currentNoOfReviews);
				}
				
				// Calling JSP
				request.getRequestDispatcher("WEB-INF/ArticleView.jsp" + "?ArticleNo=" + articleNo).forward(request, response);
			}
			// Else (article not found)
			else {
				// Calling error page
				request.setAttribute("error", "Keine Artikel mit der ID aus der URL");
				request.getRequestDispatcher("WEB-INF/Error.jsp").forward(request, response);
			}
			
		}
		catch (SQLException|NullPointerException e) {
			e.printStackTrace();
			
			// Calling error page
			request.setAttribute("error", "Fehler beim Lesen der Datenbank");
			request.getRequestDispatcher("WEB-INF/Error.jsp").forward(request, response);
		}
		
	}

	/**
	 * Do post.
	 *
	 * @param request the request
	 * @param response the response
	 * @throws ServletException the servlet exception
	 * @throws IOException Signals that an I/O exception has occurred.
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");
		// Getting submit from JSP
		String submitMessage = request.getParameter("submit");
		request.setAttribute("articleNo", request.getParameter("articleNo"));
		
		// Handling the submit
		if(submitMessage.equals("addToShoppingCart")) {
			addToShoppingCart(request);
			
			doGet(request, response);
		}
		else if(submitMessage.equals("loadMoreReviews")) {
			loadMoreReviews(request);
			
			doGet(request, response);
		}
		else if(submitMessage.equals("sendReview")) {
			if(sendReview(request)) {
				doGet(request, response);
			}
			else {
				// Calling error page
				request.setAttribute("error", "Fehler beim Lesen der Datenbank");
				request.getRequestDispatcher("WEB-INF/Error.jsp").forward(request, response);
			}
		}
		
	}
	
	/**
	 * Adds the article to the shopping cart.
	 *
	 * @param request the request
	 */
	private void addToShoppingCart(HttpServletRequest request) {
		ShoppingCartHelper shoppingCart = new ShoppingCartHelper();
		
		//Adding article to shopping cart
		shoppingCart.addToShoppingCart(request.getSession(), request.getParameter("articleNo"), request.getParameter("amount"));
		request.setAttribute("addedToShoppingCart", "true");
	}
	
	/**
	 * Load more reviews.
	 *
	 * @param request the request
	 */
	private void loadMoreReviews(HttpServletRequest request) {
		// Increasing number of reviews on site
		int currentNoOfReviews = Integer.parseInt(request.getParameter("numberOfReviews"));
		request.setAttribute("numberOfReviews", currentNoOfReviews + numberOfReviews);
	}
	
	/**
	 * Send review.
	 *
	 * @param request the request
	 * @return true, if successful
	 */
	private boolean sendReview(HttpServletRequest request) {
		try {
			String customerNo = "";
			DatabaseHelper dbHelper = null;
			HttpSession session = null;
			dbHelper = new DatabaseHelper();
			session = request.getSession();
			Review review = new Review();
			
			// getting data from JSP
			customerNo = dbHelper.getCustomerNoByEMail((String)session.getAttribute("user"));
			review.setArticleNo(request.getParameter("articleNo"));
			review.setNumberOfStars(request.getParameter("stars"));
			review.setText(request.getParameter("reviewText"));
			review.setTitle(request.getParameter("reviewTitle"));
			
			// Insert review into database
			dbHelper.insertReview(review, customerNo);
			
			request.setAttribute("savedReview", "true");
			
			return true;
		}
		catch(SQLException|NullPointerException e) {
			e.printStackTrace();
			
			return false;
		}
	}
}