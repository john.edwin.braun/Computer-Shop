package servlets;

import java.io.IOException;


import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.Customer;
import helperClasses.DatabaseHelper;

/**
 * Servlet implementation class ChgUsrAdress.
 * 
 * @author Jan-Niklas Posselt
 */
@WebServlet("/ChgUsrAdress")
public class ChgUsrAddress extends HttpServlet {
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
    /**
     * Instantiates a new Servlet.
     *
     * @see HttpServlet#HttpServlet().
     */
    public ChgUsrAddress() {
        super();
    }

	/**
	 * Do get.
	 *
	 * @param request the request
	 * @param response the response
	 * @throws ServletException the servlet exception
	 * @throws IOException Signals that an I/O exception has occurred.
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession();
		
		// If user is logged in call JSP
		if(session.getAttribute("user") != null) {
			request.getRequestDispatcher("/WEB-INF/ChgUsrAddress.jsp").forward(request, response);
		}
		// Else call error page
		else {
			request.setAttribute("error", "Keine Zugriffsberechtigung!");
			request.getRequestDispatcher("WEB-INF/Error.jsp").forward(request, response);
		}
		
		
		
	}

	/**
	 * Do post.
	 *
	 * @param request the request
	 * @param response the response
	 * @throws ServletException the servlet exception
	 * @throws IOException Signals that an I/O exception has occurred.
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");
	
		DatabaseHelper db_access = null;
		HttpSession session = null;
		String customerNo = "";

		
		try 
		{
			db_access = new DatabaseHelper();
											
			Customer customer = new Customer();
			session = request.getSession();
			
			// Get customer number and customer from database
			customerNo = db_access.getCustomerNoByEMail((String)session.getAttribute("user"));
			customer = db_access.getCustomer(customerNo);
			
			// Getting data from JSP
			customer.setInvoiceStreet(request.getParameter("newInvoiceStreet"));
			customer.setInvoiceHouseNumber(request.getParameter("newInvoiceHouseNumber"));
			customer.setInvoicePostalCode(request.getParameter("newInvoicePostalCode"));
			customer.setInvoiceTown(request.getParameter("newInvoiceTown"));	
			customer.setCustomerNo(customerNo);
			
			// Save customer in database
			db_access.changeAddress(customer);
			
			session.setAttribute("customer", customer);
			request.setAttribute("addressChanged", true);
			
			doGet(request, response);
			
		
		}
			catch (Exception e)
			{
				e.printStackTrace();
			}		
		
		
	}
}
