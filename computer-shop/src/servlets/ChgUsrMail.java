package servlets;

import java.io.IOException;
import java.sql.SQLIntegrityConstraintViolationException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.Customer;
import helperClasses.DatabaseHelper;

/**
 * Servlet implementation class ChgUsrMail.
 * 
 * @author Jan-Niklas Posselt
 */
@WebServlet("/ChgUsrMail")
public class ChgUsrMail extends HttpServlet {
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
    /**
     * Instantiates a new chg usr mail.
     *
     * @see HttpServlet#HttpServlet().
     */
    public ChgUsrMail() {
        super();
    }

	/**
	 * Do get.
	 *
	 * @param request the request
	 * @param response the response
	 * @throws ServletException the servlet exception
	 * @throws IOException Signals that an I/O exception has occurred.
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession();
		
		// If user is logged in call JSP
		if(session.getAttribute("user") != null) {
			request.getRequestDispatcher("/WEB-INF/ChgUsrMail.jsp").forward(request, response);
		}
		// Else call error page
		else {
			request.setAttribute("error", "Keine Zugriffsberechtigung!");
			request.getRequestDispatcher("WEB-INF/Error.jsp").forward(request, response);
		}
		
		
	}

	/**
	 * Do post.
	 *
	 * @param request the request
	 * @param response the response
	 * @throws ServletException the servlet exception
	 * @throws IOException Signals that an I/O exception has occurred.
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");
		
		DatabaseHelper db_access = null;
		HttpSession session = null;
		String customerNo = "";
		String currentMail = "";
		
		try 
		{
			db_access = new DatabaseHelper();
			Customer customer = new Customer();
			session = request.getSession();
			
			// Getting customer number and customer from database
			customerNo = db_access.getCustomerNoByEMail((String)session.getAttribute("user"));
			customer = db_access.getCustomer(customerNo);
			
			// Getting current Mail from session
			currentMail = (String)session.getAttribute("user");
			
			// Comparing old email from JSP and email from session
			if(request.getParameter("oldmail").equals(currentMail) ){	
				// Comparing new emails from JSP
				if( request.getParameter("newmail1").equals(request.getParameter("newmail2"))){	
					// Getting new mail from JSP
					customer.setEmail(request.getParameter("newmail1"));
					customer.setCustomerNo(customerNo);
					
					try {
						// Save new email in database
						db_access.changeEmail(customer);
						session.setAttribute("user", customer.getEmail());
						session.setAttribute("customer", customer);
					}
					catch (SQLIntegrityConstraintViolationException e1) {
						e1.printStackTrace();
						request.setAttribute("message", "Diese E-Mail wird bereits verwendet");
						doGet(request, response);
					}
					request.setAttribute("mailChanged", true);
				}
				
				else {
					request.setAttribute("message", "Die neuen E-Mail Adressen stimmen nicht �berein.");
				}
			}
		 	else{
				request.setAttribute("message", "Die Alte E-Mail Adresse stimmt nicht.");
			}
			
			doGet(request, response);
	
		}
		catch (Exception e){
			e.printStackTrace();
		}		
		

	}
}
