package servlets;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.Customer;
import helperClasses.PasswordCheckHelper;
import helperClasses.DatabaseHelper;
import helperClasses.EncryptPasswordHelper;

/**
 * Servlet implementation class ChgUsrPass.
 * 
 * @author Jan-Niklas Posselt
 */
@WebServlet("/ChgUsrPass")
public class ChgUsrPass extends HttpServlet {
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
    /**
     * Instantiates a new Servlet.
     *
     * @see HttpServlet#HttpServlet().
     */
    public ChgUsrPass() {
        super();
    }

	/**
	 * Do get.
	 *
	 * @param request the request
	 * @param response the response
	 * @throws ServletException the servlet exception
	 * @throws IOException Signals that an I/O exception has occurred.
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession();
		
		// If user is logged in call JSP
		if(session.getAttribute("user") != null) {
			request.getRequestDispatcher("/WEB-INF/ChgUsrPass.jsp").forward(request, response);
		}
		// Else call error page
		else {
			request.setAttribute("error", "Keine Zugriffsberechtigung!");
			request.getRequestDispatcher("WEB-INF/Error.jsp").forward(request, response);
		}
		
		
	}

	/**
	 * Do post.
	 *
	 * @param request the request
	 * @param response the response
	 * @throws ServletException the servlet exception
	 * @throws IOException Signals that an I/O exception has occurred.
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");
		
		DatabaseHelper db_access = null;
		PasswordCheckHelper check = null;
		HttpSession session = null;
		String customerNo = "";
		String currentPass = "";

		
		try{
			db_access = new DatabaseHelper();
			
			check = new PasswordCheckHelper();
			Customer customer = new Customer();
		
			// Getting customer number, password and customer from database
			session = request.getSession();
			customerNo = db_access.getCustomerNoByEMail((String)session.getAttribute("user"));
			currentPass = db_access.getPassword((String)session.getAttribute("user")); 
			customer = db_access.getCustomer(customerNo);

			// Comparing password from database and old password from JSP
			if(EncryptPasswordHelper.MD5(request.getParameter("oldpass")).equals(currentPass) )	{	
				// Comparing new passwords from JSP
				if( request.getParameter("newpass1").equals(request.getParameter("newpass2"))){
					// Checking if the new password meets the requirements
					if(check.isOK(request.getParameter("newpass1"))){
						// Set new password in database
						customer.setPassword(EncryptPasswordHelper.MD5(request.getParameter("newpass1")));
						customer.setCustomerNo(customerNo);
						db_access.changePassword(customer);
						
						request.setAttribute("passwordChanged", true);
					}	
					else {
						request.setAttribute("message", "Das Passwort erf�llt nicht die Kriterien.");
					}
					
				}
				else {
					request.setAttribute("message", "Die neuen Passw�rter stimmen nicht �berein.");

				}
			}
		 	else {
				request.setAttribute("message", "Das alte Passwort stimmt nicht.");
	
			}
			
				
			doGet(request, response);
			
		}
		catch (Exception e){
			e.printStackTrace();
		}		
			
    
	}
}
