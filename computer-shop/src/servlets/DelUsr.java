package servlets;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import helperClasses.DatabaseHelper;

/**
 * Servlet implementation class DelUsr.
 * 
 * @author Jan-Niklas Posselt
 */
@WebServlet("/DelUsr")
public class DelUsr extends HttpServlet {
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
    /**
     * Instantiates a new Servlet.
     *
     * @see HttpServlet#HttpServlet().
     */
    public DelUsr() {
        super();
    }

	/**
	 * Do get.
	 *
	 * @param request the request
	 * @param response the response
	 * @throws ServletException the servlet exception
	 * @throws IOException Signals that an I/O exception has occurred.
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession();
		
		// If user is logged in call JSP
		if(session.getAttribute("user") != null) {
			request.getRequestDispatcher("/WEB-INF/DelUsr.jsp").forward(request, response);
		}
		// Else call error page
		else {
			request.setAttribute("error", "Keine Zugriffsberechtigung!");
			request.getRequestDispatcher("WEB-INF/Error.jsp").forward(request, response);
		}
		
	}

	/**
	 * Do post.
	 *
	 * @param request the request
	 * @param response the response
	 * @throws ServletException the servlet exception
	 * @throws IOException Signals that an I/O exception has occurred.
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		DatabaseHelper db_access = null;
		HttpSession session = null;
		String customerNo = "";
		
			
		try{
			db_access = new DatabaseHelper();					
			session = request.getSession();
			
			// Getting customer number from database
			customerNo = db_access.getCustomerNoByEMail((String)session.getAttribute("user"));
			
			// Delete customer from database
			db_access.deleteCustomer(customerNo);
		}	
		catch (Exception e) {
			e.printStackTrace();
		}		

		// Forwarding to Logout
		request.getRequestDispatcher("Logout").forward(request, response);
	
	}
}
