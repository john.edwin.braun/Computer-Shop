package servlets;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import beans.Article;
import beans.Customer;
import helperClasses.DatabaseHelper;

/**
 * Servlet implementation class LandingPage.
 * 
 * @author Leon Stollberg
 */
@WebServlet("/LandingPage")
public class LandingPage extends HttpServlet {
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	/** The customer. */
	Customer customer = null;
       
    /**
     * Instantiates a new Servlet.
     *
     * @see HttpServlet#HttpServlet()
     */
    public LandingPage() {
        super();
    }

	/**
	 * Do get.
	 *
	 * @param request the request
	 * @param response the response
	 * @throws ServletException the servlet exception
	 * @throws IOException Signals that an I/O exception has occurred.
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		Article article1 = new Article();
		Article article2 = new Article();
		Article article3 = new Article();
		DatabaseHelper dbHelper = new DatabaseHelper();
		
		try {
			// Getting articles from database
			article1 = dbHelper.getArticle("1");
			article2 = dbHelper.getArticle("2");
			article3 = dbHelper.getArticle("3");
			
			// Setting data in request
			request.setAttribute("article1", article1);
			request.setAttribute("article2", article2);
			request.setAttribute("article3", article3);
		}
		catch (Exception ex) {
			ex.printStackTrace();
			
			request.setAttribute("noOffers", "true");
		}
		
		// Call JSP
		request.getRequestDispatcher("/WEB-INF/LandingPage.jsp").forward(request, response);
		
		request.removeAttribute("placedOrder");
		request.removeAttribute("articleCreated");
	}

	/**
	 * Do post.
	 *
	 * @param request the request
	 * @param response the response
	 * @throws ServletException the servlet exception
	 * @throws IOException Signals that an I/O exception has occurred.
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}
}
