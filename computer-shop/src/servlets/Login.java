package servlets;

import java.io.IOException;
import java.security.NoSuchAlgorithmException;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.Customer;
import helperClasses.DatabaseHelper;
import helperClasses.EncryptPasswordHelper;

/**
 * Servlet implementation class Login.
 * 
 * @author John Edwin Braun
 */
@WebServlet("/Login")
public class Login extends HttpServlet {
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;
       
    /**
     * Instantiates a new Servlet.
     *
     * @see HttpServlet#HttpServlet()
     */
    public Login() {
        super();
    }

	/**
	 * Do get.
	 *
	 * @param request the request
	 * @param response the response
	 * @throws ServletException the servlet exception
	 * @throws IOException Signals that an I/O exception has occurred.
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");
		
		HttpSession session = request.getSession();
		
		// Handling: Registration completed
		if(request.getParameter("registered") != null) {
			request.setAttribute("message", "Erfolgreich registriert!");
		}
		
		// Handling: Destination of Login
		if(request.getParameter("destination") != null ) {
			request.setAttribute("destination", request.getParameter("destination"));
		}
		
		// If user is not logged in call JSP
		if(session.getAttribute("user") == null){
			request.getRequestDispatcher("WEB-INF/Login.jsp").forward(request, response);
		}
		// Else call JSP with message
		else {
			request.setAttribute("message", "Sie sind bereits angemeldet.");
			request.getRequestDispatcher("WEB-INF/Login.jsp").forward(request, response);
		}
		
	
	}

	/**
	 * Do post.
	 *
	 * @param request the request
	 * @param response the response
	 * @throws ServletException the servlet exception
	 * @throws IOException Signals that an I/O exception has occurred.
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");
		
		String password = "";
		String email = "";
		
		Customer customer = new Customer();
		boolean rightPassword = false;
		
		HttpSession session = request.getSession();
		
		try {
			DatabaseHelper db_access = new DatabaseHelper();

				// If user is not logged in
				if(session.getAttribute("user") == null){
					try {
						// Hash password
						password = EncryptPasswordHelper.MD5(request.getParameter("password"));
					} catch (NoSuchAlgorithmException e) {
						e.printStackTrace();
					}
					
					// Get email from JSP
					email = request.getParameter("email").trim();
					
					// Check if password is right
					rightPassword = db_access.checkPasswordAndEmail(email, password);
						
					if(rightPassword == true) {
						// Get customer from email
						customer = db_access.getCustomer(db_access.getCustomerNoByEMail(email));
						
						// Set session attributes
						session.setAttribute("customer", customer);
						session.setAttribute("user", email);
						session.setAttribute("admin", customer.getAdmin());
						
						// Handling: Destination after Login
						if(!request.getParameter("destination").equals("")) {
							response.sendRedirect(request.getParameter("destination"));
						}
						else {
							response.sendRedirect("LandingPage");
						}
						
					}
					else {
						request.setAttribute("message", "Passwort oder E-Mail falsch!");
						doGet(request, response);
					}
				}
				else {
					doGet(request, response);
				}
				
		}
		catch (SQLException|NullPointerException e) {
			e.printStackTrace();
			
			// Calling error page
			request.setAttribute("error", "Fehler beim Lesen der Datenbank");
			request.getRequestDispatcher("WEB-INF/Error.jsp").forward(request, response);
		}

	}

}
