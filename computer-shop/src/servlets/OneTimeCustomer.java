package servlets;

import java.io.IOException;
import java.sql.SQLException;
import java.sql.SQLIntegrityConstraintViolationException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.Customer;
import helperClasses.DatabaseHelper;

/**
 * Servlet implementation class OneTimeCustomer.
 * 
 * @author Oliver Neumann
 */
@WebServlet("/OneTimeCustomer")
public class OneTimeCustomer extends HttpServlet {
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;
       
    /**
     * Instantiates a new Servlet.
     *
     * @see HttpServlet#HttpServlet()
     */
    public OneTimeCustomer() {
        super();
    }

	/**
	 * Do get.
	 *
	 * @param request the request
	 * @param response the response
	 * @throws ServletException the servlet exception
	 * @throws IOException Signals that an I/O exception has occurred.
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// Call JSP
		request.getRequestDispatcher("/WEB-INF/OneTimeCustomer.jsp").forward(request, response);
	}

	/**
	 * Do post.
	 *
	 * @param request the request
	 * @param response the response
	 * @throws ServletException the servlet exception
	 * @throws IOException Signals that an I/O exception has occurred.
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");
		
		DatabaseHelper db_access = null;
		Customer customer = new Customer();
		HttpSession session = null;
		
		session = request.getSession();
		
		// Get data from JSP
		customer.setTitle(request.getParameter("title"));
		customer.setFormOfAddress(request.getParameter("formOfAddress"));
		customer.setName(request.getParameter("name"));
		customer.setFirstname(request.getParameter("firstName"));
		customer.setEmail(request.getParameter("email"));
		customer.setInvoiceStreet(request.getParameter("invoiceStreet"));
		customer.setInvoiceHouseNumber(request.getParameter("invoiceHouseNumber"));
		customer.setInvoiceTown(request.getParameter("invoiceTown"));
		customer.setInvoicePostalCode(request.getParameter("invoicePostalCode"));
		customer.setOneTimeCustomer("true");
		
		try {
			db_access  = new DatabaseHelper();
			// Insert customer into database
			customer.setCustomerNo(db_access.insertOneTimeCustomer(customer));
		} 		
		catch (SQLIntegrityConstraintViolationException e1) {
			e1.printStackTrace();
			request.setAttribute("message", "Diese E-Mail wird bereits verwendet");
			doGet(request, response);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		session.setAttribute("customer", customer);
		
		// Redirect to additional order data 
		response.sendRedirect("AdditionalOrderData");
	}

}
