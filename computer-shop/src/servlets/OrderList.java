package servlets;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.Article;
import beans.Order;
import beans.OrderPosition;
import helperClasses.DatabaseHelper;

/**
 * Servlet implementation class OrderList.
 * 
 * @author John Edwin Braun
 */
@WebServlet("/OrderList")
public class OrderList extends HttpServlet {
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;
       
    /**
     * Instantiates a new Servlet.
     *
     * @see HttpServlet#HttpServlet()
     */
    public OrderList() {
        super();
    }

	/**
	 * Do get.
	 *
	 * @param request the request
	 * @param response the response
	 * @throws ServletException the servlet exception
	 * @throws IOException Signals that an I/O exception has occurred.
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession();
		
		// If user is logged in
		if(session.getAttribute("user") != null) {
			List<Order> orderList = new ArrayList<Order>();
			List<OrderPosition> orderPositionList = new ArrayList<OrderPosition>();
			List<Article> articleList = new ArrayList<Article>();
			
			DatabaseHelper dbHelper = new DatabaseHelper();
			String customerNo = "";
			String email = "";
			
			// Get customer data, orders, order positions and ordered articles
			email = (String) session.getAttribute("user");		
			customerNo = dbHelper.getCustomerNoByEMail(email);
			orderList = dbHelper.getOrderListByCustomerNo(customerNo);
			orderPositionList = dbHelper.getOrderPositionListByCustomerNo(customerNo);
			articleList = dbHelper.getOrderedArticles(customerNo);
			
			// Set data in request
			request.setAttribute("orderList", orderList);
			request.setAttribute("orderPositionList", orderPositionList);
			request.setAttribute("articleList", articleList);
			
			// Call JSP
			request.getRequestDispatcher("WEB-INF/OrderList.jsp").forward(request, response);
		}
		// Else (user not logged in)
		else {
			// Call error page
			request.setAttribute("error", "Keine Zugriffsberechtigung!");
			request.getRequestDispatcher("WEB-INF/Error.jsp").forward(request, response);
		}
		
		
	}

	/**
	 * Do post.
	 *
	 * @param request the request
	 * @param response the response
	 * @throws ServletException the servlet exception
	 * @throws IOException Signals that an I/O exception has occurred.
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

}
