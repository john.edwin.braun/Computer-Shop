package servlets;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.Customer;
import beans.Order;
import helperClasses.DatabaseHelper;
import helperClasses.ShoppingCartHelper;
import helperClasses.ShoppingCartItem;

/**
 * Servlet implementation class OrderOverview.
 * 
 * @author John Edwin Braun
 */
@WebServlet("/OrderOverview")
public class OrderOverview extends HttpServlet {
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;
       
    /**
     * Instantiates a new Servlet.
     *
     * @see HttpServlet#HttpServlet()
     */
    public OrderOverview() {
        super();
    }

	/**
	 * Do get.
	 *
	 * @param request the request
	 * @param response the response
	 * @throws ServletException the servlet exception
	 * @throws IOException Signals that an I/O exception has occurred.
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = null;
		Customer customer = null;
		List<ShoppingCartItem> shoppingCartItemList = new ArrayList<ShoppingCartItem>();
		ShoppingCartHelper shoppingCart = new ShoppingCartHelper();
		
		session = request.getSession();
		// Get shopping cart items from session
		shoppingCartItemList = shoppingCart.getShoppingCartItemList(session);
		
		// if shopping cart is not empty
		if(shoppingCartItemList != null) {
			customer = (Customer) session.getAttribute("customer");
			
			// If no special delivery information was entered, use invoice data
			if(request.getAttribute("deliveryName").equals("")){
				request.setAttribute("deliveryName", customer.getName());
			}
			if(request.getAttribute("deliveryFirstname").equals("")){
				request.setAttribute("deliveryFirstname", customer.getFirstname());
			}
			if(request.getAttribute("deliveryStreet").equals("")){
				request.setAttribute("deliveryStreet", customer.getInvoiceStreet());
			}
			if(request.getAttribute("deliveryHouseNumber").equals("")){
				request.setAttribute("deliveryHouseNumber", customer.getInvoiceHouseNumber());
			}
			if(request.getAttribute("deliveryPostalCode").equals("")){
				request.setAttribute("deliveryPostalCode", customer.getInvoicePostalCode());
			}
			if(request.getAttribute("deliveryTown").equals("")){
				request.setAttribute("deliveryTown", customer.getInvoiceTown());
			}
		
			// Set shopping cart items in request
			request.setAttribute("shoppingCartItems", shoppingCartItemList);
			
			// Call JSP
			request.getRequestDispatcher("WEB-INF/OrderOverview.jsp").forward(request, response);
		}
		else {
			// Call error page
			request.setAttribute("error", "Es ist ein Problem mit dem Warenkorb aufgetreten");
			request.getRequestDispatcher("WEB-INF/Error.jsp").forward(request, response);
		}
	}

	/**
	 * Do post.
	 *
	 * @param request the request
	 * @param response the response
	 * @throws ServletException the servlet exception
	 * @throws IOException Signals that an I/O exception has occurred.
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");
		
		// If destination is entered 
		if(request.getAttribute("destination") != null) {
			doGet(request, response);
		}
		// Else (no destination is entered)
		else {
			Order order = new Order ();
			DatabaseHelper dbHelper = new DatabaseHelper();
			HttpSession session = request.getSession();
			
			// Get data from JSP
			order.setCustomerNo(request.getParameter("customerNo"));
			order.setDeliveryFirstname(request.getParameter("deliveryFirstname"));
			order.setDeliveryHouseNumber(request.getParameter("deliveryHouseNumber"));
			order.setDeliveryName(request.getParameter("deliveryName"));
			order.setDeliveryPostalCode(request.getParameter("deliveryPostalCode"));
			order.setDeliveryStreet(request.getParameter("deliveryStreet"));
			order.setDeliveryTown(request.getParameter("deliveryTown"));
			order.setDispatchType(request.getParameter("dispatchType"));
			order.setInvoiceHouseNumber(request.getParameter("invoiceHouseNumber"));
			order.setInvoicePostalCode(request.getParameter("invoicePostalCode"));
			order.setInvoiceStreet(request.getParameter("invoiceStreet"));
			order.setInvoiceTown(request.getParameter("invoiceTown"));
			order.setPaymentMethod(request.getParameter("paymentMethod"));
			order.setInvoiceFirstname(request.getParameter("invoiceFirstname"));
			order.setInvoiceName(request.getParameter("invoiceName"));
			
			// Create order in database
			dbHelper.createOrder(order, session);
			
			// Clear the shopping cart
			session.removeAttribute("shoppingCartItemList");
			
			request.setAttribute("placedOrder", "true");
			
			// Forward to landing page
			request.getRequestDispatcher("LandingPage").forward(request, response);
		}
				
		
	}

}
