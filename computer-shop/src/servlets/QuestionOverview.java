package servlets;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import beans.Question;
import helperClasses.DatabaseHelper;

/**
 * Servlet implementation class QuestionOverview.
 * 
 * @author John Edwin Braun
 */
@WebServlet("/QuestionOverview")
public class QuestionOverview extends HttpServlet {
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;
       
    /**
     * Instantiates a new Servlet.
     *
     * @see HttpServlet#HttpServlet()
     */
    public QuestionOverview() {
        super();
    }

	/**
	 * Do get.
	 *
	 * @param request the request
	 * @param response the response
	 * @throws ServletException the servlet exception
	 * @throws IOException Signals that an I/O exception has occurred.
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		DatabaseHelper db_access = null;
		List<Question> questionList = new ArrayList<Question>();

		db_access =  new DatabaseHelper();
		
		try {
			// Get all active questions from database
			questionList = db_access.getAllActiveQuestions();
		
			// Set data in request
			request.setAttribute("questionList", questionList);
			
			// Call JSP
			request.getRequestDispatcher("WEB-INF/QuestionOverview.jsp").forward(request, response);
			request.removeAttribute("questionAsked");
		} 
		catch (SQLException|NullPointerException e) {
			e.printStackTrace();
			// Call error page
			request.setAttribute("error", "Fehler beim Lesen der Datenbank");
			request.getRequestDispatcher("WEB-INF/Error.jsp").forward(request, response);
		}
	}

	/**
	 * Do post.
	 *
	 * @param request the request
	 * @param response the response
	 * @throws ServletException the servlet exception
	 * @throws IOException Signals that an I/O exception has occurred.
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");
		
		DatabaseHelper db_access = null;
		Question question = new Question();
		
		// Get data from JSP
		question.setTitle(request.getParameter("title"));
		question.setQuestion(request.getParameter("question"));
		question.setQuestioner(request.getParameter("name"));
		
		try {
			// Insert question in database
			db_access  = new DatabaseHelper();
			db_access.insertQuestion(question);
			
		} 
		
		catch (Exception e) {
			e.printStackTrace();
		}
		
		request.setAttribute("questionAsked", request.getParameter("questionAsked"));
		doGet(request, response);
	}

}
