package servlets;

import java.io.IOException;
import java.security.NoSuchAlgorithmException;
import java.sql.SQLIntegrityConstraintViolationException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import beans.Customer;
import helperClasses.PasswordCheckHelper;
import helperClasses.DatabaseHelper;
import helperClasses.EncryptPasswordHelper;

/**
 * Servlet implementation class OneTimeCustomer.
 * 
 * @author Katharina Unruhe
 */
@WebServlet("/Registration")
public class Registration extends HttpServlet {
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;
       
    /**
     * Instantiates a new Servlet.
     *
     * @see HttpServlet#HttpServlet()
     */
    public Registration() {
        super();
    }

	/**
	 * Do get.
	 *
	 * @param request the request
	 * @param response the response
	 * @throws ServletException the servlet exception
	 * @throws IOException Signals that an I/O exception has occurred.
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// Call JSP
		request.getRequestDispatcher("/WEB-INF/Registration.jsp").forward(request, response);
		
	}

	/**
	 * Do post.
	 *
	 * @param request the request
	 * @param response the response
	 * @throws ServletException the servlet exception
	 * @throws IOException Signals that an I/O exception has occurred.
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");
	
		PasswordCheckHelper check = new PasswordCheckHelper() ;
		
		// If password meets requirements
		if (check.isOK(request.getParameter("password"))) {
			DatabaseHelper db_access = null;
			Customer customer = new Customer();
			
			// Get data from JSP
			customer.setTitle(request.getParameter("title"));
			customer.setFormOfAddress(request.getParameter("formOfAddress"));
			customer.setName(request.getParameter("name"));
			customer.setFirstname(request.getParameter("firstName"));				
			customer.setEmail(request.getParameter("email"));
			customer.setInvoiceHouseNumber(request.getParameter("invoiceHouseNumber"));
			customer.setInvoicePostalCode(request.getParameter("invoicePostalCode"));
			customer.setInvoiceStreet(request.getParameter("invoiceStreet"));
			customer.setInvoiceTown(request.getParameter("invoiceTown"));
			customer.setOneTimeCustomer("false");
			
			try {
				// Hash password
				customer.setPassword(EncryptPasswordHelper.MD5(request.getParameter("password")));
			} catch (NoSuchAlgorithmException e2) {
				e2.printStackTrace();
			}
			
			try {
				db_access  = new DatabaseHelper();
				
				// Insert customer into database
				db_access.insertCustomer(customer); 
				
				request.setAttribute("message", "Erfolgreich registriert. Sie können sich jetzt anmelden!");

				// Redirect to login
				response.sendRedirect("Login?registered=true");
			} 
			
			catch (SQLIntegrityConstraintViolationException e1) {
				e1.printStackTrace();
				// Email is already used
				request.setAttribute("message", "Diese E-Mail wird bereits verwendet");
				doGet(request, response);
			}
			
			catch (Exception e) {
				e.printStackTrace();
			
			
			}

			
			
		}
		// Else (password does not meet requirements)
		else {
			// Send entered data back to JSP
			request.setAttribute("title", request.getParameter("title"));
			request.setAttribute("name", request.getParameter("name"));
			request.setAttribute("firstName", request.getParameter("firstName"));
			request.setAttribute("email", request.getParameter("email"));
			request.setAttribute("invoiceHouseNumber", request.getParameter("invoiceHouseNumber"));
			request.setAttribute("invoicePostalCode", request.getParameter("invoicePostalCode"));
			request.setAttribute("invoiceStreet", request.getParameter("invoiceStreet"));
			request.setAttribute("invoiceTown", request.getParameter("invoiceTown"));

			// Set message
			request.setAttribute("message", "Das Passwort entspricht nicht den Anforderungen.<br>Das Passwort muss genau 8 Zeichen lang sein.<br>1 Großbuchstabe (A-Z)<br>"
					+ "1 Kleinbuchstabe (a-z)<br>1 Ziffern (0-9)<br>1 Sonderzeichen (%/!,.)");
			doGet(request, response);
		}
	
	}
	
	
		
}