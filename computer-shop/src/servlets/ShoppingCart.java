package servlets;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import helperClasses.ShoppingCartHelper;
import helperClasses.ShoppingCartItem;

/**
 * Servlet implementation class ShoppingCart.
 * 
 * @author John Edwin Braun
 */
@WebServlet("/ShoppingCart")
public class ShoppingCart extends HttpServlet {
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
    /**
     * Instantiates a new shopping cart.
     *
     * @see HttpServlet#HttpServlet()
     */
    public ShoppingCart() {
        super();
    }

	/**
	 * Do get.
	 *
	 * @param request the request
	 * @param response the response
	 * @throws ServletException the servlet exception
	 * @throws IOException Signals that an I/O exception has occurred.
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = null;
		List<ShoppingCartItem> shoppingCartItemList = new ArrayList<ShoppingCartItem>();
		ShoppingCartHelper shoppingCart = new ShoppingCartHelper();
		
		// Get shopping cart items from session
		session = request.getSession();
		shoppingCartItemList = shoppingCart.getShoppingCartItemList(session);
		
		// If shopping cart is empty
		if(shoppingCartItemList == null) {
			// Call JSP
			request.setAttribute("message", "Keine Artikel im Warenkorb!");
			request.getRequestDispatcher("WEB-INF/ShoppingCart.jsp").forward(request, response);
		}
		// Else (shopping cart probably not empty)
		else {
			// Shopping cart is empty
			if(shoppingCartItemList.isEmpty()) {
				// Call JSP
				request.setAttribute("message", "Keine Artikel im Warenkorb!");
				request.getRequestDispatcher("WEB-INF/ShoppingCart.jsp").forward(request, response);
			}
			// Shopping cart not empty
			else {
				// Call JSP
				request.setAttribute("shoppingCartItems", shoppingCartItemList);			
				request.getRequestDispatcher("WEB-INF/ShoppingCart.jsp").forward(request, response);
			}
		}
	}

	/**
	 * Do post.
	 *
	 * @param request the request
	 * @param response the response
	 * @throws ServletException the servlet exception
	 * @throws IOException Signals that an I/O exception has occurred.
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String submitMessage = "";
		int i = 0;
		String positionToDelete = "";
		ShoppingCartHelper shoppingCart = new ShoppingCartHelper();
		HttpSession session = null;
		
		session = request.getSession();
		// Get submit message
		submitMessage = request.getParameter("submit");
		
		// If submit message "delete all"
		if(submitMessage.equals("deleteAll")) {
			// Delete all positions 
			shoppingCart.deleteAll(session);
		}
		else {
			// Loop through submit message until '_' is found
			while(!submitMessage.substring(i, i+1).equals("_")) {
				i++;
			}
			i++;
			// Extract position to delete
			positionToDelete = submitMessage.substring(i);
			// Delete position
			shoppingCart.deletePosition(session, positionToDelete);
		}
		
		doGet(request, response);
	}

}
