package servlets;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import java.io.File;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.http.Part;

import beans.Article;
import helperClasses.DatabaseHelper;

/**
 * The Class UploadServlet.
 * 
 * @author https://www.codejava.net/java-ee/servlet/java-file-upload-example-with-servlet-30-api
 */
@WebServlet("/UploadServlet")
@MultipartConfig(fileSizeThreshold=1024*1024*2, // 2MB
                 maxFileSize=1024*1024*10,      // 10MB
                 maxRequestSize=1024*1024*50)   // 50MB
public class UploadServlet extends HttpServlet {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	/** The article. */
	Article article = new Article();
	
	/** The Constant SAVE_DIR. */
	//ordner an dem die bilder gespeichert werden
    private static final String SAVE_DIR = "Pictures";
    
    /* (non-Javadoc)
     * @see javax.servlet.http.HttpServlet#doGet(javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse)
     */
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    	
    	HttpSession session = null;
		session = request.getSession();
		
		if(session.getAttribute("admin").equals("true")) {
	    	article.setArticleNo(request.getParameter("ArticleID"));
	    	System.out.println(article.getArticleNo());
	    	
	    	request.getRequestDispatcher("/WEB-INF/upload.jsp").forward(request, response);
		}
		else {
			request.setAttribute("error", "Keine Zugruffsberechtigung!");
			request.getRequestDispatcher("WEB-INF/Error.jsp").forward(request, response);
		}
    	
    	
    	
    	

    }

    
    /* (non-Javadoc)
     * @see javax.servlet.http.HttpServlet#doPost(javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse)
     */
    //handles file upload
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    	DatabaseHelper db_access = null;
    	
        // hier wird der pfad des webshops erkannt
        String appPath = request.getServletContext().getRealPath("");
        
        System.out.println(appPath);
        
        // erstellen des pfades unter dem das bild gespeichert wird
        String savePath = appPath + File.separator + SAVE_DIR;
        System.out.println(savePath);
         
        // creates the save directory if it does not exists
        File fileSaveDir = new File(savePath);
        if (!fileSaveDir.exists()) {
            fileSaveDir.mkdir();
        }
         
        for (Part part : request.getParts()) {
            String fileName = extractFileName(part); // werden in der form sowohl text als auch datei migegeben, wird hier der path zu lang
            
//            System.out.println(part);  
//            System.out.println(fileName);
            
            // refines the fileName in case it is an absolute path
            fileName = new File(fileName).getName();
            part.write(savePath + File.separator + fileName);
            
            //hier wird der pfad f�r die datenbank generiert
            article.setPictureLink("Pictures/" + fileName);
            System.out.println(article.getPictureLink());
        }
        
		try {
			db_access  = new DatabaseHelper();
			db_access.updatePicture(article);
		} 
		
		catch (Exception e) {
			e.printStackTrace();
		}
        
        //aufrufen eines nachrichten servlets
		request.setAttribute("articleCreated", true);
		request.getRequestDispatcher("LandingPage").forward(request, response);
		
//		response.sendRedirect("UploadServlet");
    }
    
    /**
     * Extract file name.
     *
     * @param part the part
     * @return the string
     */
    //Extracts file name from HTTP header content-disposition
    private String extractFileName(Part part) {
        String contentDisp = part.getHeader("content-disposition");
//      System.out.println(contentDisp);
        String[] items = contentDisp.split(";");
        for (String s : items) {
        	System.out.println(s);
            if (s.trim().startsWith("filename")) {
                return s.substring(s.indexOf("=") + 2, s.length()-1);
                
                
            }
            
        }
        return "";
    }
}
